﻿#ifndef VOXELCRAFT_TYPES_H
#define VOXELCRAFT_TYPES_H

#include "../../core/typedefs.h"
#include "../../core/math/vector3.h"
#include "utils/structs/int3.h"

#define BLOCK_SIZE 1

#define SIDE_PZZ 0
#define SIDE_NZZ 1
#define SIDE_ZPZ 2
#define SIDE_ZNZ 3
#define SIDE_ZZP 4
#define SIDE_ZZN 5

#define SIDE_PPZ 6
#define SIDE_PNZ 7
#define SIDE_NPZ 8
#define SIDE_NNZ 9
#define SIDE_PZP 10
#define SIDE_PZN 11
#define SIDE_NZP 12
#define SIDE_NZN 13
#define SIDE_ZPP 14
#define SIDE_ZPN 15
#define SIDE_ZNP 16
#define SIDE_ZNN 17
#define SIDE_PPP 18
#define SIDE_PPN 19
#define SIDE_PNP 20
#define SIDE_PNN 21
#define SIDE_NPP 22
#define SIDE_NPN 23
#define SIDE_NNP 24
#define SIDE_NNN 25

#define SIDES_CONTACT_FLAG 0x3F
#define SIDES_ALL_FLAG 0x3FFFFFF
#define SIDES_CONTACT_COUNT 6
#define SIDES_COUNT 26

#define CHUNK_BLOCKS_SIZE_BITS 5
#define CHUNK_BLOCKS_SIZE (1 << CHUNK_BLOCKS_SIZE_BITS)
#define CHUNK_BLOCKS_COUNT (CHUNK_BLOCKS_SIZE * CHUNK_BLOCKS_SIZE * CHUNK_BLOCKS_SIZE)
#define CHUNK_SIDE_BLOCKS_COUNT (CHUNK_BLOCKS_SIZE * CHUNK_BLOCKS_SIZE)
#define CHUNK_BLOCKS_OUSIDE_SIZE (CHUNK_BLOCKS_SIZE + 2)

#define DIV_BY_BASE(r, base) ((r) < 0 ? (((r) + 1) / (base)) - 1: (r) / (base))
#define DIV_VEC_BY_BASE(v, base) (Int3(DIV_BY_BASE(v.x, base), DIV_BY_BASE(v.y, base), DIV_BY_BASE(v.z, base)))
#define FLOAT_COORD_TO_INT(val) (val < 0 ? Math::ceil(val) : Math::floor(val))

#define WORLD_TO_BLOCK(v) DIV_VEC_BY_BASE(Int3(10000 * v), 10000 * BLOCK_SIZE)
#define WORLD_TO_CHUNK(v) DIV_VEC_BY_BASE(Int3(10000 * v), 10000 * CHUNK_BLOCKS_SIZE * BLOCK_SIZE)
#define BLOCK_TO_CHUNK(v) DIV_VEC_BY_BASE(v, CHUNK_BLOCKS_SIZE)
#define CHUNK_TO_BLOCK(c) (c * CHUNK_BLOCKS_SIZE)

#define UINT32_PRIME 10002017
#define N_BLOCKS_SIDE_TRANSPARENCY_SIZE (CHUNK_SIDE_BLOCKS_COUNT / 8)
#define N_BLOCKS_TRANSPARENCY_SIZE (N_BLOCKS_SIDE_TRANSPARENCY_SIZE * 6)

typedef uint16_t block_ipos_t; //max 32x32x32!

class Actions {
public:
	enum ActionsEnum {
		NONE = 0,
		REMOVE = 1
	};
};

namespace vxl_t {

	const int flags[] = {
		1 << SIDE_PZZ,
		1 << SIDE_NZZ,
		1 << SIDE_ZPZ,
		1 << SIDE_ZNZ,
		1 << SIDE_ZZP,
		1 << SIDE_ZZN,
		1 << SIDE_PPZ,
		1 << SIDE_PNZ,
		1 << SIDE_NPZ,
		1 << SIDE_NNZ,
		1 << SIDE_PZP,
		1 << SIDE_PZN,
		1 << SIDE_NZP,
		1 << SIDE_NZN,
		1 << SIDE_ZPP,
		1 << SIDE_ZPN,
		1 << SIDE_ZNP,
		1 << SIDE_ZNN,
		1 << SIDE_PPP,
		1 << SIDE_PPN,
		1 << SIDE_PNP,
		1 << SIDE_PNN,
		1 << SIDE_NPP,
		1 << SIDE_NPN,
		1 << SIDE_NNP,
		1 << SIDE_NNN
	};

	const int opposite_sides[] = {
		SIDE_NZZ,//SIDE_PZZ 0
		SIDE_PZZ,//SIDE_NZZ 1
		SIDE_ZNZ,//SIDE_ZPZ 2
		SIDE_ZPZ,//SIDE_ZNZ 3
		SIDE_ZZN,//SIDE_ZZP 4
		SIDE_ZZP,//SIDE_ZZN 5
		SIDE_NNZ,//SIDE_PPZ 6
		SIDE_NPZ,//SIDE_PNZ 7
		SIDE_PNZ,//SIDE_NPZ 8
		SIDE_PPZ,//SIDE_NNZ 9
		SIDE_NZN,//SIDE_PZP 10
		SIDE_NZP,//SIDE_PZN 11
		SIDE_PZN,//SIDE_NZP 12
		SIDE_PZP,//SIDE_NZN 13
		SIDE_ZNN,//SIDE_ZPP 14
		SIDE_ZNP,//SIDE_ZPN 15
		SIDE_ZPN,//SIDE_ZNP 16
		SIDE_ZPP,//SIDE_ZNN 17
		SIDE_NNN,//SIDE_PPP 18
		SIDE_NNP,//SIDE_PPN 19
		SIDE_NPN,//SIDE_PNP 20
		SIDE_NPP,//SIDE_PNN 21
		SIDE_PNN,//SIDE_NPP 22
		SIDE_PNP,//SIDE_NPN 23
		SIDE_PPN,//SIDE_NNP 24
		SIDE_PPP //SIDE_NNN 25
	};

    const int near_sides_flag[] = {
        0,//(1 << SIDE_PZN) | (1 << SIDE_PZP) | (1 << SIDE_PNZ) | (1 << SIDE_PPZ),//SIDE_PZZ 0
        0,//(1 << SIDE_ZZN) | (1 << SIDE_ZZP) | (1 << SIDE_ZNZ) | (1 << SIDE_ZPZ),//SIDE_NZZ 1
        0,//(1 << SIDE_ZPN) | (1 << SIDE_ZPP) | (1 << SIDE_NPZ) | (1 << SIDE_PPZ),//SIDE_ZPZ 2
        0,//(1 << SIDE_ZNN) | (1 << SIDE_ZNP) | (1 << SIDE_NNZ) | (1 << SIDE_PNZ),//SIDE_ZNZ 3
        0,//(1 << SIDE_ZNP) | (1 << SIDE_ZPP) | (1 << SIDE_NZP) | (1 << SIDE_PZP),//SIDE_ZZP 4
        0,//(1 << SIDE_ZNN) | (1 << SIDE_ZPN) | (1 << SIDE_NZN) | (1 << SIDE_PZN),//SIDE_ZZN 5
        (1 << SIDE_PZZ) | (1 << SIDE_ZPZ),//SIDE_PPZ 6
        (1 << SIDE_PZZ) | (1 << SIDE_ZNZ),//SIDE_PNZ 7
        (1 << SIDE_NZZ) | (1 << SIDE_ZPZ),//SIDE_NPZ 8
        (1 << SIDE_NZZ) | (1 << SIDE_ZNZ),//SIDE_NNZ 9
        (1 << SIDE_PZZ) | (1 << SIDE_ZZP),//SIDE_PZP 10
        (1 << SIDE_PZZ) | (1 << SIDE_ZZN),//SIDE_PZN 11
        (1 << SIDE_NZZ) | (1 << SIDE_ZZP),//SIDE_NZP 12
        (1 << SIDE_NZZ) | (1 << SIDE_ZZN),//SIDE_NZN 13
        (1 << SIDE_ZPZ) | (1 << SIDE_ZZP),//SIDE_ZPP 14
        (1 << SIDE_ZPZ) | (1 << SIDE_ZZN),//SIDE_ZPN 15
        (1 << SIDE_ZNZ) | (1 << SIDE_ZZP),//SIDE_ZNP 16
        (1 << SIDE_ZNZ) | (1 << SIDE_ZZN),//SIDE_ZNN 17
        (1 << SIDE_PPZ) | (1 << SIDE_PZP) | (1 << SIDE_ZPP),//SIDE_PPP 18
        (1 << SIDE_PPZ) | (1 << SIDE_PZN) | (1 << SIDE_ZPN),//SIDE_PPN 19
        (1 << SIDE_PNZ) | (1 << SIDE_PZP) | (1 << SIDE_ZNP),//SIDE_PNP 20
        (1 << SIDE_PNZ) | (1 << SIDE_PZN) | (1 << SIDE_ZNN),//SIDE_PNN 21
        (1 << SIDE_NPZ) | (1 << SIDE_NZP) | (1 << SIDE_ZPP),//SIDE_NPP 22
        (1 << SIDE_NPZ) | (1 << SIDE_NZN) | (1 << SIDE_ZPN),//SIDE_NPN 23
        (1 << SIDE_NNZ) | (1 << SIDE_NZP) | (1 << SIDE_ZNP),//SIDE_NNP 24
        (1 << SIDE_NNZ) | (1 << SIDE_NZN) | (1 << SIDE_ZNN) //SIDE_NNN 25
    };

    const Int3 sides_vec[] = {
        Int3( 1,  0,  0),//SIDE_PZZ 0
        Int3(-1,  0,  0),//SIDE_NZZ 1
        Int3( 0,  1,  0),//SIDE_ZPZ 2
        Int3( 0, -1,  0),//SIDE_ZNZ 3
        Int3( 0,  0,  1),//SIDE_ZZP 4
        Int3( 0,  0, -1),//SIDE_ZZN 5
        Int3( 1,  1,  0),//SIDE_PPZ 6
        Int3( 1, -1,  0),//SIDE_PNZ 7
        Int3(-1,  1,  0),//SIDE_NPZ 8
        Int3(-1, -1,  0),//SIDE_NNZ 9
        Int3( 1,  0,  1),//SIDE_PZP 10
        Int3( 1,  0, -1),//SIDE_PZN 11
        Int3(-1,  0,  1),//SIDE_NZP 12
        Int3(-1,  0, -1),//SIDE_NZN 13
        Int3( 0,  1,  1),//SIDE_ZPP 14
        Int3( 0,  1, -1),//SIDE_ZPN 15
        Int3( 0, -1,  1),//SIDE_ZNP 16
        Int3( 0, -1, -1),//SIDE_ZNN 17
        Int3( 1,  1,  1),//SIDE_PPP 18
        Int3( 1,  1, -1),//SIDE_PPN 19
        Int3( 1, -1,  1),//SIDE_PNP 20
        Int3( 1, -1, -1),//SIDE_PNN 21
        Int3(-1,  1,  1),//SIDE_NPP 22
        Int3(-1,  1, -1),//SIDE_NPN 23
        Int3(-1, -1,  1),//SIDE_NNP 24
        Int3(-1, -1, -1) //SIDE_NNN 25
	};

    const float side_distance[] = {
        Int3( 1,  0,  0).length(),//SIDE_PZZ 0
        Int3(-1,  0,  0).length(),//SIDE_NZZ 1
        Int3( 0,  1,  0).length(),//SIDE_ZPZ 2
        Int3( 0, -1,  0).length(),//SIDE_ZNZ 3
        Int3( 0,  0,  1).length(),//SIDE_ZZP 4
        Int3( 0,  0, -1).length(),//SIDE_ZZN 5
        Int3( 1,  1,  0).length(),//SIDE_PPZ 6
        Int3( 1, -1,  0).length(),//SIDE_PNZ 7
        Int3(-1,  1,  0).length(),//SIDE_NPZ 8
        Int3(-1, -1,  0).length(),//SIDE_NNZ 9
        Int3( 1,  0,  1).length(),//SIDE_PZP 10
        Int3( 1,  0, -1).length(),//SIDE_PZN 11
        Int3(-1,  0,  1).length(),//SIDE_NZP 12
        Int3(-1,  0, -1).length(),//SIDE_NZN 13
        Int3( 0,  1,  1).length(),//SIDE_ZPP 14
        Int3( 0,  1, -1).length(),//SIDE_ZPN 15
        Int3( 0, -1,  1).length(),//SIDE_ZNP 16
        Int3( 0, -1, -1).length(),//SIDE_ZNN 17
        Int3( 1,  1,  1).length(),//SIDE_PPP 18
        Int3( 1,  1, -1).length(),//SIDE_PPN 19
        Int3( 1, -1,  1).length(),//SIDE_PNP 20
        Int3( 1, -1, -1).length(),//SIDE_PNN 21
        Int3(-1,  1,  1).length(),//SIDE_NPP 22
        Int3(-1,  1, -1).length(),//SIDE_NPN 23
        Int3(-1, -1,  1).length(),//SIDE_NNP 24
        Int3(-1, -1, -1).length() //SIDE_NNN 25
    };

    _FORCE_INLINE_ block_ipos_t encode_block_ipos(int x, int y, int z) {
        block_ipos_t res_x = (x & (CHUNK_BLOCKS_SIZE - 1)) << (2 * CHUNK_BLOCKS_SIZE_BITS);
        block_ipos_t res_y = (y & (CHUNK_BLOCKS_SIZE - 1)) << (1 * CHUNK_BLOCKS_SIZE_BITS);
        block_ipos_t res_z = (z & (CHUNK_BLOCKS_SIZE - 1)) << (0 * CHUNK_BLOCKS_SIZE_BITS);
        return res_x | res_y | res_z;
    }

    _FORCE_INLINE_ block_ipos_t encode_block_ipos(Int3& pos) {
        return encode_block_ipos(pos.x, pos.y, pos.z);
    }

    _FORCE_INLINE_ Int3 decode_block_ipos(block_ipos_t pos) {
        int x = (pos >> (2 * CHUNK_BLOCKS_SIZE_BITS)) & (CHUNK_BLOCKS_SIZE - 1);
        int y = (pos >> (1 * CHUNK_BLOCKS_SIZE_BITS)) & (CHUNK_BLOCKS_SIZE - 1);
        int z = (pos >> (0 * CHUNK_BLOCKS_SIZE_BITS)) & (CHUNK_BLOCKS_SIZE - 1);
        return Int3(x, y, z);
    }

/*
    _FORCE_INLINE_ int coord_by_base(int coord, int base) {
        return coord < 0 ? ((coord + 1) / base) - 1: coord / base;
    }

    _FORCE_INLINE_ int coord_by_base(real_t coord, int base) {
        int int_coord = (int) (coord * 100000);
        //int

        real_t res = coord / base;
        real_t fract = coord - base * res;
        return res < 0 ? (fract < 0 ? res - 1 : res) : res;
    }

    _FORCE_INLINE_ Int3 world_by_base(Vector3 vec, int base) {
        return Int3(coord_by_base(vec.x, base),
                    coord_by_base(vec.y, base),
                    coord_by_base(vec.z, base));
    }

    _FORCE_INLINE_ Int3 world_to_block(Vector3 pos) {
        return world_by_base(pos, BLOCK_SIZE);
    }

    _FORCE_INLINE_ Int3 world_to_chunk(Vector3 pos) {
        return world_by_base(pos, CHUNK_BLOCKS_SIZE);
    }
*/
    /*
    _FORCE_INLINE_ int coord_by_base(int coord, int base) {
        if (coord >= 0) {
            return coord / base;
        } else {
            int shifted_coord = coord + 1;
            int by_base = shifted_coord / base;
            return
        }
    }

    _FORCE_INLINE_ Int3 block_to_chunk(Int3 block_pos) {

    }*/
}

#endif // !VOXELCRAFT_TYPES_H




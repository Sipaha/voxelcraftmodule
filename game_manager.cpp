#include "game_manager.h"
#include "utils/threads/thread_pool.h"
#include "terrain/chunks/chunk.h"
#include "os/input.h"
#include "terrain/controller/light/light.h"
#include "hash_map.h"

void GameManager::_notification(int p_what) {

    switch (p_what) {

    case NOTIFICATION_READY: {

        if (!Engine::get_singleton()->is_editor_hint()) {

            set_process(true);

            ThreadPool::get_singleton()->start();
        }

        printf("BLOCK SIZE: %zd\n", sizeof(Block));
        printf("CHUNK SIZE: %zd\n", sizeof(Chunk));
        //printf("CHUNK_BLOCK SIZE: %zd\n", sizeof(ChunkBlocks));
    }
        break;

    case NOTIFICATION_PROCESS:

        if (quit_requested) {

            if (ThreadPool::get_singleton()->is_finished()) {

                get_tree()->quit();

                set_process(false);
            }

        } else if (Input::get_singleton()->is_action_just_pressed("quit")) {

            quit_requested = true;
            ThreadPool::get_singleton()->finish();
        }

        break;

    }
}

GameManager::GameManager()
{

}

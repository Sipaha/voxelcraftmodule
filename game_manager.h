#ifndef GAME_MANAGER_H
#define GAME_MANAGER_H

#include "scene/main/node.h"

class GameManager : public Node {

GDCLASS(GameManager, Node)

private:

	bool quit_requested = false;

protected:

	//static void _bind_methods();
	void _notification(int p_what);

public:
	GameManager();
};

#endif // GAME_CONTROLLER_H

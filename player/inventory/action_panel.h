#ifndef INVENTORY_H
#define INVENTORY_H

#include "item.h"
#include "vector.h"

class Inventory {

    Item* items[];
    int size;

public:

    Item*& operator [](int idx) {
        CRASH_COND(idx < 0 || idx >= size);
        return items[idx];
    }

    Inventory(int initial_size) {
        items = memnew_arr(Item, initial_size);
        size = initial_size;
    }

    ~Inventory() {
        memdelete_arr(items);
    }
};

#endif // INVENTORY_H

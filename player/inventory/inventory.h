#ifndef INVENTORY_H
#define INVENTORY_H

#include "../item/item.h"
#include "vector.h"

class Inventory {

    Array items;
    int size = 0;

protected:

    static void _bind_methods();

public:

    Variant &operator [](int idx) {
        CRASH_COND(idx < 0 || idx >= size);
        return items[idx];
    }

    Inventory() {}

    ~Inventory() {}
};

#endif // INVENTORY_H

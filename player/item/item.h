#ifndef ITEM_H
#define ITEM_H

#include "scene/resources/mesh.h"

struct Item {

    uint32_t id = 0;
    Ref<Texture> icon;

};

#endif // ITEM_H

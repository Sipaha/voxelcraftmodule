#include "item_node.h"

void ItemNode::_notification(int p_what) {

}

void ItemNode::set_item_id(uint16_t id) {
	item.id = item.id & 0xFFFF | (id << 16);
}

uint16_t ItemNode::get_item_id() {
	return item.id >> 16 & 0xFFFF;
}

void ItemNode::set_mod_id(uint16_t id) {
	item.id = item.id & 0xFFFF0000 | id;
}

uint16_t ItemNode::get_mod_id() {
	return item.id & 0xFFFF;
}

void ItemNode::set_icon(const Ref<Texture> &icon) {
	item.icon = icon;
}

Ref<Texture> ItemNode::get_icon() const {
	return item.icon;
}

void ItemNode::_bind_methods() {

	ClassDB::bind_method(D_METHOD("set_item_id", "id"), &ItemNode::set_item_id);
	ClassDB::bind_method(D_METHOD("get_item_id"), &ItemNode::get_item_id);
	ClassDB::bind_method(D_METHOD("set_mod_id", "id"), &ItemNode::set_mod_id);
	ClassDB::bind_method(D_METHOD("get_mod_id"), &ItemNode::get_mod_id);

	ClassDB::bind_method(D_METHOD("set_icon", "icon"), &ItemNode::set_icon);
	ClassDB::bind_method(D_METHOD("get_icon"), &ItemNode::get_icon);

	ADD_PROPERTY(PropertyInfo(Variant::INT, "ID"), "set_item_id", "get_item_id");
	ADD_PROPERTY(PropertyInfo(Variant::INT, "ModID"), "set_mod_id", "get_mod_id");
	ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "icon", PROPERTY_HINT_RESOURCE_TYPE, "Texture"), "set_icon", "get_icon");
}

ItemNode::ItemNode() {

}

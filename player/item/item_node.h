#ifndef ITEM_NODE_H
#define ITEM_NODE_H

#include "scene/3d/spatial.h"
#include "item.h"

class ItemNode : public Spatial {

    GDCLASS(ItemNode, Spatial)

    Item item;

    void set_item_id(uint16_t id);
    uint16_t get_item_id();

    void set_mod_id(uint16_t id);
    uint16_t get_mod_id();

    void set_icon(const Ref<Texture> &p_texture);
    Ref<Texture> get_icon() const;

protected:

    static void _bind_methods();
    void _notification(int p_what);

public:

    ItemNode();
};

#endif // ITEM_NODE_H

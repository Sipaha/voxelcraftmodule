#include "player.h"

void Player::_notification(int p_what) {

    switch (p_what) {

    case NOTIFICATION_READY:

        camera = Object::cast_to<Spatial>(get_node(NodePath("Camera")));
        view_cube_exclude.insert(get_rid());

        set_process_input(true);
        set_physics_process(true);
        set_process(true);

        observer = Terrain::get_observers()->create_observer();

        //set_fixed_process(true);
        break;

    case NOTIFICATION_PHYSICS_PROCESS:

        update_view_cube();

        break;

    case NOTIFICATION_PROCESS:

        observer->update_position(this->get_translation());

        break;

    default:
        break;
    }
}

void Player::_input(const Ref<InputEvent> &p_event) {

    Ref<InputEventMouseMotion> mm = p_event;

    view_cube.dirty = true;

    if (mm.is_valid()) {

        Vector2 rel = mm->get_relative();

        view_rotation.y = fmod(view_rotation.y - rel.x * 0.005, 2 * Math_PI);
        view_rotation.x = MAX(MIN(view_rotation.x - rel.y * 0.005, Math_PI), -Math_PI/2);

        camera->set_rotation(view_rotation);

		Transform ctr = camera->get_global_transform();
		view_dir = ctr.xform(Vector3(0, 0, -1)) - ctr.origin;

	} else {

		Ref<InputEventMouseButton> mb = p_event;

		if (mb.is_valid()) {

        }
    }
}

ViewCube* Player::get_view_cube() {
    return &view_cube;
}

void Player::update_view_cube() {

    if (view_cube.dirty) {

        Ref<World> world = get_world();

        PhysicsServerSync::mutex_lock();
        PhysicsDirectSpaceState *dss = PhysicsServer::get_singleton()->space_get_direct_state(world->get_space());

        Vector3 from = camera->get_global_transform().get_origin();
        Vector3 to = from + PLAYER_HAND_LENGTH * view_dir;

        PhysicsDirectSpaceState::RayResult rr;
        bool intersects = dss != NULL && dss->intersect_ray(from, to, rr, view_cube_exclude);
        PhysicsServerSync::mutex_unlock();

        if (intersects) {

            Vector3 normal = rr.normal;
            Vector3 block_pos = rr.position - normal * 0.001;
            Vector3 neighbour_pos = rr.position + normal * 0.001;

            view_cube.position = WORLD_TO_BLOCK(block_pos);
            view_cube.neighbour = WORLD_TO_BLOCK(neighbour_pos);
            view_cube.valid = true;

        } else {
            view_cube.valid = false;
        }

        view_cube.dirty = false;
    }
}

Player::Player() {

}

void Player::_bind_methods() {
    ClassDB::bind_method(D_METHOD("_input"), &Player::_input);
    ClassDB::bind_method(D_METHOD("get_view_cube"), &Player::get_view_cube);
}

void ViewCube::_bind_methods() {
    ClassDB::bind_method(D_METHOD("is_valid"), &ViewCube::is_valid);
    ClassDB::bind_method(D_METHOD("get_position"), &ViewCube::get_position);
    ClassDB::bind_method(D_METHOD("get_neighbour"), &ViewCube::get_neighbour);
}

bool ViewCube::is_valid() {
    return valid;
}

Vector3 ViewCube::get_position() {
    return Vector3(position.x, position.y, position.z);
}

Vector3 ViewCube::get_neighbour() {
    return Vector3(neighbour.x, neighbour.y, neighbour.z);
}

Vector3 ViewCube::get_world_position() {
    return Vector3(position.x * BLOCK_SIZE + 0.5f,
                   position.y * BLOCK_SIZE + 0.5f,
                   position.z * BLOCK_SIZE + 0.5f);
}

Vector3 ViewCube::get_world_neighbour() {
    return Vector3(neighbour.x * BLOCK_SIZE + 0.5f,
                   neighbour.y * BLOCK_SIZE + 0.5f,
                   neighbour.z * BLOCK_SIZE + 0.5f);
}




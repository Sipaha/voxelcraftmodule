#ifndef PLAYER_H
#define PLAYER_H

#include "inventory/inventory.h"
#include "scene/3d/camera.h"
#include "scene/3d/physics_body.h"
#include "../utils/structs/int3.h"
#include "../terrain/terrain.h"
#include "../servers/physics_server_wrap_sync.h"

//#define PLAYER_HAND_LENGTH 4
#define PLAYER_HAND_LENGTH 100

class ViewCube : public Object {

    GDCLASS(ViewCube, Object)

protected:

    static void _bind_methods();

public:

    bool valid = false;
    bool dirty = true;
    Int3 position = Int3(-1000, -1000, -1000);
    Int3 neighbour = Int3(-1000, -1000, -1000);

    bool is_valid();
    Vector3 get_position();
    Vector3 get_neighbour();
    Vector3 get_world_position();
    Vector3 get_world_neighbour();
};

class Player : public KinematicBody {

    GDCLASS(Player, KinematicBody)

private:

    Spatial* camera;

    Vector3 view_rotation = Vector3();
    Vector3 view_dir = Vector3(0, 0, -1);

    Set<RID> view_cube_exclude;
    ViewCube view_cube;

    Observer* observer;

    void update_view_cube();

protected:

    static void _bind_methods();
    void _notification(int p_what);
    void _input(const Ref<InputEvent> &p_input);

    ViewCube* get_view_cube();

public:

    Player();

};


#endif // PLAYER_H

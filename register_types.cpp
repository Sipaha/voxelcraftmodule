/* register_types.cpp */

#include "register_types.h"
#include "terrain/terrain.h"
#include "player/item/item_node.h"
#include "player/player.h"

#include "terrain/generator/flat_generator.h"
#include "terrain/generator/fast_noise_generator.h"

#include "game_manager.h"
#include "terrain/generator/vsn_generator/simd_noise_vs_node.h"
#include "terrain/generator/vsn_generator/noise_mixer.h"

template<class T>
static Ref<VisualScriptNode> create_visual_script_node(const String &p_name) {
	Ref<VisualScriptNode> node = memnew(T);
	return node;
}

void register_voxelcraft_types() {

    ClassDB::register_class<ItemNode>();
    ClassDB::register_class<Terrain>();
    ClassDB::register_class<ViewCube>();
    ClassDB::register_class<Player>();

    ClassDB::register_class<FlatGenerator>();
    ClassDB::register_class<FastNoiseGenerator>();

    ClassDB::register_class<GameManager>();

    ClassDB::register_class<SIMDNoiseVSNode>();
    ClassDB::register_class<NoiseMixerVSNode>();

    VisualScriptLanguage::singleton->add_register_func("noise/generator", create_visual_script_node<SIMDNoiseVSNode>);
    VisualScriptLanguage::singleton->add_register_func("noise/mixer", create_visual_script_node<NoiseMixerVSNode>);
}

void unregister_voxelcraft_types() {
   //nothing to do here
}

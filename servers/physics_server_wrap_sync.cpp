#include "physics_server_wrap_sync.h"

void PhysicsServerSync::mutex_lock() {
    if (mutex != NULL) mutex->lock();
}

void PhysicsServerSync::mutex_unlock() {
    if (mutex != NULL) mutex->unlock();
}

RID PhysicsServerSync::shape_create(PhysicsServer::ShapeType p_shape) {
    mutex->lock();
    RID result = impl->shape_create(p_shape);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::shape_set_data(RID p_shape, const Variant &p_data) {
    mutex->lock();
    impl->shape_set_data(p_shape, p_data);
    mutex->unlock();
}

void PhysicsServerSync::shape_set_custom_solver_bias(RID p_shape, real_t p_bias) {
    mutex->lock();
    impl->shape_set_custom_solver_bias(p_shape, p_bias);
    mutex->unlock();
}

PhysicsServer::ShapeType PhysicsServerSync::shape_get_type(RID p_shape) const {
    mutex->lock();
    PhysicsServer::ShapeType result = impl->shape_get_type(p_shape);
    mutex->unlock();
    return result;
}

Variant PhysicsServerSync::shape_get_data(RID p_shape) const {
    mutex->lock();
    Variant result = impl->shape_get_data(p_shape);
    mutex->unlock();
    return result;
}

real_t PhysicsServerSync::shape_get_custom_solver_bias(RID p_shape) const {
    mutex->lock();
    real_t result = impl->shape_get_custom_solver_bias(p_shape);
    mutex->unlock();
    return result;
}

RID PhysicsServerSync::space_create() {
    mutex->lock();
    RID result = impl->space_create();
    mutex->unlock();
    return result;
}

void PhysicsServerSync::space_set_active(RID p_space, bool p_active) {
    mutex->lock();
    impl->space_set_active(p_space, p_active);
    mutex->unlock();
}

bool PhysicsServerSync::space_is_active(RID p_space) const {
    mutex->lock();
    bool result = impl->space_is_active(p_space);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::space_set_param(RID p_space, PhysicsServer::SpaceParameter p_param, real_t p_value) {
    mutex->lock();
    impl->space_set_param(p_space, p_param, p_value);
    mutex->unlock();
}

real_t PhysicsServerSync::space_get_param(RID p_space, PhysicsServer::SpaceParameter p_param) const {
    mutex->lock();
    real_t result = impl->space_get_param(p_space, p_param);
    mutex->unlock();
    return result;
}

PhysicsDirectSpaceState *PhysicsServerSync::space_get_direct_state(RID p_space) {
    mutex->lock();
    PhysicsDirectSpaceState* result = impl->space_get_direct_state(p_space);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::space_set_debug_contacts(RID p_space, int p_max_contacts) {
    mutex->lock();
    impl->space_set_debug_contacts(p_space, p_max_contacts);
    mutex->unlock();
}

Vector<Vector3> PhysicsServerSync::space_get_contacts(RID p_space) const {
    mutex->lock();
    Vector<Vector3> result = impl->space_get_contacts(p_space);
    mutex->unlock();
    return result;
}

int PhysicsServerSync::space_get_contact_count(RID p_space) const {
    mutex->lock();
    int result = impl->space_get_contact_count(p_space);
    mutex->unlock();
    return result;
}

RID PhysicsServerSync::area_create() {
    mutex->lock();
    RID result = impl->area_create();
    mutex->unlock();
    return result;
}

void PhysicsServerSync::area_set_space_override_mode(RID p_area, PhysicsServer::AreaSpaceOverrideMode p_mode) {
    mutex->lock();
    impl->area_set_space_override_mode(p_area, p_mode);
    mutex->unlock();
}

PhysicsServer::AreaSpaceOverrideMode PhysicsServerSync::area_get_space_override_mode(RID p_area) const {
    mutex->lock();
    PhysicsServer::AreaSpaceOverrideMode result = impl->area_get_space_override_mode(p_area);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::area_set_space(RID p_area, RID p_space) {
    mutex->lock();
    impl->area_set_space(p_area, p_space);
    mutex->unlock();
}

RID PhysicsServerSync::area_get_space(RID p_area) const {
    mutex->lock();
    RID result = impl->area_get_space(p_area);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::area_add_shape(RID p_area, RID p_shape, const Transform &p_transform) {
    mutex->lock();
    impl->area_add_shape(p_area, p_shape, p_transform);
    mutex->unlock();
}

void PhysicsServerSync::area_set_shape(RID p_area, int p_shape_idx, RID p_shape) {
    mutex->lock();
    impl->area_set_shape(p_area, p_shape_idx, p_shape);
    mutex->unlock();
}

void PhysicsServerSync::area_set_shape_transform(RID p_area, int p_shape_idx, const Transform &p_transform) {
    mutex->lock();
    impl->area_set_shape_transform(p_area, p_shape_idx, p_transform);
    mutex->unlock();
}

int PhysicsServerSync::area_get_shape_count(RID p_area) const {
    mutex->lock();
    int result = impl->area_get_shape_count(p_area);
    mutex->unlock();
    return result;
}

RID PhysicsServerSync::area_get_shape(RID p_area, int p_shape_idx) const {
    mutex->lock();
    RID result = impl->area_get_shape(p_area, p_shape_idx);
    mutex->unlock();
    return result;
}

Transform PhysicsServerSync::area_get_shape_transform(RID p_area, int p_shape_idx) const {
    mutex->lock();
    Transform result = impl->area_get_shape_transform(p_area, p_shape_idx);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::area_remove_shape(RID p_area, int p_shape_idx) {
    mutex->lock();
    impl->area_remove_shape(p_area, p_shape_idx);
    mutex->unlock();
}

void PhysicsServerSync::area_clear_shapes(RID p_area) {
    mutex->lock();
    impl->area_clear_shapes(p_area);
    mutex->unlock();
}

void PhysicsServerSync::area_set_shape_disabled(RID p_area, int p_shape_idx, bool p_disabled) {
    mutex->lock();
    impl->area_set_shape_disabled(p_area, p_shape_idx, p_disabled);
    mutex->unlock();
}

void PhysicsServerSync::area_attach_object_instance_id(RID p_area, ObjectID p_ID) {
    mutex->lock();
    impl->area_attach_object_instance_id(p_area, p_ID);
    mutex->unlock();
}

ObjectID PhysicsServerSync::area_get_object_instance_id(RID p_area) const {
    mutex->lock();
    ObjectID result =  impl->area_get_object_instance_id(p_area);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::area_set_param(RID p_area, PhysicsServer::AreaParameter p_param, const Variant &p_value) {
    mutex->lock();
    impl->area_set_param(p_area, p_param, p_value);
    mutex->unlock();
}

void PhysicsServerSync::area_set_transform(RID p_area, const Transform &p_transform) {
    mutex->lock();
    impl->area_set_transform(p_area, p_transform);
    mutex->unlock();
}

Variant PhysicsServerSync::area_get_param(RID p_area, PhysicsServer::AreaParameter p_param) const {
    mutex->lock();
    Variant result = impl->area_get_param(p_area, p_param);
    mutex->unlock();
    return result;
}

Transform PhysicsServerSync::area_get_transform(RID p_area) const {
    mutex->lock();
    Transform result = impl->area_get_transform(p_area);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::area_set_ray_pickable(RID p_area, bool p_enable) {
    mutex->lock();
    impl->area_set_ray_pickable(p_area, p_enable);
    mutex->unlock();
}


bool PhysicsServerSync::area_is_ray_pickable(RID p_area) const {
    mutex->lock();
    bool result = impl->area_is_ray_pickable(p_area);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::area_set_collision_mask(RID p_area, uint32_t p_mask) {
    mutex->lock();
    impl->area_set_collision_mask(p_area, p_mask);
    mutex->unlock();
}

void PhysicsServerSync::area_set_collision_layer(RID p_area, uint32_t p_layer) {
    mutex->lock();
    impl->area_set_collision_layer(p_area, p_layer);
    mutex->unlock();
}

void PhysicsServerSync::area_set_monitorable(RID p_area, bool p_monitorable) {
    mutex->lock();
    impl->area_set_monitorable(p_area, p_monitorable);
    mutex->unlock();
}

void PhysicsServerSync::area_set_monitor_callback(RID p_area, Object *p_receiver, const StringName &p_method) {
    mutex->lock();
    impl->area_set_monitor_callback(p_area, p_receiver, p_method);
    mutex->unlock();
}

void PhysicsServerSync::area_set_area_monitor_callback(RID p_area, Object *p_receiver, const StringName &p_method) {
    mutex->lock();
    impl->area_set_area_monitor_callback(p_area, p_receiver, p_method);
    mutex->unlock();
}

RID PhysicsServerSync::body_create(PhysicsServer::BodyMode p_mode, bool p_init_sleeping) {
    mutex->lock();
    RID result = impl->body_create(p_mode, p_init_sleeping);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::body_set_space(RID p_body, RID p_space) {
    mutex->lock();
    impl->body_set_space(p_body, p_space);
    mutex->unlock();
}

RID PhysicsServerSync::body_get_space(RID p_body) const {
    mutex->lock();
    RID result = impl->body_get_space(p_body);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::body_set_mode(RID p_body, PhysicsServer::BodyMode p_mode) {
    mutex->lock();
    impl->body_set_mode(p_body, p_mode);
    mutex->unlock();
}

PhysicsServer::BodyMode PhysicsServerSync::body_get_mode(RID p_body) const {
    mutex->lock();
    PhysicsServer::BodyMode result = impl->body_get_mode(p_body);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::body_add_shape(RID p_body, RID p_shape, const Transform &p_transform) {
    mutex->lock();
    impl->body_add_shape(p_body, p_shape, p_transform);
    mutex->unlock();
}

void PhysicsServerSync::body_set_shape(RID p_body, int p_shape_idx, RID p_shape) {
    mutex->lock();
    impl->body_set_shape(p_body, p_shape_idx, p_shape);
    mutex->unlock();
}

void PhysicsServerSync::body_set_shape_transform(RID p_body, int p_shape_idx, const Transform &p_transform) {
    mutex->lock();
    impl->body_set_shape_transform(p_body, p_shape_idx, p_transform);
    mutex->unlock();
}

int PhysicsServerSync::body_get_shape_count(RID p_body) const {
    mutex->lock();
    int result = impl->body_get_shape_count(p_body);
    mutex->unlock();
    return result;
}

RID PhysicsServerSync::body_get_shape(RID p_body, int p_shape_idx) const {
    mutex->lock();
    RID result = impl->body_get_shape(p_body, p_shape_idx);
    mutex->unlock();
    return result;
}

Transform PhysicsServerSync::body_get_shape_transform(RID p_body, int p_shape_idx) const {
    mutex->lock();
    Transform result = impl->body_get_shape_transform(p_body, p_shape_idx);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::body_set_shape_disabled(RID p_body, int p_shape_idx, bool p_disabled) {
    mutex->lock();
    impl->body_set_shape_disabled(p_body, p_shape_idx, p_disabled);
    mutex->unlock();
}

void PhysicsServerSync::body_remove_shape(RID p_body, int p_shape_idx) {
    mutex->lock();
    impl->body_remove_shape(p_body, p_shape_idx);
    mutex->unlock();
}

void PhysicsServerSync::body_clear_shapes(RID p_body) {
    mutex->lock();
    impl->body_clear_shapes(p_body);
    mutex->unlock();
}

void PhysicsServerSync::body_attach_object_instance_id(RID p_body, uint32_t p_ID) {
    mutex->lock();
    impl->body_attach_object_instance_id(p_body, p_ID);
    mutex->unlock();
}

uint32_t PhysicsServerSync::body_get_object_instance_id(RID p_body) const {
    mutex->lock();
    uint32_t result = impl->body_get_object_instance_id(p_body);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::body_set_enable_continuous_collision_detection(RID p_body, bool p_enable) {
    mutex->lock();
    impl->body_set_enable_continuous_collision_detection(p_body, p_enable);
    mutex->unlock();
}

bool PhysicsServerSync::body_is_continuous_collision_detection_enabled(RID p_body) const {
    mutex->lock();
    bool result = impl->body_is_continuous_collision_detection_enabled(p_body);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::body_set_collision_layer(RID p_body, uint32_t p_layer) {
    mutex->lock();
    impl->body_set_collision_layer(p_body, p_layer);
    mutex->unlock();
}

uint32_t PhysicsServerSync::body_get_collision_layer(RID p_body) const {
    mutex->lock();
    uint32_t result = impl->body_get_collision_layer(p_body);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::body_set_collision_mask(RID p_body, uint32_t p_mask) {
    mutex->lock();
    impl->body_set_collision_mask(p_body, p_mask);
    mutex->unlock();
}

uint32_t PhysicsServerSync::body_get_collision_mask(RID p_body) const {
    mutex->lock();
    uint32_t result = impl->body_get_collision_mask(p_body);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::body_set_user_flags(RID p_body, uint32_t p_flags) {
    mutex->lock();
    impl->body_set_user_flags(p_body, p_flags);
    mutex->unlock();
}

uint32_t PhysicsServerSync::body_get_user_flags(RID p_body) const {
    mutex->lock();
    uint32_t result = impl->body_get_user_flags(p_body);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::body_set_param(RID p_body, PhysicsServer::BodyParameter p_param, real_t p_value) {
    mutex->lock();
    impl->body_set_param(p_body, p_param, p_value);
    mutex->unlock();
}

real_t PhysicsServerSync::body_get_param(RID p_body, PhysicsServer::BodyParameter p_param) const {
    mutex->lock();
    real_t result = impl->body_get_param(p_body, p_param);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::body_set_state(RID p_body, PhysicsServer::BodyState p_state, const Variant &p_variant) {
    mutex->lock();
    impl->body_set_state(p_body, p_state, p_variant);
    mutex->unlock();
}

Variant PhysicsServerSync::body_get_state(RID p_body, PhysicsServer::BodyState p_state) const {
    mutex->lock();
    Variant result = impl->body_get_state(p_body, p_state);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::body_set_applied_force(RID p_body, const Vector3 &p_force) {
    mutex->lock();
    impl->body_set_applied_force(p_body, p_force);
    mutex->unlock();
}

Vector3 PhysicsServerSync::body_get_applied_force(RID p_body) const {
    mutex->lock();
    Vector3 result = impl->body_get_applied_force(p_body);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::body_set_applied_torque(RID p_body, const Vector3 &p_torque) {
    mutex->lock();
    impl->body_set_applied_torque(p_body, p_torque);
    mutex->unlock();
}

Vector3 PhysicsServerSync::body_get_applied_torque(RID p_body) const {
    mutex->lock();
    Vector3 result = impl->body_get_applied_torque(p_body);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::body_apply_impulse(RID p_body, const Vector3 &p_pos, const Vector3 &p_impulse) {
    mutex->lock();
    impl->body_apply_impulse(p_body, p_pos, p_impulse);
    mutex->unlock();
}

void PhysicsServerSync::body_apply_torque_impulse(RID p_body, const Vector3 &p_impulse) {
    mutex->lock();
    impl->body_apply_torque_impulse(p_body, p_impulse);
    mutex->unlock();
}

void PhysicsServerSync::body_set_axis_velocity(RID p_body, const Vector3 &p_axis_velocity) {
    mutex->lock();
    impl->body_set_axis_velocity(p_body, p_axis_velocity);
    mutex->unlock();
}

/*
void PhysicsServerSync::body_set_axis_lock(RID p_body, PhysicsServer::BodyAxisLock p_lock) {
    mutex->lock();
    impl->body_set_axis_lock(p_body, p_lock);
    mutex->unlock();
}

PhysicsServer::BodyAxisLock PhysicsServerSync::body_get_axis_lock(RID p_body) const {
    mutex->lock();
    PhysicsServer::BodyAxisLock result = impl->body_get_axis_lock(p_body);
    mutex->unlock();
    return result;
}
*/
void PhysicsServerSync::body_add_collision_exception(RID p_body, RID p_body_b) {
    mutex->lock();
    impl->body_add_collision_exception(p_body, p_body_b);
    mutex->unlock();
}

void PhysicsServerSync::body_remove_collision_exception(RID p_body, RID p_body_b) {
    mutex->lock();
    impl->body_remove_collision_exception(p_body, p_body_b);
    mutex->unlock();
}

void PhysicsServerSync::body_get_collision_exceptions(RID p_body, List<RID> *p_exceptions) {
    mutex->lock();
    impl->body_get_collision_exceptions(p_body, p_exceptions);
    mutex->unlock();
}

void PhysicsServerSync::body_set_contacts_reported_depth_threshold(RID p_body, real_t p_threshold) {
    mutex->lock();
    impl->body_set_contacts_reported_depth_threshold(p_body, p_threshold);
    mutex->unlock();
}

real_t PhysicsServerSync::body_get_contacts_reported_depth_threshold(RID p_body) const {
    mutex->lock();
    real_t result = impl->body_get_contacts_reported_depth_threshold(p_body);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::body_set_omit_force_integration(RID p_body, bool p_omit) {
    mutex->lock();
    impl->body_set_omit_force_integration(p_body, p_omit);
    mutex->unlock();
}

bool PhysicsServerSync::body_is_omitting_force_integration(RID p_body) const {
    mutex->lock();
    bool result = impl->body_is_omitting_force_integration(p_body);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::body_set_max_contacts_reported(RID p_body, int p_contacts) {
    mutex->lock();
    impl->body_set_max_contacts_reported(p_body, p_contacts);
    mutex->unlock();
}

int PhysicsServerSync::body_get_max_contacts_reported(RID p_body) const {
    mutex->lock();
    int result = impl->body_get_max_contacts_reported(p_body);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::body_set_force_integration_callback(RID p_body, Object *p_receiver, const StringName &p_method, const Variant &p_udata) {
    mutex->lock();
    impl->body_set_force_integration_callback(p_body, p_receiver, p_method, p_udata);
    mutex->unlock();
}

void PhysicsServerSync::body_set_ray_pickable(RID p_body, bool p_enable) {
    mutex->lock();
    impl->body_set_ray_pickable(p_body, p_enable);
    mutex->unlock();
}

bool PhysicsServerSync::body_is_ray_pickable(RID p_body) const {
    mutex->lock();
    bool result = impl->body_is_ray_pickable(p_body);
    mutex->unlock();
    return result;
}

/*
bool PhysicsServerSync::body_test_motion(RID p_body, const Transform &p_from, const Vector3 &p_motion, float p_margin, PhysicsServer::MotionResult *r_result) {
    mutex->lock();
    bool result = impl->body_test_motion(p_body, p_from, p_motion, p_margin, r_result);
    mutex->unlock();
    return result;
}
*/

RID PhysicsServerSync::joint_create_pin(RID p_body_A, const Vector3 &p_local_A, RID p_body_B, const Vector3 &p_local_B) {
    mutex->lock();
    RID result = impl->joint_create_pin(p_body_A, p_local_A, p_body_B, p_local_B);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::pin_joint_set_param(RID p_joint, PhysicsServer::PinJointParam p_param, real_t p_value) {
    mutex->lock();
    impl->pin_joint_set_param(p_joint, p_param, p_value);
    mutex->unlock();
}

real_t PhysicsServerSync::pin_joint_get_param(RID p_joint, PhysicsServer::PinJointParam p_param) const {
    mutex->lock();
    real_t result = impl->pin_joint_get_param(p_joint, p_param);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::pin_joint_set_local_a(RID p_joint, const Vector3 &p_A) {
    mutex->lock();
    impl->pin_joint_set_local_a(p_joint, p_A);
    mutex->unlock();
}

Vector3 PhysicsServerSync::pin_joint_get_local_a(RID p_joint) const {
    mutex->lock();
    Vector3 result = impl->pin_joint_get_local_a(p_joint);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::pin_joint_set_local_b(RID p_joint, const Vector3 &p_B) {
    mutex->lock();
    impl->pin_joint_set_local_b(p_joint, p_B);
    mutex->unlock();
}

Vector3 PhysicsServerSync::pin_joint_get_local_b(RID p_joint) const {
    mutex->lock();
    Vector3 result = impl->pin_joint_get_local_b(p_joint);
    mutex->unlock();
    return result;
}

RID PhysicsServerSync::joint_create_hinge(RID p_body_A, const Transform &p_frame_A, RID p_body_B, const Transform &p_frame_B) {
    mutex->lock();
    RID result = impl->joint_create_hinge(p_body_A, p_frame_A, p_body_B, p_frame_B);
    mutex->unlock();
    return result;
}

RID PhysicsServerSync::joint_create_hinge_simple(RID p_body_A, const Vector3 &p_pivot_A, const Vector3 &p_axis_A, RID p_body_B, const Vector3 &p_pivot_B, const Vector3 &p_axis_B) {
    mutex->lock();
    RID result = impl->joint_create_hinge_simple(p_body_A, p_pivot_A, p_axis_A, p_body_B, p_pivot_B, p_axis_B);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::hinge_joint_set_param(RID p_joint, PhysicsServer::HingeJointParam p_param, real_t p_value) {
    mutex->lock();
    impl->hinge_joint_set_param(p_joint, p_param, p_value);
    mutex->unlock();
}

real_t PhysicsServerSync::hinge_joint_get_param(RID p_joint, PhysicsServer::HingeJointParam p_param) const {
    mutex->lock();
    real_t result = impl->hinge_joint_get_param(p_joint, p_param);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::hinge_joint_set_flag(RID p_joint, PhysicsServer::HingeJointFlag p_flag, bool p_value) {
    mutex->lock();
    impl->hinge_joint_set_flag(p_joint, p_flag, p_value);
    mutex->unlock();
}

bool PhysicsServerSync::hinge_joint_get_flag(RID p_joint, PhysicsServer::HingeJointFlag p_flag) const {
    mutex->lock();
    bool result = impl->hinge_joint_get_flag(p_joint, p_flag);
    mutex->unlock();
    return result;
}

RID PhysicsServerSync::joint_create_slider(RID p_body_A, const Transform &p_local_frame_A, RID p_body_B, const Transform &p_local_frame_B) {
    mutex->lock();
    RID result = impl->joint_create_slider(p_body_A, p_local_frame_A, p_body_B, p_local_frame_B);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::slider_joint_set_param(RID p_joint, PhysicsServer::SliderJointParam p_param, real_t p_value) {
    mutex->lock();
    impl->slider_joint_set_param(p_joint, p_param, p_value);
    mutex->unlock();
}

real_t PhysicsServerSync::slider_joint_get_param(RID p_joint, PhysicsServer::SliderJointParam p_param) const {
    mutex->lock();
    real_t result = impl->slider_joint_get_param(p_joint, p_param);
    mutex->unlock();
    return result;
}

RID PhysicsServerSync::joint_create_cone_twist(RID p_body_A, const Transform &p_local_frame_A, RID p_body_B, const Transform &p_local_frame_B) {
    mutex->lock();
    RID result = impl->joint_create_cone_twist(p_body_A, p_local_frame_A, p_body_B, p_local_frame_B);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::cone_twist_joint_set_param(RID p_joint, PhysicsServer::ConeTwistJointParam p_param, real_t p_value) {
    mutex->lock();
    impl->cone_twist_joint_set_param(p_joint, p_param, p_value);
    mutex->unlock();
}

real_t PhysicsServerSync::cone_twist_joint_get_param(RID p_joint, PhysicsServer::ConeTwistJointParam p_param) const {
    mutex->lock();
    real_t result = impl->cone_twist_joint_get_param(p_joint, p_param);
    mutex->unlock();
    return result;
}

RID PhysicsServerSync::joint_create_generic_6dof(RID p_body_A, const Transform &p_local_frame_A, RID p_body_B, const Transform &p_local_frame_B) {
    mutex->lock();
    RID result = impl->joint_create_generic_6dof(p_body_A, p_local_frame_A, p_body_B, p_local_frame_B);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::generic_6dof_joint_set_param(RID p_joint, Vector3::Axis p_axis, PhysicsServer::G6DOFJointAxisParam p_param, real_t p_value) {
    mutex->lock();
    impl->generic_6dof_joint_set_param(p_joint, p_axis, p_param, p_value);
    mutex->unlock();
}

real_t PhysicsServerSync::generic_6dof_joint_get_param(RID p_joint, Vector3::Axis p_axis, PhysicsServer::G6DOFJointAxisParam p_param) {
    mutex->lock();
    real_t result = impl->generic_6dof_joint_get_param(p_joint, p_axis, p_param);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::generic_6dof_joint_set_flag(RID p_joint, Vector3::Axis p_axis, PhysicsServer::G6DOFJointAxisFlag p_flag, bool p_enable) {
    mutex->lock();
    impl->generic_6dof_joint_set_flag(p_joint, p_axis, p_flag, p_enable);
    mutex->unlock();
}

bool PhysicsServerSync::generic_6dof_joint_get_flag(RID p_joint, Vector3::Axis p_axis, PhysicsServer::G6DOFJointAxisFlag p_flag) {
    mutex->lock();
    bool result = impl->generic_6dof_joint_get_flag(p_joint, p_axis, p_flag);
    mutex->unlock();
    return result;
}

PhysicsServer::JointType PhysicsServerSync::joint_get_type(RID p_joint) const {
    mutex->lock();
    PhysicsServer::JointType result = impl->joint_get_type(p_joint);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::joint_set_solver_priority(RID p_joint, int p_priority) {
    mutex->lock();
    impl->joint_set_solver_priority(p_joint, p_priority);
    mutex->unlock();
}

int PhysicsServerSync::joint_get_solver_priority(RID p_joint) const {
    mutex->lock();
    int result = impl->joint_get_solver_priority(p_joint);
    mutex->unlock();
    return result;
}

void PhysicsServerSync::free(RID p_rid) {
    mutex->lock();
    impl->free(p_rid);
    mutex->unlock();
}

void PhysicsServerSync::set_active(bool p_active) {
    mutex->lock();
    impl->set_active(p_active);
    mutex->unlock();
}

void PhysicsServerSync::init() {
    mutex->lock();
    impl->init();
    mutex->unlock();
}

void PhysicsServerSync::step(real_t p_step) {
    mutex->lock();
    impl->step(p_step);
    mutex->unlock();
}

void PhysicsServerSync::sync() {
    mutex->lock();
    impl->sync();
    mutex->unlock();
}

void PhysicsServerSync::flush_queries() {
    mutex->lock();
    impl->flush_queries();
    mutex->unlock();
}

void PhysicsServerSync::finish() {
    mutex->lock();
    impl->finish();
    mutex->unlock();
}

int PhysicsServerSync::get_process_info(PhysicsServer::ProcessInfo p_info) {
    mutex->lock();
    int result = impl->get_process_info(p_info);
    mutex->unlock();
    return result;
}

Mutex* PhysicsServerSync::mutex = NULL;

PhysicsServerSync::PhysicsServerSync(PhysicsServer* server) {
    mutex = Mutex::create();
    impl = server;
}

PhysicsServerSync::~PhysicsServerSync() {
    memdelete(mutex);
    memdelete(impl);
    mutex = NULL;
}

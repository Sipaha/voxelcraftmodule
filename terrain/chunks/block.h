#ifndef BLOCK_H
#define BLOCK_H

#include "typedefs.h"
#include "reference.h"
#include "scene/resources/material.h"
#include "../library/atlas_region.h"
#include "../../constants.h"

struct BlockInfo {
	int id;
	Ref<Material> material;
	RES shader;
	const AtlasRegion* faces[SIDES_CONTACT_COUNT];

	BlockInfo() : faces() {}
};

struct Block {
    uint16_t id = 0;
    int8_t temperature = 0;

    _FORCE_INLINE_ bool operator ==(const Block& other) const {
        return id == other.id && temperature == other.temperature;
    }

    _FORCE_INLINE_ bool operator !=(const Block& other) const {
        return id != other.id || temperature != other.temperature;
    }
};

#endif // BLOCK_H

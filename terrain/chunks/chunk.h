#ifndef CHUNK_H
#define CHUNK_H

#include "typedefs.h"

#include "../../utils/structs/int3.h"
#include "../../utils/reference/simple_reference.h"
#include "chunk_blocks.h"
#include "chunk_mesh.h"
#include "boost/atomic.hpp"
#include "string.h"
#include "boost/intrusive_ptr.hpp"
#include "../../constants.h"
#include "chunk_light.h"
#include "chunk_flags.h"
#include "chunk_locks.h"
#include "chunk_life.h"
#include "chunk_commands.h"
#include "../../constants.h"
#include "chunk_utils.h"

class Chunk {

public:

    const Int3 position;
    boost::atomic<uint32_t> references {0};

    ChunkMesh mesh;
    ChunkLife life;
    ChunkFlags flags;
    ChunkLight light;
    ChunkLocks locks;
    ChunkBlocks blocks;
    ChunkCommands commands;

    uint32_t transparency = 0;
    uint8_t n_blocks_transparency[N_BLOCKS_TRANSPARENCY_SIZE] {0};
    uint8_t loaded_neighbours = 0;

    void release_processing_data() {
        light.release_processing_data();
    }

    _FORCE_INLINE_ bool operator ==(Chunk& chunk) {
        return position == chunk.position;
    }

    static _FORCE_INLINE_ uint32_t hash(const Chunk* const &chunk) {
        return chunk != NULL ? Int3::hash(chunk->position) : 0;
    }

    static _FORCE_INLINE_ bool compare(const Chunk* const &p_lhs, const Chunk* const &p_rhs) {
        return p_lhs->position == p_rhs->position;
    }

    String to_string() {
        Array values;
        values.append(Variant(position.x));
        values.append(Variant(position.y));
        values.append(Variant(position.z));
        values.append(Variant(flags.get()));
        return String("Chunk[({0},{1},{2}),{3}]").format(values);
    }

    Chunk(const Int3& position) : position(position),
                                  commands(this, &locks),
                                  light(&flags),
                                  blocks(&light) {
    }

    ~Chunk() {

    }
};

typedef boost::intrusive_ptr<Chunk> ChunkRef;

#endif // CHUNK_H

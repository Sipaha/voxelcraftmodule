#ifndef CHUNK_BLOCKS_H
#define CHUNK_BLOCKS_H

#include "typedefs.h"
#include "dvector.h"
#include "block.h"
#include "../../utils/collections/cache.h"

#include "chunk_light.h"

class ChunkBlocks {

public:

    uint32_t changed_neighbours = 0;
    bool data_changed = false;
    bool is_not_empty = false;

private:

    static const size_t BLOCKS_BYTES_SIZE = CHUNK_BLOCKS_COUNT * sizeof(Block);

    PoolVector<Block> blocks;
    bool isCached = false;
    ChunkLight* chunk_light;

    mutable uint32_t _hash = 0;

    _FORCE_INLINE_ void update_neighbours(int coord, uint32_t min_side, uint32_t max_side) {
        if (coord == 0) {
            changed_neighbours |= min_side;
        } else if (coord == CHUNK_BLOCKS_SIZE - 1) {
            changed_neighbours |= max_side;
        }
    }

public:

    PoolVector<Block>::Read read;
    PoolVector<Block>::Write write;

    _FORCE_INLINE_ bool is_empty() {
        return !is_not_empty;
    }

    _FORCE_INLINE_ const Block& get_block(block_ipos_t idx) const {
        return read[idx];
    }

    _FORCE_INLINE_ const Block& get_block(Int3& pos) {
        return get_block(vxl_t::encode_block_ipos(pos.x, pos.y, pos.z));
    }

    _FORCE_INLINE_ const Block& get_block(int x, int y, int z) const {
        return get_block(vxl_t::encode_block_ipos(x, y, z));
    }

    _FORCE_INLINE_ void set_block(Int3& pos, Block& block) {
        set_block(pos.x, pos.y, pos.z, block);
    }

    _FORCE_INLINE_ void set_block(int x, int y, int z, Block& block) {
        block_ipos_t idx = vxl_t::encode_block_ipos(x, y, z);
        if (set_block_impl(idx, block)) {
            data_changed = true;
            update_neighbours(x, vxl_t::flags[SIDE_NZZ], vxl_t::flags[SIDE_PZZ]);
            update_neighbours(y, vxl_t::flags[SIDE_ZNZ], vxl_t::flags[SIDE_ZPZ]);
            update_neighbours(z, vxl_t::flags[SIDE_ZZN], vxl_t::flags[SIDE_ZZP]);
        }
    }

    _FORCE_INLINE_ bool set_block_impl(block_ipos_t idx, const Block& block) {
        if (read[idx] != block) {
            write[idx] = block;
            if (block.id == 2) {
                chunk_light->add_internal_light_source(idx, LightSource(1, 0, 0));
            }
            if (block.id == 3) {
                chunk_light->add_internal_light_source(idx, LightSource(1, 1, 1));
            }
            is_not_empty = is_not_empty || block.id != 0;
            return true;
        }
        return false;
    }

    void cache() {
        //ChunkBlocks& cached = chunks.blocks_cache.add(this);
        //blocks = cached.blocks;
        //isCached = true;
        //read = blocks.read();
    }

    static _FORCE_INLINE_ uint32_t hash(const ChunkBlocks& p_blocks) {
        if (p_blocks._hash == 0) {
            p_blocks._hash = hash_djb2_buffer(reinterpret_cast<const uint8_t*>(p_blocks.read.ptr()), BLOCKS_BYTES_SIZE);
        }
        return p_blocks._hash;
    }

    static _FORCE_INLINE_ bool compare(const ChunkBlocks& p_lhs, const ChunkBlocks& p_rhs) {
        return memcmp(p_lhs.read.ptr(), p_rhs.read.ptr(), BLOCKS_BYTES_SIZE) == 0;
    }

    void lock() {
        isCached = false;
        write = blocks.write();
        read = blocks.read();
    }

    void unlock() {
        write = PoolVector<Block>::Write();
        read = blocks.read();
        if (data_changed) {
            _hash = 0;
        }
    }

    ChunkBlocks(ChunkLight* chunk_light) : chunk_light(chunk_light) {
        blocks.resize(CHUNK_BLOCKS_COUNT);
        write = blocks.write();
        memset(write.ptr(), 0, BLOCKS_BYTES_SIZE);
        write = PoolVector<Block>::Write();
        read = blocks.read();
    }

    ChunkBlocks(const ChunkBlocks& other) {
        blocks = other.blocks;
        read = blocks.read();
        //chunks = other.chunks;
    }

    ~ChunkBlocks() {
        if (isCached) {
            //get_cache()->remove(this);
            isCached = false;
        }
    }
};

#endif // CHUNK_BLOCKS_H

#include "chunk_commands.h"

#include "typedefs.h"
#include "chunk.h"
#include "../controller/commands/commands.h"

int ChunkCommands::invoke_all() {

    int priority = 999999;

    locks->lock(ChunkLocks::LOCK_COMMANDS);
    List<Command*>::Element *iter = commands.front();

    while (iter != NULL) {

        Command* comm = iter->get();
        iter->erase();
        locks->unlock(ChunkLocks::LOCK_COMMANDS);

        priority = MIN(priority, comm->priority);
        comm->call(owner);
        memdelete(comm);

        locks->lock(ChunkLocks::LOCK_COMMANDS);
        iter = commands.front();
    }
    locks->unlock(ChunkLocks::LOCK_COMMANDS);

    return priority;
}

#ifndef CHUNK_COMMANDS_H
#define CHUNK_COMMANDS_H

#include "list.h"
#include "chunk_locks.h"

namespace vxl {
    namespace commands {
        class Command;
    }
}
class Chunk;

using namespace vxl::commands;

class ChunkCommands {

    List<Command*> commands;
    ChunkLocks* locks;
    Chunk* owner;

public:

    void add(Command* comm) {
        locks->lock(ChunkLocks::LOCK_COMMANDS);
        commands.push_back(comm);
        locks->unlock(ChunkLocks::LOCK_COMMANDS);
    }

    int invoke_all();

    ChunkCommands(Chunk* owner, ChunkLocks* locks) : owner(owner),
                                                     locks(locks) {
    }
};

#endif // CHUNKCOMMANDS_H

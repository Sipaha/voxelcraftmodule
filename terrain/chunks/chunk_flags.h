#ifndef CHUNK_FLAGS_H
#define CHUNK_FLAGS_H

#include "typedefs.h"
#include "boost/atomic.hpp"

class ChunkFlags {

public:

    enum Flags {
        NOT_EMPTY =      1 << 0,
        VISIBLE =        1 << 1,
        LOADED =         1 << 2,
        LIGHTED =        1 << 3,
        MESHED =         1 << 4,
        ON_SCENE =       1 << 5,
        ON_PROCESSING =  1 << 6,
        IS_PROCESSED =   Flags::LIGHTED | Flags::MESHED | Flags::ON_SCENE,
        OBS_IS_FINAL1 =  1 << 7,
        OBS_IS_FINAL2 =  1 << 8
    };

private:

    boost::atomic<uint32_t> flags {0};

public:

    _FORCE_INLINE_ bool get(uint32_t f) {
        return (flags & f) == f;
    }

    //return true if set is successfull
    //return false if flag was set before
    _FORCE_INLINE_ bool set(uint32_t f) {
        return (flags.fetch_or(f) & f) != f;
    }

    _FORCE_INLINE_ void unset(uint32_t f) {
        flags.fetch_and(~f);
    }

    _FORCE_INLINE_ uint32_t get() {
        return flags;
    }

};

#endif // CHUNK_FLAGS_H

#ifndef CHUNK_LIFE_H
#define CHUNK_LIFE_H

#include "typedefs.h"

class ChunkLife {

public:

    enum State {
        ALIVE,
        WAITING_UNLOAD,
        WAITING_DELETE
    };

private:

    State state = ALIVE;

public:

    _FORCE_INLINE_ State get_state() {
        return state;
    }

    _FORCE_INLINE_ void set_state(State state) {
        this->state = state;
    }

};

#endif // CHUNK_LIFE_H

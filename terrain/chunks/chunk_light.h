#ifndef CHUNK_LIGHT_H
#define CHUNK_LIGHT_H

#include "../../constants.h"
#include "hash_map.h"
#include "list.h"
#include "../controller/light/light.h"
#include "chunk_flags.h"
#include "chunk_utils.h"

class ChunkLight {

    ChunkFlags* flags;

public:

    NeighbourLight* n_lights[SIDES_CONTACT_COUNT] { NULL };
    NeighbourSunLight* sun_light[SIDES_CONTACT_COUNT] { NULL };

    HashMap<block_ipos_t, LightSource> int_light;

    void add_internal_light_source(block_ipos_t pos, LightSource& light) {
        int_light[pos] = light;
    }

    void set_neighbour_light(uint8_t side, NeighbourLight* new_n_light, NeighbourSunLight* new_sun_light) {

        NeighbourLight* ex_n_light = n_lights[side];

        bool changed = false;
        if (!NeighbourLight::equals(ex_n_light, new_n_light)) {
            if (ex_n_light != NULL) {
                memdelete(ex_n_light);
            }
            n_lights[side] = new_n_light;
            changed = true;
        } else if (new_n_light != NULL) {
            memdelete(new_n_light);
        }

        NeighbourSunLight* ex_s_light = sun_light[side];

        if (!NeighbourSunLight::equals(ex_s_light, new_sun_light)) {
            if (ex_s_light != NULL) {
                memdelete(ex_s_light);
            }
            sun_light[side] = new_sun_light;
            changed = true;
        } else if (new_sun_light != NULL) {
            memdelete(new_sun_light);
        }

        if (changed) {
            flags->unset(ChunkFlags::IS_PROCESSED);
        }
    }

    void release_processing_data() {

    }

    void fill_light_sources(Set<uint16_t>& sources, LightData* light_map) {

        //==========INTERNAL===========

        const block_ipos_t* pos_idx = NULL;

        while (pos_idx = int_light.next(pos_idx)) {

            LightSource light_source = int_light[*pos_idx];

            Int3 pos = vxl_t::decode_block_ipos(*pos_idx);
            int light_idx = encode_light_block_idx(pos);

            light_map[light_idx].set_color(light_source);
            sources.insert(light_idx);
        }
        //=========/INTERNAL===========

        //==========EXTERNAL===========

        for (int side = 0; side < SIDES_CONTACT_COUNT; side++) {

            NeighbourLight* n_light = n_lights[side];

            if (n_light == NULL) {
                continue;
            }

            SideBlocksIterator side_it(side, true);

            while (side_it.hasNext()) {

                LightSource source = n_light->data[side_it.get_idx()];

                if (!source.isEmpty()) {
                    uint16_t light_idx = encode_light_block_idx(side_it.get_pos());
                    light_map[light_idx].set_color(source);
                    sources.insert(light_idx);
                }

                side_it.next();
            }
        }

        //=========/EXTERNAL===========

        //=========SUNLIGHT===========

        for (int side = 0; side < SIDES_CONTACT_COUNT; side++) {

            NeighbourSunLight* n_sun_light = sun_light[side];

            if (n_sun_light == NULL) {
                continue;
            }

            SideBlocksIterator side_it(side, true);

            while (side_it.hasNext()) {

                uint8_t source = n_sun_light->data[side_it.get_idx()];

                if (source > 0) {
                    uint16_t light_idx = encode_light_block_idx(side_it.get_pos());
                    light_map[light_idx].set_sun(source);
                }

                side_it.next();
            }
        }

        //=========/SUNLIGHT===========
    }

    static uint16_t encode_light_block_idx(const Int3& pos) {
        return (pos.x + 1) * CHUNK_BLOCKS_OUSIDE_SIZE * CHUNK_BLOCKS_OUSIDE_SIZE +
               (pos.y + 1) * CHUNK_BLOCKS_OUSIDE_SIZE +
               (pos.z + 1);
    }

    static Int3 decode_light_block_idx(const uint16_t idx) {
        uint16_t left = idx;
        int x = left / (CHUNK_BLOCKS_OUSIDE_SIZE * CHUNK_BLOCKS_OUSIDE_SIZE);
        left = left % (CHUNK_BLOCKS_OUSIDE_SIZE * CHUNK_BLOCKS_OUSIDE_SIZE);
        int y = left / CHUNK_BLOCKS_OUSIDE_SIZE;
        left = left % CHUNK_BLOCKS_OUSIDE_SIZE;
        int z = left;
        return Int3(x - 1, y - 1, z - 1);
    }

    ChunkLight(ChunkFlags* flags) : flags(flags) {
    }
};

#endif // CHUNK_LIGHT_H

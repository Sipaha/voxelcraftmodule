#ifndef CHUNK_LOCKS_H
#define CHUNK_LOCKS_H

#include "typedefs.h"
#include "boost/atomic.hpp"

class ChunkLocks {

public:

    enum Locks {
        LOCK_COMMANDS = 1 << 0,
        LOCK_BLOCKS =   1 << 1
    };

private:

    boost::atomic<uint8_t> locks {0};

public:

    _FORCE_INLINE_ bool try_lock(Locks type) {
        return (locks.fetch_or(type, boost::memory_order_acquire) & type) == 0;
    }

    _FORCE_INLINE_ void lock(Locks type) {
        while (!try_lock(type)) {
            //busy wait
        }
    }

    _FORCE_INLINE_ void unlock(Locks type) {
        locks.fetch_and(~type);
    }
};

#endif // CHUNK_LOCKS_H

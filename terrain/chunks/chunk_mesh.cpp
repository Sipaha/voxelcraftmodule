#include "chunk_mesh.h"
#include "../terrain.h"

bool ChunkMesh::Vertex::operator==(const Vertex &p_vertex) const {

	if (vertex != p_vertex.vertex)
		return false;

	if (uv != p_vertex.uv)
		return false;

//	if (uv2 != p_vertex.uv2)
	//	return false;

	if (normal != p_vertex.normal)
		return false;

//	if (binormal != p_vertex.binormal)
//		return false;

	if (color != p_vertex.color)
		return false;

//	if (bones.size() != p_vertex.bones.size())
//		return false;

//	for (int i = 0; i < bones.size(); i++) {
//		if (bones[i] != p_vertex.bones[i])
//			return false;
//	}

//	for (int i = 0; i < weights.size(); i++) {
//		if (weights[i] != p_vertex.weights[i])
//			return false;
//	}

	return true;
}


void ChunkMesh::build_mesh() {

    mesh = surface->commit();
    surface->clear();
    memdelete(surface);
    surface = NULL;

    if (mesh.is_valid()) {

        if (mesh_instance == NULL) {
            mesh_instance = memnew(MeshInstance);
            Terrain::add_child_node(mesh_instance);
        }

        mesh_instance->set_mesh(mesh);
    }
}

void ChunkMesh::build_physics() {

    PhysicsServer *ps = PhysicsServer::get_singleton();

    if (faces->size() > 0) {
        if (!ph_shape.is_valid()) {
            ph_shape = ps->shape_create(PhysicsServer::SHAPE_CONCAVE_POLYGON);
        }
        ps->shape_set_data(ph_shape, *faces);
        if (!ph_body.is_valid()) {
            ph_body = ps->body_create(PhysicsServer::BODY_MODE_STATIC, false);
            ps->body_add_shape(ph_body, ph_shape);
            ps->body_set_space(ph_body, Terrain::get_world()->get_space());
        } else {
            ps->body_set_shape(ph_body, 0, ph_shape);
        }
    } else {
        empty = true;
        if (ph_shape.is_valid()) {
            ps->free(ph_shape);
            ph_shape = RID();
        }
        if (ph_body.is_valid()) {
            ps->free(ph_body);
            ph_body = RID();
        }
    }

    memdelete_notnull(faces);
    faces = NULL;
}

ChunkMesh::~ChunkMesh()
{
    clear();
    if (ph_body.is_valid()) {
        Chunks::free_physics_rid(ph_body);
    }
    if (ph_shape.is_valid()) {
        Chunks::free_physics_rid(ph_shape);
    }
    if (mesh_instance != NULL) {
        mesh_instance->queue_delete();
    }
}

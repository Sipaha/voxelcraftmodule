#ifndef MESH_BUILDER_H
#define MESH_BUILDER_H

#include "reference.h"
#include "scene/resources/concave_polygon_shape.h"
#include "servers/physics_server.h"
#include "scene/resources/surface_tool.h"
#include "scene/resources/material.h"
#include "scene/3d/mesh_instance.h"

class ChunkMesh {

public:

	struct Vertex {

		Vector3 vertex;
		Color color;
		Vector3 normal; // normal, binormal, tangent
		//Vector3 binormal;
		//Vector3 tangent;
		Vector2 uv;
		//Vector2 uv2;

		bool operator==(const Vertex &p_vertex) const;

		Vertex() {}
	};

private:

	RID ph_shape;
	RID ph_body;

	List<Vertex>* vertex_array = NULL;

	PoolVector<Vector3>* faces = NULL;

	bool empty = true;

	void _generate_faces() {

		faces = memnew(PoolVector<Vector3>);

		faces->resize(vertex_array->size());

		PoolVector<Vector3>::Write write = faces->write();
		Vector3 *write_ptr = write.ptr();
		List<Vertex>::Element *el = vertex_array->front();

		int idx = 0;
		while (el) {
			write_ptr[idx++] = el->get().vertex;
			el = el->next();
		}
    }

public:

    Ref<ArrayMesh> mesh;
    MeshInstance* mesh_instance = NULL;

    SurfaceTool* surface = NULL;

    void add_vertex(Vertex& vertex) {
        empty = false;
        vertex_array->push_back(vertex);
        surface->add_uv(vertex.uv);
        surface->add_normal(vertex.normal);
        surface->add_color(vertex.color);
        surface->add_vertex(vertex.vertex);
    }

    void clear() {
        memdelete_notnull(vertex_array);
        vertex_array = NULL;
        memdelete_notnull(faces);
        faces = NULL;
        if (surface != NULL) {
            surface->clear();
            memdelete(surface);
            surface = NULL;
        }
    }

    bool is_empty() {
        return empty;
    }

    void begin(Ref<Material> material) {
        clear();
        vertex_array = memnew(List<Vertex>);
        surface = memnew(SurfaceTool);
        surface->begin(Mesh::PRIMITIVE_TRIANGLES);
        surface->set_material(material);
    }

    void end() {
        if (!empty) {
            _generate_faces();
        }
        memdelete_notnull(vertex_array);
        vertex_array = NULL;
    }

    void build_mesh();

    void build_physics();

    ChunkMesh() {}

    ~ChunkMesh();

};

#endif // MESH_BUILDER_H

#ifndef CHUNK_UTILS_H
#define CHUNK_UTILS_H

#include "typedefs.h"
#include "../../utils/structs/int3.h"
#include "../../constants.h"

struct SideBlocksIterator {

private:

    int* it0 = NULL;
    int* it1 = NULL;

    uint16_t idx = 0;

    int x = 0;
    int y = 0;
    int z = 0;

public:

    SideBlocksIterator(uint8_t side, bool outside = false) {

        switch (side) {
        case SIDE_PZZ:
            it0 = &y;
            it1 = &z;
            x = outside ? CHUNK_BLOCKS_SIZE : CHUNK_BLOCKS_SIZE - 1;
            break;
        case SIDE_NZZ:
            it0 = &y;
            it1 = &z;
            x = outside ? -1 : 0;
            break;
        case SIDE_ZPZ:
            it0 = &x;
            it1 = &z;
            y = outside ? CHUNK_BLOCKS_SIZE : CHUNK_BLOCKS_SIZE - 1;
            break;
        case SIDE_ZNZ:
            it0 = &x;
            it1 = &z;
            y = outside ? -1 : 0;
            break;
        case SIDE_ZZP:
            it0 = &x;
            it1 = &y;
            z = outside ? CHUNK_BLOCKS_SIZE : CHUNK_BLOCKS_SIZE - 1;
            break;
        case SIDE_ZZN:
            it0 = &x;
            it1 = &y;
            z = outside ? -1 : 0;
            break;
        }
    }

    _FORCE_INLINE_ int get_x() {
        return x;
    }

    _FORCE_INLINE_ int get_y() {
        return y;
    }

    _FORCE_INLINE_ int get_z() {
        return z;
    }

    _FORCE_INLINE_ int get_idx() {
        return idx;
    }

    _FORCE_INLINE_ Int3 get_pos() {
        return Int3(x, y, z);
    }

    _FORCE_INLINE_ bool hasNext() {
        return idx < CHUNK_SIDE_BLOCKS_COUNT;
    }

    _FORCE_INLINE_ void next() {
        if (++(*it1) >= CHUNK_BLOCKS_SIZE) {
            *it1 = 0;
            ++(*it0);
        }
        idx++;
    }

};

struct NeighbourTransparencyIterator {

private:

    SideBlocksIterator side_it;

public:

    uint8_t* byte;
    uint8_t bit_idx = 0;

    NeighbourTransparencyIterator(uint8_t side, uint8_t* transparency_data) : side_it(SideBlocksIterator(side)) {
        byte = transparency_data;
    }

    _FORCE_INLINE_ int get_x() {
        return side_it.get_x();
    }

    _FORCE_INLINE_ int get_y() {
        return side_it.get_y();
    }

    _FORCE_INLINE_ int get_z() {
        return side_it.get_z();
    }

    _FORCE_INLINE_ int get_idx() {
        return side_it.get_idx();
    }

    _FORCE_INLINE_ Int3 get_pos() {
        return side_it.get_pos();
    }

    _FORCE_INLINE_ bool hasNext() {
        return side_it.hasNext();
    }

    _FORCE_INLINE_ void next() {
        side_it.next();
        if (++bit_idx >= 8) {
            byte++;
            bit_idx = 0;
        }
    }

};

struct ChunkBlocksIterator {

private:

    int x = 0;
    int y = 0;
    int z = 0;
    uint16_t index = 0;

public:

    _FORCE_INLINE_ bool hasNext() {
        return index < CHUNK_BLOCKS_COUNT;
    }

    _FORCE_INLINE_ void next() {
        index += 1;
        if (++z >= CHUNK_BLOCKS_SIZE) {
            z = 0;
            if (++y >= CHUNK_BLOCKS_SIZE) {
                y = 0;
                x++;
            }
        }
    }

    _FORCE_INLINE_ int get_x() {
        return x;
    }

    _FORCE_INLINE_ int get_y() {
        return y;
    }

    _FORCE_INLINE_ int get_z() {
        return z;
    }

    _FORCE_INLINE_ int get_idx() {
        return index;
    }

    _FORCE_INLINE_ Int3 get_pos() {
        return Int3(x, y, z);
    }

};

#endif // CHUNK_UTILS_H

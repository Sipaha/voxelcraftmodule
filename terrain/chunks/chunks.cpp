#include "chunks.h"
#include "../controller/chunks_controller.h"

#define CHUNK_LIFETIME 3

Chunks* Chunks::instance;

ChunkRef Chunks::_raw_get_impl(const Int3 &pos) {

    ChunkRef* chunk_ptr = data->getptr(pos);
    ChunkRef chunk;
    if (chunk_ptr != NULL) {
        chunk = *chunk_ptr;
        chunk->life.set_state(ChunkLife::ALIVE);
    }

    return chunk;

}

ChunkRef Chunks::_raw_get_or_add_impl(const Int3 &pos) {

    load_mutex->lock();

    ChunkRef chunk = _raw_get_impl(pos);

    if (chunk.get() == NULL) {

        chunk = ChunkRef(memnew(Chunk(pos)));
        data->put(pos, chunk);
    }

    load_mutex->unlock();

    return chunk;
}

ChunkRef Chunks::_raw_load_impl(const Int3 &pos) {

    ChunkRef chunk = _raw_get_or_add_impl(pos);

    controller->load_chunk(chunk);

    return chunk;
}

void Chunks::_unreference_impl(Chunk *chunk) {

    load_mutex->lock();

    if (chunk->references == 1) {

        switch (chunk->life.get_state()) {

        case ChunkLife::ALIVE:

            chunk->life.set_state(ChunkLife::WAITING_UNLOAD);

            if (!exit_mode) {
                data->erase(chunk->position);
            }

            //unload_queue.add(ChunkRef(chunk), CHUNK_LIFETIME);

            break;

        case ChunkLife::WAITING_UNLOAD:

            chunk->life.set_state(ChunkLife::WAITING_DELETE);
            //unloader->process(ChunkRef(chunk));
            //ChunkRef ref = ChunkRef(chunk);

            break;

        case ChunkLife::WAITING_DELETE:

            data->erase(chunk->position);
            memdelete(chunk);

            break;
        }
    }

    load_mutex->unlock();
}

void Chunks::update(float delta) {

    RID rid;
    while (physics_rids_to_remove.pop(rid)) {
        PhysicsServer::get_singleton()->free(rid);
    }

    //unload_queue.update(delta);

    controller->main_th_update();
}

void Chunks::execute(Int3& key, Command* command, uint8_t priority) {
    instance->controller->execute(key, command, priority);
}

Chunks::Chunks() {
    instance = this;
    data = memnew(DataStorage);
    controller = memnew(ChunksController(3, 3));
    load_mutex = Mutex::create();
}

Chunks::~Chunks() {
    exit_mode = true;
    data->clear();
    memdelete(data);
    memdelete(controller);
    memdelete(load_mutex);
}

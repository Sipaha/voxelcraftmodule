#ifndef CHUNKS_H
#define CHUNKS_H

#include "typedefs.h"
#include "../../utils/collections/concurrent_map.h"
#include "../../utils/collections/concurrent_list.h"
#include "../../utils/collections/waiting_queue.h"
#include "../controller/commands/commands.h"
#include "os/memory.h"
#include "chunk.h"

#include "boost/lockfree/queue.hpp"

class Chunks {

private:

    friend class ChunksController;

    static Chunks* instance;
    typedef ConcurrentMap<Int3, ChunkRef> DataStorage;

    //WaitingQueue<ChunkRef> unload_queue;
    DataStorage* data;
    ConcurrentList<ChunkRef> to_remove;
    Mutex* load_mutex;

    //Cache<ChunkBlocks> blocks_cache;

    boost::atomic<bool> exit_mode {false};

    boost::lockfree::queue<RID> physics_rids_to_remove {100};

    ChunkRef _raw_get_impl(const Int3& pos);
    ChunkRef _raw_get_or_add_impl(const Int3& pos);
    ChunkRef _raw_load_impl(const Int3& pos);

    void _unreference_impl(Chunk* chunk);

    ChunksController* controller;

public:

    _FORCE_INLINE_ static void free_physics_rid(RID& rid) {
        instance->physics_rids_to_remove.push(rid);
    }

    _FORCE_INLINE_ static void _unreference(Chunk* chunk) {
        instance->_unreference_impl(chunk);
    }

    _FORCE_INLINE_ static ChunkRef get(const Int3& pos) {
        return instance->_raw_get_impl(pos);
    }

    _FORCE_INLINE_ static ChunkRef get_or_add(const Int3& pos) {
        return instance->_raw_get_or_add_impl(pos);
    }

    _FORCE_INLINE_ static ChunkRef load(const Int3& pos) {
        return instance->_raw_load_impl(pos);
    }

    static void execute(Int3& key, vxl::commands::Command* command, uint8_t priority = 1);

    void update(float delta);

    Chunks();
    ~Chunks();
};

inline void intrusive_ptr_add_ref(Chunk* chunk) {
    chunk->references.fetch_add(1, boost::memory_order_relaxed);
}

inline void intrusive_ptr_release(Chunk* chunk){
    if (chunk->references.fetch_sub(1, boost::memory_order_release) < 3) {
        boost::atomic_thread_fence(boost::memory_order_acquire);
        if (chunk->references == 1) {
            Chunks::_unreference(chunk);
        } else {
            memdelete(chunk);
        }
    }
}

#endif // CHUNKS_H

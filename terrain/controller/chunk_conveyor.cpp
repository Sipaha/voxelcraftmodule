#include "chunk_conveyor.h"
#include "processor/chunk_processor.h"
#include "print_string.h"

bool ChunkConveyor::process() {

    ProcessingData proc_data;

    for (int priority = 0; priority < priorities; ++priority) {

        ChunkRef* chunkRef;

        int not_ready_count = 0;

        while (input[priority]->pop(chunkRef)) {

            ChunkProcessor::Status status = ChunkProcessor::CONTINUE;

            for (int i = 0, ii = processors.size(); i < ii; i++) {
                status = processors[i]->process(chunkRef, priority, proc_data);
                if (status != ChunkProcessor::CONTINUE) {
                    break;
                }
            }

            if (status == ChunkProcessor::QUEUE_BACK) {
                input[priority]->push(chunkRef);
                if (not_ready_count++ < 5) {
                    continue;
                } else {
                    break;
                }
            }

            input_size.fetch_sub(1);
            return true;
        }

    }
    return false;
}

#ifndef CHUNK_CONVEYOR_H
#define CHUNK_CONVEYOR_H

#include "typedefs.h"

#include "vector.h"
#include "reference.h"
#include "../../utils/structs/int3.h"
#include "../../utils/threads/thread_pool.h"
#include "../../utils/collections/ordered_hset.h"
#include "../../utils/collections/concurrent_list.h"
#include "print_string.h"
#include "processor/chunk_processor.h"
#include "boost/lockfree/queue.hpp"

using namespace boost;
using namespace boost::lockfree;

#define PRIORITIES_COUNT 4

class ChunkConveyor {

private:

    class ConveyorPush : public ChunkProcessor {

    public:

        ChunkConveyor* conveyor;

        virtual Status process(ChunkRef* chunk, int priority, ProcessingData& proc_data) {
            /*
            int idx = chunk->idx;
            if (idx >= 1000) {
                idx = 0;
            }
            chunk->idx = idx + 1;
            chunk->conveyors[idx] = conveyor->name.utf8().get_data();
*/
            conveyor->push(chunk, priority);
            return Status::STOP;
        }

        ConveyorPush(ChunkConveyor* conveyor) : conveyor(conveyor) {}
    };

    const String name;
    const uint8_t priorities;

    queue<ChunkRef*>** input;
    boost::atomic<uint32_t> input_size;

    Vector<ChunkProcessor*> processors;
    ConveyorPush* push_processor;

private:

    static bool th_process(void* self) {
        return static_cast<ChunkConveyor*>(self)->process();
    }

public:

    bool process();

    void push(ChunkRef* chunk, int priority) {
        input_size.fetch_add(1);
        if (priority < 0) {
            priority = 0;
        } else if (priority > PRIORITIES_COUNT - 1) {
            priority = PRIORITIES_COUNT - 1;
        }
        input[priority]->push(chunk);
    }

    void add_processor(ChunkProcessor* processor) {
        processors.push_back(processor);
    }

    String to_string() {
        Array values;
        values.append(name);
        values.append(Variant(input_size));
        return String("{0}: {1}").format(values);
    }

    ChunkProcessor* get_push_proc() {
        return push_processor;
    }

    ChunkConveyor(const char* name, int threads = 1)
                                                   : name(name),
                                                     priorities(PRIORITIES_COUNT) {

        push_processor = memnew(ConveyorPush(this));

        input = memnew_arr(queue<ChunkRef*>*, priorities);
        for (int i = 0; i < priorities; i++) {
            input[i] = memnew(queue<ChunkRef*>(100));
        }

        if (threads > 0) {
            ThreadPool::get_singleton()->create(ChunkConveyor::th_process, this, threads);
        }
    }

    ~ChunkConveyor() {
        for (int i = 0; i < priorities; i++) {
            memdelete(input[i]);
        }
        memdelete_arr(input);

        memdelete(push_processor);
    }
};

#endif // CHUNK_CONVEYOR_H

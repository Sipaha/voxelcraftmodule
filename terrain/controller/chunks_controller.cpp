#include "chunks_controller.h"

#include "commands/set_data_changed.h"
#include "commands/set_neighbour_blocks.h"
#include "processor/add_to_scene_processor.h"
#include "loading/loading_processor.h"
#include "meshing/mesh_processor.h"
#include "observer/observers.h"
#include "print_string.h"
#include "light/light_processor.h"
#include "processing_data.h"

using namespace vxl::commands;

ChunkProcessor::Status ChunksController::process(ChunkRef *chunkRef, int priority, ProcessingData& proc_data) {

    Chunk* chunk = chunkRef->get();

    //printf("start process %s\n", chunk->to_string().utf8().get_data());

    if (!chunk->flags.get(ChunkFlags::LOADED)) {

        loading_proc->process(chunkRef, priority, proc_data);
        Terrain::get_observers()->chunk_loaded(chunkRef, priority);
        chunk->flags.set(ChunkFlags::LOADED);

        for (int i = 0; i < SIDES_CONTACT_COUNT; i++) {

            Int3 n_pos = chunk->position + vxl_t::sides_vec[i];
            ChunkRef neighbour = Chunks::get(n_pos);

            if (neighbour.get() != NULL) {
                int op_side = vxl_t::opposite_sides[i];
                int n_priority = MAX(0, priority - 1);
                execute(n_pos, memnew(SetNeighbourBlocksTransparency(chunk, op_side, n_priority)), n_priority);
                execute(n_pos, memnew(GetNeighbourBlocksTransparency(op_side, priority)), priority);
            }
        }
    }

    invoke_commands(chunkRef, priority);

    bool ready_to_process = true;
    if (!chunk->blocks.is_empty() && chunk->loaded_neighbours != SIDES_CONTACT_FLAG) {
        input_mutex->lock();
        invoke_commands(chunkRef, priority);
        if (chunk->loaded_neighbours != SIDES_CONTACT_FLAG) {
            chunk->flags.unset(ChunkFlags::ON_PROCESSING);
            memdelete(chunkRef);
            ready_to_process = false;
        }
        input_mutex->unlock();
    }

    if (ready_to_process) {

        invoke_commands(chunkRef, priority);

        bool on_processing = true;

        if (chunk->flags.get(ChunkFlags::IS_PROCESSED)) {
            input_mutex->lock();
            invoke_commands(chunkRef, priority);
            if (chunk->flags.get(ChunkFlags::IS_PROCESSED)) {
                chunk->flags.unset(ChunkFlags::ON_PROCESSING);
                chunk->release_processing_data();
                memdelete(chunkRef);
                on_processing = false;
            }
            input_mutex->unlock();
        }

        if (on_processing) {

            while (!chunk->flags.get(ChunkFlags::LIGHTED)) {

                light_proc->process(chunkRef, priority, proc_data);
                invoke_commands(chunkRef, priority);

                if (!chunk->blocks.is_empty()) {

                    while (chunk->flags.get(ChunkFlags::LIGHTED)
                       && !chunk->flags.get(ChunkFlags::MESHED)) {

                        mesh_proc->process(chunkRef, priority, proc_data);
                        invoke_commands(chunkRef, priority);
                    }
                } else {
                    chunk->flags.set(ChunkFlags::MESHED);
                }
            }

            if (chunk->flags.set(ChunkFlags::ON_SCENE) && !chunk->mesh.is_empty()) {
                main_th_conveyor->push(chunkRef, priority);
            } else {
                control_conveyor->push(chunkRef, priority);
            }
        }

    } else {
        //printf("without neighbours %s\n", chunk->to_string().utf8().get_data());
    }

    return ChunkProcessor::STOP;
}

void ChunksController::invoke_commands(ChunkRef *chunkRef, int priority) {

    Chunk* chunk = (*chunkRef).get();

    int new_priority = MIN(chunk->commands.invoke_all(), priority);

    if (chunk->blocks.data_changed) {

        chunk->flags.unset(ChunkFlags::IS_PROCESSED);
        chunk->blocks.data_changed = false;

        if (chunk->blocks.changed_neighbours) {

            for (int i = 0, mask = 1; i < SIDES_CONTACT_COUNT; i++, mask <<= 1) {

                if (chunk->blocks.changed_neighbours & mask) {
                    ChunkRef neighbour = Chunks::get(chunk->position + vxl_t::sides_vec[i]);
                    if (neighbour.get() != NULL) {
                        int op_side = vxl_t::opposite_sides[i];
                        neighbour->flags.unset(ChunkFlags::IS_PROCESSED);
                        execute(neighbour->position, memnew(SetNeighbourBlocksTransparency(chunk, op_side, new_priority)), new_priority);
                    }
                }
            }
            chunk->blocks.changed_neighbours = 0;
        }

        Terrain::get_observers()->chunk_updated(chunkRef, new_priority);
    }
}

void ChunksController::to_control_input(ChunkRef &ref, int priority) {
    input_mutex->lock();
    if (ref->flags.set(ChunkFlags::ON_PROCESSING)) {
        control_conveyor->push(memnew(ChunkRef(ref)), priority);
    }
    input_mutex->unlock();
}

void ChunksController::load_chunk(ChunkRef& chunk, int priority) {
    to_control_input(chunk, priority);
}

void ChunksController::execute(const Int3& key, Command* command, int priority) {
    ChunkRef ref = Chunks::get_or_add(key);
    ref->commands.add(command);
    to_control_input(ref, priority);
}

void ChunksController::main_th_update() {
    main_th_conveyor->process();
}

ChunksController::ChunksController(uint8_t load_threads, uint8_t meshing_threads) {

    control_conveyor = memnew(ChunkConveyor("Control", 4));
    main_th_conveyor = memnew(ChunkConveyor("AddToScene", 0));

    add_to_scene_proc = memnew(AddToSceneProcessor);
    loading_proc = memnew(LoadingProcessor);
    mesh_proc = memnew(MeshProcessor);
    light_proc = memnew(LightProcessor);

    control_conveyor->add_processor(this);
    main_th_conveyor->add_processor(add_to_scene_proc);
    main_th_conveyor->add_processor(control_conveyor->get_push_proc());

    input_mutex = Mutex::create();
}

ChunksController::~ChunksController() {

    memdelete(control_conveyor);
    memdelete(main_th_conveyor);

    memdelete(add_to_scene_proc);
    memdelete(loading_proc);
    memdelete(mesh_proc);
    memdelete(light_proc);

    memdelete(input_mutex);
}

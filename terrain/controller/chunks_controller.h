#ifndef CHUNK_CONTROLLER_H
#define CHUNK_CONTROLLER_H

#include "processor/chunk_processor.h"
#include "commands/commands.h"

using namespace vxl::commands;

class ChunkConveyor;
class LoadingProcessor;
class AddToSceneProcessor;
class MeshProcessor;
class ObserversProcessor;
class LightProcessor;

class ChunksController : public ChunkProcessor {

private:

    ChunkConveyor* control_conveyor;
    ChunkConveyor* main_th_conveyor;

    LoadingProcessor* loading_proc;
    MeshProcessor* mesh_proc;
    AddToSceneProcessor* add_to_scene_proc;
    LightProcessor* light_proc;

    Mutex* input_mutex;

protected:

    virtual Status process(ChunkRef* chunk, int priority, ProcessingData& empty);
    void invoke_commands(ChunkRef* chunk, int priority);

    void to_control_input(ChunkRef& ref, int priority);

public:

    void load_chunk(ChunkRef& ref, int priority = 3);
    void execute(const Int3& key, Command* command, int priority = 3);

    void main_th_update();

    ChunksController(uint8_t load_threads = 1, uint8_t meshing_threads = 1);
    ~ChunksController();
};


#endif // CHUNK_CONTROLLER_H

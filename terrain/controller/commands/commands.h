#ifndef VXL_COMMANDS_H
#define VXL_COMMANDS_H

#include "simple_type.h"
#include "../../../utils/structs/int3.h"
#include "../../chunks/chunk.h"

namespace vxl {
namespace commands {

struct Command {

	int priority = 0;

	virtual void call(Chunk* chunk) = 0;
	virtual ~Command() {};
};

template <class T, class M>
struct Command0 : public Command {

	T* instance;
	M method;

	virtual void call(Chunk* chunk) {
		(instance->*method)(chunk);
	}

	Command0(T* instance, M method)
				: instance(instance),
				  method(method) {
	}
};

template <class T, class M, class P1>
struct Command1 : public Command {

	T *instance;
	M method;
	typename GetSimpleTypeT<P1>::type_t p1;

	virtual void call(Chunk* chunk) {
		(instance->*method)(chunk, p1);
	}

	Command1(T* instance, M method, typename GetSimpleTypeT<P1>::type_t p1)
				: instance(instance),
				  method(method),
				  p1(p1) {
	}
};

template <class T, class M, class P1, class P2>
struct Command2 : public Command {

	T *instance;
	M method;
	typename GetSimpleTypeT<P1>::type_t p1;
	typename GetSimpleTypeT<P2>::type_t p2;

	virtual void call(Chunk* chunk) {
		(instance->*method)(chunk, p1, p2);
	}

	Command2(T* instance, M method, typename GetSimpleTypeT<P1>::type_t p1,
									typename GetSimpleTypeT<P2>::type_t p2)
				: instance(instance),
				  method(method),
				  p1(p1), p2(p2) {
	}
};

template <class T, class M, class P1, class P2, class P3>
struct Command3 : public Command {

	T *instance;
	M method;
	typename GetSimpleTypeT<P1>::type_t p1;
	typename GetSimpleTypeT<P2>::type_t p2;
	typename GetSimpleTypeT<P3>::type_t p3;

	virtual void call(Chunk* chunk) {
		(instance->*method)(chunk, p1, p2, p3);
	}

	Command3(T* instance, M method, typename GetSimpleTypeT<P1>::type_t p1,
									typename GetSimpleTypeT<P2>::type_t p2,
									typename GetSimpleTypeT<P3>::type_t p3)
				: instance(instance),
				  method(method),
				  p1(p1), p2(p2), p3(p3) {
	}
};

template <class T, class M, class P1, class P2, class P3, class P4>
struct Command4 : public Command {

	T *instance;
	M method;
	typename GetSimpleTypeT<P1>::type_t p1;
	typename GetSimpleTypeT<P2>::type_t p2;
	typename GetSimpleTypeT<P3>::type_t p3;
	typename GetSimpleTypeT<P4>::type_t p4;

	virtual void call(Chunk* chunk) {
		(instance->*method)(chunk, p1, p2, p3, p4);
	}

	Command4(T* instance, M method, typename GetSimpleTypeT<P1>::type_t p1,
									typename GetSimpleTypeT<P2>::type_t p2,
									typename GetSimpleTypeT<P2>::type_t p3,
									typename GetSimpleTypeT<P2>::type_t p4)
				: instance(instance),
				  method(method),
				  p1(p1), p2(p2), p3(p3), p4(p4) {
	}
};

template <class T, class M, class P1, class P2, class P3, class P4, class P5>
struct Command5 : public Command {

	T *instance;
	M method;
	typename GetSimpleTypeT<P1>::type_t p1;
	typename GetSimpleTypeT<P2>::type_t p2;
	typename GetSimpleTypeT<P3>::type_t p3;
	typename GetSimpleTypeT<P4>::type_t p4;
	typename GetSimpleTypeT<P5>::type_t p5;

	virtual void call(Chunk* chunk) {
		(instance->*method)(chunk, p1, p2, p3, p4, p5);
	}

	Command5(T* instance, M method, typename GetSimpleTypeT<P1>::type_t p1,
									typename GetSimpleTypeT<P2>::type_t p2,
									typename GetSimpleTypeT<P2>::type_t p3,
									typename GetSimpleTypeT<P2>::type_t p4,
									typename GetSimpleTypeT<P2>::type_t p5)
				: instance(instance),
				  method(method),
				  p1(p1), p2(p2), p3(p3), p4(p4), p5(p5) {
	}
};

template <class T, class M>
Command* new_command(T *p_instance, M p_method, int priority = 0) {
	return memnew((Command0<T, M>)(p_instance, p_method));
}

template <class T, class M, class P1>
Command* new_command(T *p_instance, M p_method, P1 p1) {
	return memnew((Command1<T, M, P1>)(p_instance, p_method, p1));
}

template <class T, class M, class P1, class P2>
Command* new_command(T *p_instance, M p_method, P1 p1, P2 p2) {
	return memnew((Command2<T, M, P1, P2>)(p_instance, p_method, p1, p2));
}

template <class T, class M, class P1, class P2, class P3>
Command* new_command( T *p_instance, M p_method, P1 p1, P2 p2, P3 p3) {
	return memnew((Command3<T, M, P1, P2, P3>)(p_instance, p_method, p1, p2, p3));
}

template <class T, class M, class P1, class P2, class P3, class P4>
Command* new_command(T *p_instance, M p_method, P1 p1, P2 p2, P3 p3, P4 p4) {
	return memnew((Command4<T, M, P1, P2, P3, P4>)(p_instance, p_method, p1, p2, p3, p4));
}

template <class T, class M, class P1, class P2, class P3, class P4, class P5>
Command* new_command(T *p_instance, M p_method, P1 p1, P2 p2, P3 p3, P4 p4, P5 p5) {
	return memnew((Command5<T, M, P1, P2, P3, P4, P5>)(p_instance, p_method, p1, p2, p3, p4, p5));
}

}
}

#endif // VXL_COMMANDS_H

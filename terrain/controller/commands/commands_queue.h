#ifndef COMMAND_PROCESSOR_H
#define COMMAND_PROCESSOR_H

#include "boost/lockfree/queue.hpp"
#include "commands.h"
#include "hash_map.h"
#include "os/mutex.h"
#include "../chunks/chunk.h"

using namespace vxl::commands;
using namespace boost::lockfree;

class CommandsQueue {

private:

    template <class T>
    _FORCE_INLINE_ T* instantiate() {
        return memnew(T);
    }

    Mutex* mutex;
    HashMap<Int3, List<Command*>*, Int3, Int3> commands_by_key;

public:

    bool pop(Int3 key, List<Command*>*& out) {

        mutex->lock();

        bool result = false;

        List<Command*>** commands_p = commands_by_key.getptr(key);
        if (commands_p != NULL) {
            out = *commands_p;
            result = true;
            commands_by_key.erase(key);
        }

        mutex->unlock();

        return result;
    }

    void push(Int3 key, Command* command) {

        mutex->lock();

        List<Command*>** commands_p = commands_by_key.getptr(key);
        List<Command*>* commands;
        if (commands_p == NULL) {
            commands = memnew(List<Command*>);
            commands_by_key[key] = commands;
        } else {
            commands = *commands_p;
        }
        commands->push_back(command);

        mutex->unlock();
    }

    int invoke_all(Chunk* chunk) {

        int priority = 1;

        List<Command*>* commands;

        if (pop(chunk->position, commands)) {

            List<Command*>::Element *iter = commands->front();

            while (iter != NULL) {

                Command* comm = iter->get();
                iter->erase();

                priority = MIN(priority, comm->priority);
                comm->call(chunk);
                memdelete(comm);

                iter = commands->front();
            }
            memdelete(commands);
        }

        return priority;
    }

	CommandsQueue() {
		mutex = Mutex::create();
	}

	~CommandsQueue() {
		memdelete(mutex);
	}

};


#endif // COMMAND_PROCESSOR_H

#ifndef SET_DATA_CHANGED_H
#define SET_DATA_CHANGED_H

#include "commands.h"

namespace vxl {
namespace commands {

struct SetDataChanged : public Command {

    virtual void call(Chunk* chunk) {
        chunk->blocks.data_changed = true;
    }

    SetDataChanged(int priority) {
        this->priority = priority;
    }

};

}}
#endif // SET_DATA_CHANGED_H

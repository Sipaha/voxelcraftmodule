#ifndef SET_NEIGHBOUR_BLOCKS_H
#define SET_NEIGHBOUR_BLOCKS_H

#include "commands.h"
#include "../../../constants.h"
#include "../../chunks/chunks.h"

namespace vxl {
namespace commands {

struct SetNeighbourBlocksTransparency : public Command {

    int offset;
    uint8_t side;
    uint8_t transparency[N_BLOCKS_SIDE_TRANSPARENCY_SIZE] {0};

    virtual void call(Chunk* chunk) {
        if (memcmp(&chunk->n_blocks_transparency[offset], transparency, N_BLOCKS_SIDE_TRANSPARENCY_SIZE) != 0) {
            memcpy(&chunk->n_blocks_transparency[offset], transparency, N_BLOCKS_SIDE_TRANSPARENCY_SIZE);
            chunk->blocks.data_changed = true;
        }
        chunk->loaded_neighbours |= vxl_t::flags[side];
    }

    SetNeighbourBlocksTransparency(Chunk* neighbour, uint8_t chunk_side, int priority) {

        this->priority = priority;
        this->side = chunk_side;
        this->offset = chunk_side * N_BLOCKS_SIDE_TRANSPARENCY_SIZE;

        uint8_t op_side = vxl_t::opposite_sides[chunk_side];
        NeighbourTransparencyIterator it = NeighbourTransparencyIterator(op_side, transparency);

        while (it.hasNext()) {
            if (neighbour->blocks.get_block(it.get_pos()).id == 0) {
                *it.byte |= 1 << it.bit_idx;
            }
            it.next();
        }
    }

};

struct GetNeighbourBlocksTransparency : public Command {

    int side;

    virtual void call(Chunk* n_chunk) {
        int op_side = vxl_t::opposite_sides[side];
        Command* comm = memnew(SetNeighbourBlocksTransparency(n_chunk, op_side, priority));
        Chunks::execute(n_chunk->position + vxl_t::sides_vec[side], comm);
    }

    GetNeighbourBlocksTransparency(uint8_t side, int priority) {
        this->priority = priority;
        this->side = side;
    }
};

}}

#endif // SET_NEIGHBOUR_BLOCKS_H

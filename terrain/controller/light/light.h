#ifndef LIGHT_TYPES_H
#define LIGHT_TYPES_H

#include "typedefs.h"
#include "color.h"
#include "../../../constants.h"
#include "math/math_funcs.h"

#define CHUNK_BLOCKS_LIGHT_SIZE (CHUNK_BLOCKS_OUSIDE_SIZE * CHUNK_BLOCKS_OUSIDE_SIZE * CHUNK_BLOCKS_OUSIDE_SIZE)
#define COLOR_MAX 15

struct LightSource {

    static const uint8_t MAX_INT_INTENCITY = 127;

    union {
        struct {
            uint8_t r;
            uint8_t g;
            uint8_t b;
        };
        uint8_t components[3];
    };

	_FORCE_INLINE_ bool isEmpty() {
		return r == 0 && g == 0 && b == 0;
	}

    LightSource() {
    }

    LightSource(float r, float g, float b) {
        this->r = MIN(r * MAX_INT_INTENCITY, 255);
        this->g = MIN(g * MAX_INT_INTENCITY, 255);
        this->b = MIN(b * MAX_INT_INTENCITY, 255);
    }

    LightSource(const LightSource& other) {
        memcpy(components, other.components, 3);
    }
};

struct LightData {

	float MIN_FLOAT_COLOR = 1.0 / LightSource::MAX_INT_INTENCITY;

	union {
		struct {
			float r;
			float g;
			float b;
			float sun;
		};
		float components[4];
	};

	_FORCE_INLINE_ float &operator[](int idx) {
		return components[idx];
	}
	_FORCE_INLINE_ const uint8_t &operator[](int idx) const {
		return components[idx];
	}

	LightData multiply_color(float amount) {
		return LightData(r * amount, g * amount, b * amount, sun);
	}

	void set_color(LightSource& source) {
		r = (float) source.r / LightSource::MAX_INT_INTENCITY;
		g = (float) source.g / LightSource::MAX_INT_INTENCITY;
		b = (float) source.b / LightSource::MAX_INT_INTENCITY;
	}

	void set_sun(uint8_t sun) {
		this->sun = (float) sun / LightSource::MAX_INT_INTENCITY;
	}

	_FORCE_INLINE_ bool is_not_empty_color() {
		return r >= MIN_FLOAT_COLOR || g >= MIN_FLOAT_COLOR || b >= MIN_FLOAT_COLOR;
	}

	_FORCE_INLINE_ bool is_empty_color() {
		return r < MIN_FLOAT_COLOR && g < MIN_FLOAT_COLOR && b < MIN_FLOAT_COLOR;
	}

	_FORCE_INLINE_ bool is_empty_sun() {
		return sun == 0.0;
	}

	LightData() : r(0), g(0), b(0) {
	}

	LightData(float r, float g, float b, float sun) : r(r), g(g), b(b), sun(sun) {
	}
};

struct NeighbourLight {

    static const size_t BYTES_COUNT = sizeof(LightSource) * CHUNK_SIDE_BLOCKS_COUNT;

    LightSource data[CHUNK_SIDE_BLOCKS_COUNT] {LightSource()};

    NeighbourLight() {
    }

    _FORCE_INLINE_ static bool equals(NeighbourLight* l0, NeighbourLight* l1) {
        return (l0 == NULL && l1 == NULL)
            || (l0 != NULL && l1 != NULL && memcmp(l0->data, l1->data, BYTES_COUNT) == 0);
    }
};

struct NeighbourSunLight {

    static const uint8_t MAX_INTENCITY = LightSource::MAX_INT_INTENCITY;
    static const size_t BYTES_COUNT = CHUNK_SIDE_BLOCKS_COUNT;

    uint8_t data[CHUNK_SIDE_BLOCKS_COUNT];

    NeighbourSunLight(uint8_t initial = 0) {
        memset(data, initial, BYTES_COUNT);
    }

    _FORCE_INLINE_ static bool equals(NeighbourSunLight* l0, NeighbourSunLight* l1) {
        return (l0 == NULL && l1 == NULL)
            || (l0 != NULL && l1 != NULL && memcmp(l0->data, l1->data, BYTES_COUNT) == 0);
    }
};

#endif // LIGHT_TYPES_H

#ifndef LIGHT_PROCESSOR_H
#define LIGHT_PROCESSOR_H

#include "../processor/chunk_processor.h"
#include "hash_map.h"
#include "color.h"
#include "../../chunks/chunks.h"
#include "../../chunks/chunk_utils.h"
#include "list.h"
#include "math/math_funcs.h"
#include "set_neighbour_light.h"

class LightProcessor : ChunkProcessor {

private:

    void fill_light(LightData* light_map, Set<uint16_t>& sources, ChunkBlocks& blocks) {

        while (sources.size() > 0) {

            Set<uint16_t>::Element* op_nodes_element = sources.front();

            uint16_t node_idx = op_nodes_element->get();

            LightData light_data = light_map[node_idx].multiply_color(0.85);
            Int3 pos = ChunkLight::decode_light_block_idx(node_idx);

            for (int side = 0; side < SIDES_CONTACT_COUNT; side++) {

                Int3 n_pos = pos + vxl_t::sides_vec[side];

                if (n_pos.x >= 0 && n_pos.x < CHUNK_BLOCKS_SIZE &&
                    n_pos.y >= 0 && n_pos.y < CHUNK_BLOCKS_SIZE &&
                    n_pos.z >= 0 && n_pos.z < CHUNK_BLOCKS_SIZE) {

                    if (blocks.get_block(n_pos).id != 0) {
                        continue;
                    }

                    uint16_t n_idx = ChunkLight::encode_light_block_idx(n_pos);

                    LightData ex_light = light_map[n_idx];
                    if (mix(ex_light, light_data)) {
                        sources.insert(n_idx);
                        light_map[n_idx] = ex_light;
                    }
                }
            }
            sources.erase(op_nodes_element);
        }
    }

    bool mix(LightData& to, LightData& from) {
        bool changed = false;
        for (int i = 0; i < 3; i++) {
            if (from.components[i] > to.components[i]) {
                to.components[i] = from.components[i];
                changed = true;
            }
        }
        return changed;
    }

    void fill_sunlight(LightData* light_map, ChunkBlocks& blocks) {

        Set<uint16_t> open_nodes;
        Int3 empty_blocks[CHUNK_SIDE_BLOCKS_COUNT];

        for (int y = CHUNK_BLOCKS_SIZE - 1; y >= 0; y--) {

            int empty_count = 0;

            for (int x = 0; x < CHUNK_BLOCKS_SIZE; x++) {
                for (int z = 0; z < CHUNK_BLOCKS_SIZE; z++) {

                    if (blocks.get_block(x, y, z).id == 0) {

                        uint16_t top_idx = ChunkLight::encode_light_block_idx(Int3(x, y + 1, z));
                        uint16_t curr_idx = ChunkLight::encode_light_block_idx(Int3(x, y, z));

                        light_map[curr_idx].sun = light_map[top_idx].sun;

                        if (light_map[top_idx].sun < 1.0) {
                            empty_blocks[empty_count++] = Int3(x, y, z);
                        }
                    }
                }
            }

            for (int i = 0; i < empty_count; i++) {

                Int3 pos = empty_blocks[i];

                int curr_idx = ChunkLight::encode_light_block_idx(pos);

                float max = light_map[curr_idx].sun;

                uint16_t xp_idx = ChunkLight::encode_light_block_idx(Int3(pos.x + 1, pos.y, pos.z));
                uint16_t xn_idx = ChunkLight::encode_light_block_idx(Int3(pos.x - 1, pos.y, pos.z));
                uint16_t zp_idx = ChunkLight::encode_light_block_idx(Int3(pos.x, pos.y, pos.z + 1));
                uint16_t zn_idx = ChunkLight::encode_light_block_idx(Int3(pos.x, pos.y, pos.z - 1));

                max = MAX(max, light_map[xp_idx].sun);
                max = MAX(max, light_map[xn_idx].sun);
                max = MAX(max, light_map[zp_idx].sun);
                max = MAX(max, light_map[zn_idx].sun);

                if (y == 0) {
                    uint16_t yn_idx = ChunkLight::encode_light_block_idx(Int3(pos.x, pos.y - 1, pos.z));
                    max = MAX(max, light_map[yn_idx].sun);
                }

                if (max > 0) {
                    light_map[curr_idx].sun = 0.85 * max;
                    open_nodes.insert(curr_idx);
                }
            }
        }

        while (open_nodes.size() > 0) {

            Set<uint16_t>::Element* op_nodes_element = open_nodes.front();

            uint16_t node_idx = op_nodes_element->get();

            float light_data = light_map[node_idx].sun * 0.85;
            Int3 pos = ChunkLight::decode_light_block_idx(node_idx);

            for (int side = 0; side < SIDES_CONTACT_COUNT; side++) {

                Int3 n_pos = pos + vxl_t::sides_vec[side];

                if (n_pos.x >= 0 && n_pos.x < CHUNK_BLOCKS_SIZE &&
                    n_pos.y >= 0 && n_pos.y < CHUNK_BLOCKS_SIZE &&
                    n_pos.z >= 0 && n_pos.z < CHUNK_BLOCKS_SIZE) {

                    if (blocks.get_block(n_pos).id != 0) {
                        continue;
                    }

                    uint16_t n_idx = ChunkLight::encode_light_block_idx(n_pos);

                    if (light_map[n_idx].sun < light_data) {
                        light_map[n_idx].sun = light_data;
                        open_nodes.insert(n_idx);
                    }
                }
            }
            open_nodes.erase(op_nodes_element);
        }
    }

    void get_light_for_neighbour(LightData* light_map, int side, NeighbourLight** n_out, NeighbourSunLight** sun_out) {

        SideBlocksIterator it(side);

        *n_out = NULL;
        *sun_out = NULL;

        while (it.hasNext()) {

            uint16_t pos_idx = ChunkLight::encode_light_block_idx(it.get_pos());

            LightData light_data = light_map[pos_idx];
            if (light_data.is_not_empty_color()) {
                if (*n_out == NULL) {
                    *n_out = memnew(NeighbourLight);
                }
                (*n_out)->data[it.get_idx()] = LightSource(light_data.r, light_data.g, light_data.b);
            }
            if (!light_data.is_empty_sun()) {
                if (*sun_out == NULL) {
                    *sun_out = memnew(NeighbourSunLight);
                }
                (*sun_out)->data[it.get_idx()] = light_data.sun * NeighbourSunLight::MAX_INTENCITY;
            }

            it.next();
        }
    }

public:

    virtual Status process(ChunkRef* chunkRef, int priority, ProcessingData& proc_data) {

        Chunk* chunk = chunkRef->get();

        proc_data.clean_light_map();
        LightData* light_map = proc_data.light_map;

        Set<uint16_t> sources;
        chunk->light.fill_light_sources(sources, light_map);
        fill_light(light_map, sources, chunk->blocks);
        fill_sunlight(light_map, chunk->blocks);

        for (int i = 0; i < SIDES_CONTACT_COUNT; i++) {

            Int3 pos = chunk->position + vxl_t::sides_vec[i];
            ChunkRef ref = Chunks::get(pos);

            if (ref.get() != NULL) {

                NeighbourLight* light_to_n;
                NeighbourSunLight* sun_light_to_n;

                get_light_for_neighbour(light_map, i, &light_to_n, &sun_light_to_n);

                int op_side = vxl_t::opposite_sides[i];
                Command* comm = memnew(SetNeighbourLight(op_side, light_to_n, sun_light_to_n, priority));
                Chunks::execute(pos, comm, priority);
            }
        }

        chunk->flags.set(ChunkFlags::LIGHTED);

        return ChunkProcessor::STOP;
    }


    LightProcessor() {
    }
};

#endif // LIGHT_PROCESSOR_H

#ifndef SET_NEIGHBOUR_LIGHT_H
#define SET_NEIGHBOUR_LIGHT_H

#include "../commands/commands.h"
#include "../../chunks/chunk_light.h"
#include "list.h"

struct SetNeighbourLight : public Command {

private:

    int side;
    NeighbourLight* new_n_light;
    NeighbourSunLight* new_sun_light;

public:

    virtual void call(Chunk* chunk) {
        chunk->light.set_neighbour_light(side, new_n_light, new_sun_light);
    }

    SetNeighbourLight(int side, NeighbourLight* n_light, NeighbourSunLight* sun_light, int priority) {
        this->priority = priority;
        this->new_n_light = n_light;
        this->new_sun_light = sun_light;
        this->side = side;
    }
};

#endif // SET_NEIGHBOUR_LIGHT_H

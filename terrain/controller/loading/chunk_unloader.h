#ifndef CHUNK_UNLOADER_H
#define CHUNK_UNLOADER_H

#include "../../utils/collections/ordered_hset.h"
#include "../../utils/collections/waiting_queue.h"
#include "../terrain.h"

class ChunkUnloader : public ChunkProcessor {

private:

protected:

    virtual bool process_impl(ChunkRef& chunk) {

        return true;
    }

public:

    ChunkUnloader(int threads = 1) : ChunkProcessor("Unloader", threads) {

    }

};

#endif // CHUNK_UNLOADER_H

#ifndef LOADING_PROCESSOR_H
#define LOADING_PROCESSOR_H

#include "../processor/chunk_processor.h"
#include "../../terrain.h"

class LoadingProcessor : public ChunkProcessor {

public:

    virtual Status process(ChunkRef* chunk, int priority, ProcessingData& proc_data) {

        Terrain::get_generator()->generate((*chunk).get());

        return Status::CONTINUE;
    }

};

#endif // LOADING_PROCESSOR_H

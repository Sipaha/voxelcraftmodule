#include "chunk_geometry.h"
#include "../../terrain.h"

void ChunkGeometry::generate(Chunk* chunk, ProcessingData& proc_data) {

    ChunkMesh* cmesh = &chunk->mesh;

    const BlockInfo* block_info = Terrain::get_library()->get_block(1);
    cmesh->begin(block_info->material);

    const Int3& pos = chunk->position;
    Vector3 base(pos.x * CHUNK_BLOCKS_SIZE * BLOCK_SIZE,
                 pos.y * CHUNK_BLOCKS_SIZE * BLOCK_SIZE,
                 pos.z * CHUNK_BLOCKS_SIZE * BLOCK_SIZE);

    ChunkBlocks* chunk_blocks = &chunk->blocks;

    uint8_t* chunk_x_neg = &chunk->n_blocks_transparency[SIDE_NZZ * N_BLOCKS_SIDE_TRANSPARENCY_SIZE];
    uint8_t* chunk_x_pos = &chunk->n_blocks_transparency[SIDE_PZZ * N_BLOCKS_SIDE_TRANSPARENCY_SIZE];
    uint8_t* chunk_y_neg = &chunk->n_blocks_transparency[SIDE_ZNZ * N_BLOCKS_SIDE_TRANSPARENCY_SIZE];
    uint8_t* chunk_y_pos = &chunk->n_blocks_transparency[SIDE_ZPZ * N_BLOCKS_SIDE_TRANSPARENCY_SIZE];
    uint8_t* chunk_z_neg = &chunk->n_blocks_transparency[SIDE_ZZN * N_BLOCKS_SIDE_TRANSPARENCY_SIZE];
    uint8_t* chunk_z_pos = &chunk->n_blocks_transparency[SIDE_ZZP * N_BLOCKS_SIDE_TRANSPARENCY_SIZE];

    for (int x = 0; x < CHUNK_BLOCKS_SIZE; ++x) {
        for (int y = 0; y < CHUNK_BLOCKS_SIZE; ++y) {
            for (int z = 0; z < CHUNK_BLOCKS_SIZE; ++z) {

                const Block& block = chunk_blocks->get_block(x, y, z);
                const BlockInfo* concrete_info = Terrain::get_library()->get_block(block.id);

                if (block.id > 0) {

                    int faces = 0;
                    Vector3 pos = base + Vector3(x * BLOCK_SIZE, y * BLOCK_SIZE, z * BLOCK_SIZE);

                    //x
                    if (x == 0) {
                        if (is_block_transparent(chunk_x_neg, y, z)) {
                            faces |= vxl_t::flags[SIDE_NZZ];
                        }
                    } else if (chunk_blocks->get_block(x - 1, y, z).id == 0) {
                        faces |= vxl_t::flags[SIDE_NZZ];
                    }
                    if (x == CHUNK_BLOCKS_SIZE - 1) {
                        if (is_block_transparent(chunk_x_pos, y, z)) {
                            faces |= vxl_t::flags[SIDE_PZZ];
                        }
                    } else if (chunk_blocks->get_block(x + 1, y, z).id == 0) {
                        faces |= vxl_t::flags[SIDE_PZZ];
                    }
                    //y
                    if (y == 0) {
                        if (is_block_transparent(chunk_y_neg, x, z)) {
                            faces |= vxl_t::flags[SIDE_ZNZ];
                        }
                    } else if (chunk_blocks->get_block(x, y - 1, z).id == 0) {
                        faces |= vxl_t::flags[SIDE_ZNZ];
                    }
                    if (y == CHUNK_BLOCKS_SIZE - 1) {
                        if (is_block_transparent(chunk_y_pos, x, z)) {
                            faces |= vxl_t::flags[SIDE_ZPZ];
                        }
                    } else if (chunk_blocks->get_block(x, y + 1, z).id == 0) {
                        faces |= vxl_t::flags[SIDE_ZPZ];
                    }
                    //z
                    if (z == 0) {
                        if (is_block_transparent(chunk_z_neg, x, y)) {
                            faces |= vxl_t::flags[SIDE_ZZN];
                        }
                    } else if (chunk_blocks->get_block(x, y, z - 1).id == 0) {
                        faces |= vxl_t::flags[SIDE_ZZN];
                    }
                    if (z == CHUNK_BLOCKS_SIZE - 1) {
                        if (is_block_transparent(chunk_z_pos, x, y)) {
                            faces |= vxl_t::flags[SIDE_ZZP];
                        }
                    } else if (chunk_blocks->get_block(x, y, z + 1).id == 0) {
                        faces |= vxl_t::flags[SIDE_ZZP];
                    }

                    if (faces > 0) {

                        LightData neighbours_light[SIDES_COUNT];
                        for (int c_side = 0; c_side < SIDES_COUNT; c_side++) {
                            Int3 n_pos = Int3(x, y, z) + vxl_t::sides_vec[c_side];
                            int idx = ChunkLight::encode_light_block_idx(n_pos);
                            neighbours_light[c_side] = proc_data.light_map[idx];
                        }

                        mesh_utils.add_cube_faces(cmesh, pos, faces, concrete_info, neighbours_light);
                        chunk->flags.set(ChunkFlags::VISIBLE);
                    }
                }
            }
        }
    }
    cmesh->end();

    //chunk->blocks.data_changed = false;
    //chunk->chunk_mesh.build_physics(Terrain::get_world()->get_space());

}

//mesh utils

void MeshUtils::_init_vertices() {
    int idx;
    //face front
    idx = 4 * SIDE_ZZP;
    normals[SIDE_ZZP] = Vector3(0, 0, 1);

    cube_vertices[idx + 0] = Vector3(0, 0, BLOCK_SIZE);
    cube_light_sides[idx + 0][0] = SIDE_ZZP;
    cube_light_sides[idx + 0][1] = SIDE_NZP;
    cube_light_sides[idx + 0][2] = SIDE_NNP;
    cube_light_sides[idx + 0][3] = SIDE_ZNP;

    cube_vertices[idx + 1] = Vector3(0, BLOCK_SIZE, BLOCK_SIZE);
    cube_light_sides[idx + 1][0] = SIDE_ZZP;
    cube_light_sides[idx + 1][1] = SIDE_NZP;
    cube_light_sides[idx + 1][2] = SIDE_NPP;
    cube_light_sides[idx + 1][3] = SIDE_ZPP;

    cube_vertices[idx + 2] = Vector3(BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);
    cube_light_sides[idx + 2][0] = SIDE_ZZP;
    cube_light_sides[idx + 2][1] = SIDE_ZPP;
    cube_light_sides[idx + 2][2] = SIDE_PPP;
    cube_light_sides[idx + 2][3] = SIDE_PZP;

    cube_vertices[idx + 3] = Vector3(BLOCK_SIZE, 0, BLOCK_SIZE);
    cube_light_sides[idx + 3][0] = SIDE_ZZP;
    cube_light_sides[idx + 3][1] = SIDE_PZP;
    cube_light_sides[idx + 3][2] = SIDE_ZNP;
    cube_light_sides[idx + 3][3] = SIDE_PNP;

    //face back
    idx = 4 * SIDE_ZZN;
    normals[SIDE_ZZN] = Vector3(0, 0, -1);

    cube_vertices[idx + 0] = Vector3(BLOCK_SIZE, 0, 0);
    cube_light_sides[idx + 0][0] = SIDE_ZZN;
    cube_light_sides[idx + 0][1] = SIDE_PZN;
    cube_light_sides[idx + 0][2] = SIDE_PNN;
    cube_light_sides[idx + 0][3] = SIDE_ZNN;

    cube_vertices[idx + 1] = Vector3(BLOCK_SIZE, BLOCK_SIZE, 0);
    cube_light_sides[idx + 1][0] = SIDE_ZZN;
    cube_light_sides[idx + 1][1] = SIDE_PPN;
    cube_light_sides[idx + 1][2] = SIDE_PZN;
    cube_light_sides[idx + 1][3] = SIDE_ZPN;

    cube_vertices[idx + 2] = Vector3(0, BLOCK_SIZE, 0);
    cube_light_sides[idx + 2][0] = SIDE_ZZN;
    cube_light_sides[idx + 2][1] = SIDE_ZPN;
    cube_light_sides[idx + 2][2] = SIDE_NPN;
    cube_light_sides[idx + 2][3] = SIDE_NZN;

    cube_vertices[idx + 3] = Vector3(0, 0, 0);
    cube_light_sides[idx + 3][0] = SIDE_ZZN;
    cube_light_sides[idx + 3][1] = SIDE_NZN;
    cube_light_sides[idx + 3][2] = SIDE_ZNN;
    cube_light_sides[idx + 3][3] = SIDE_NNN;

    //face right
    idx = 4 * SIDE_PZZ;
    normals[SIDE_PZZ] = Vector3(1, 0, 0);

    cube_vertices[idx + 0] = Vector3(BLOCK_SIZE, 0, BLOCK_SIZE);
    cube_light_sides[idx + 0][0] = SIDE_PZZ;
    cube_light_sides[idx + 0][1] = SIDE_PZP;
    cube_light_sides[idx + 0][2] = SIDE_PNZ;
    cube_light_sides[idx + 0][3] = SIDE_PNP;

    cube_vertices[idx + 1] = Vector3(BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);
    cube_light_sides[idx + 1][0] = SIDE_PZZ;
    cube_light_sides[idx + 1][1] = SIDE_PZP;
    cube_light_sides[idx + 1][2] = SIDE_PPZ;
    cube_light_sides[idx + 1][3] = SIDE_PPP;

    cube_vertices[idx + 2] = Vector3(BLOCK_SIZE, BLOCK_SIZE, 0);
    cube_light_sides[idx + 2][0] = SIDE_PZZ;
    cube_light_sides[idx + 2][1] = SIDE_PZN;
    cube_light_sides[idx + 2][2] = SIDE_PPZ;
    cube_light_sides[idx + 2][3] = SIDE_PPN;

    cube_vertices[idx + 3] = Vector3(BLOCK_SIZE, 0, 0);
    cube_light_sides[idx + 3][0] = SIDE_PZZ;
    cube_light_sides[idx + 3][1] = SIDE_PZN;
    cube_light_sides[idx + 3][2] = SIDE_PNZ;
    cube_light_sides[idx + 3][3] = SIDE_PNN;

    //face left
    idx = 4 * SIDE_NZZ;
    normals[SIDE_NZZ] = Vector3(-1, 0, 0);

    cube_vertices[idx + 0] = Vector3(0, 0, 0);
    cube_light_sides[idx + 0][0] = SIDE_NZZ;
    cube_light_sides[idx + 0][1] = SIDE_NZN;
    cube_light_sides[idx + 0][2] = SIDE_NNZ;
    cube_light_sides[idx + 0][3] = SIDE_NNN;

    cube_vertices[idx + 1] = Vector3(0, BLOCK_SIZE, 0);
    cube_light_sides[idx + 1][0] = SIDE_NZZ;
    cube_light_sides[idx + 1][1] = SIDE_NZN;
    cube_light_sides[idx + 1][2] = SIDE_NPZ;
    cube_light_sides[idx + 1][3] = SIDE_NPN;

    cube_vertices[idx + 2] = Vector3(0, BLOCK_SIZE, BLOCK_SIZE);
    cube_light_sides[idx + 2][0] = SIDE_NZZ;
    cube_light_sides[idx + 2][1] = SIDE_NZP;
    cube_light_sides[idx + 2][2] = SIDE_NPZ;
    cube_light_sides[idx + 2][3] = SIDE_NPP;

    cube_vertices[idx + 3] = Vector3(0, 0, BLOCK_SIZE);
    cube_light_sides[idx + 3][0] = SIDE_NZZ;
    cube_light_sides[idx + 3][1] = SIDE_NZP;
    cube_light_sides[idx + 3][2] = SIDE_NNZ;
    cube_light_sides[idx + 3][3] = SIDE_NNP;

    //face top
    idx = 4 * SIDE_ZPZ;
    normals[SIDE_ZPZ] = Vector3(0, 1, 0);

    cube_vertices[idx + 0] = Vector3(0, BLOCK_SIZE, BLOCK_SIZE);
    cube_light_sides[idx + 0][0] = SIDE_ZPZ;
    cube_light_sides[idx + 0][1] = SIDE_ZPP;
    cube_light_sides[idx + 0][2] = SIDE_NPZ;
    cube_light_sides[idx + 0][3] = SIDE_NPP;

    cube_vertices[idx + 1] = Vector3(0, BLOCK_SIZE, 0);
    cube_light_sides[idx + 1][0] = SIDE_ZPZ;
    cube_light_sides[idx + 1][1] = SIDE_ZPN;
    cube_light_sides[idx + 1][2] = SIDE_NPZ;
    cube_light_sides[idx + 1][3] = SIDE_NPN;

    cube_vertices[idx + 2] = Vector3(BLOCK_SIZE, BLOCK_SIZE, 0);
    cube_light_sides[idx + 2][0] = SIDE_ZPZ;
    cube_light_sides[idx + 2][1] = SIDE_ZPN;
    cube_light_sides[idx + 2][2] = SIDE_PPZ;
    cube_light_sides[idx + 2][3] = SIDE_PPN;

    cube_vertices[idx + 3] = Vector3(BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);
    cube_light_sides[idx + 3][0] = SIDE_ZPZ;
    cube_light_sides[idx + 3][1] = SIDE_ZPP;
    cube_light_sides[idx + 3][2] = SIDE_PPZ;
    cube_light_sides[idx + 3][3] = SIDE_PPP;

    //face bottom
    idx = 4 * SIDE_ZNZ;
    normals[SIDE_ZNZ] = Vector3(0, -1, 0);

    cube_vertices[idx + 0] = Vector3(0, 0, 0);
    cube_light_sides[idx + 0][0] = SIDE_ZNZ;
    cube_light_sides[idx + 0][1] = SIDE_ZNN;
    cube_light_sides[idx + 0][2] = SIDE_NNZ;
    cube_light_sides[idx + 0][3] = SIDE_NNN;

    cube_vertices[idx + 1] = Vector3(0, 0, BLOCK_SIZE);
    cube_light_sides[idx + 1][0] = SIDE_ZNZ;
    cube_light_sides[idx + 1][1] = SIDE_ZNP;
    cube_light_sides[idx + 1][2] = SIDE_NNZ;
    cube_light_sides[idx + 1][3] = SIDE_NNP;

    cube_vertices[idx + 2] = Vector3(BLOCK_SIZE, 0, BLOCK_SIZE);
    cube_light_sides[idx + 2][0] = SIDE_ZNZ;
    cube_light_sides[idx + 2][1] = SIDE_ZNP;
    cube_light_sides[idx + 2][2] = SIDE_PNZ;
    cube_light_sides[idx + 2][3] = SIDE_PNP;

    cube_vertices[idx + 3] = Vector3(BLOCK_SIZE, 0, 0);
    cube_light_sides[idx + 3][0] = SIDE_ZNZ;
    cube_light_sides[idx + 3][1] = SIDE_ZNN;
    cube_light_sides[idx + 3][2] = SIDE_PNZ;
    cube_light_sides[idx + 3][3] = SIDE_PNN;
}

MeshUtils::MeshUtils() {
    _init_vertices();
}

MeshUtils::~MeshUtils() {
}



#ifndef CHUNK_GEOMETRY_H
#define CHUNK_GEOMETRY_H

#include "../../library/voxel_library.h"
#include "../../chunks/chunk.h"
#include "../processing_data.h"

class MeshUtils {

public:

    Vector3 cube_vertices[SIDES_CONTACT_COUNT * 4];
    uint16_t cube_light_sides[SIDES_CONTACT_COUNT * 4][4];

    Vector3 normals[SIDES_CONTACT_COUNT];

    _FORCE_INLINE_ void _add_face(ChunkMesh* cmesh, Vector3 pos, int face_verts_offset, int face, const BlockInfo* info, LightData* n_light) {

        Vector3* face_verts = &cube_vertices[face_verts_offset];

        ChunkMesh::Vertex vertex;

        vertex.normal = normals[face];

        const AtlasRegion* region = info->faces[face];

        vertex.uv = region->uv[0];
        vertex.vertex = face_verts[0] + pos;
        vertex.color = get_vertex_color(cube_light_sides[face_verts_offset + 0], n_light);
        cmesh->add_vertex(vertex);

        vertex.uv = region->uv[1];
        vertex.vertex = face_verts[1] + pos;
        vertex.color = get_vertex_color(cube_light_sides[face_verts_offset + 1], n_light);
        cmesh->add_vertex(vertex);

        vertex.uv = region->uv[2];
        vertex.vertex = face_verts[2] + pos;
        vertex.color = get_vertex_color(cube_light_sides[face_verts_offset + 2], n_light);
        cmesh->add_vertex(vertex);

        vertex.uv = region->uv[2];
        vertex.vertex = face_verts[2] + pos;
        vertex.color = get_vertex_color(cube_light_sides[face_verts_offset + 2], n_light);
        cmesh->add_vertex(vertex);

        vertex.uv = region->uv[3];
        vertex.vertex = face_verts[3] + pos;
        vertex.color = get_vertex_color(cube_light_sides[face_verts_offset + 3], n_light);
        cmesh->add_vertex(vertex);

        vertex.uv = region->uv[0];
        vertex.vertex = face_verts[0] + pos;
        vertex.color = get_vertex_color(cube_light_sides[face_verts_offset + 0], n_light);
        cmesh->add_vertex(vertex);
    }

    _FORCE_INLINE_ Color get_vertex_color(uint16_t* light_sides, LightData* n_light) {

        float r = 0;
        float g = 0;
        float b = 0;
        float sun = 0;

        for (int i = 0; i < 4; i++) {
            LightData l = n_light[light_sides[i]];
            r += l.r;
            g += l.g;
            b += l.b;
            sun += l.sun;
        }
        return Color(MIN(r * 0.25, 1.0),
                     MIN(g * 0.25, 1.0),
                     MIN(b * 0.25, 1.0),
                     MIN(sun * 0.25, 1.0));
    }

    void _init_vertices();

    _FORCE_INLINE_ void add_cube_faces(ChunkMesh* cmesh, Vector3 pos, int faces, const BlockInfo* info, LightData* n_light) {

        int dir_flag = 1;
        int vertices_offset = 0;
        for (int i = 0; i < SIDES_CONTACT_COUNT; i++) {
            if (faces & dir_flag) {
                _add_face(cmesh, pos, vertices_offset, i, info, n_light);
            }
            dir_flag = dir_flag << 1;
            vertices_offset += 4;
        }
    }

public:

    MeshUtils();
    ~MeshUtils();
};

class ChunkGeometry {

private:

    MeshUtils mesh_utils;

    bool is_block_transparent(uint8_t* side, int coord0, int coord1) {
        uint32_t side_block_idx = CHUNK_BLOCKS_SIZE * coord0 + coord1;
        uint32_t byte_idx = side_block_idx / 8;
        uint32_t bit_idx = side_block_idx % 8;
        return (side[byte_idx] & (1 << bit_idx)) > 0;
    }

public:

    void generate(Chunk* chunk_ref, ProcessingData& proc_data);

    ChunkGeometry(){}
    ~ChunkGeometry(){}
};

#endif // CHUNK_GEOMETRY_H

#ifndef MESH_PROCESSOR_H
#define MESH_PROCESSOR_H

#include "../meshing/chunk_geometry.h"
#include "../processor/chunk_processor.h"

class MeshProcessor : public ChunkProcessor {

    ChunkGeometry geometry;

public:

    virtual Status process(ChunkRef* chunkRef, int priority, ProcessingData& proc_data) {

        Chunk* chunk = chunkRef->get();

        geometry.generate(chunk, proc_data);
        chunk->flags.set(ChunkFlags::MESHED);

        return Status::STOP;
    }

};

#endif // MESH_PROCESSOR_H

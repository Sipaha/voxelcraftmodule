#include "observers.h"

Observer::ChunkInfo *Observer::get_info(Int3 pos, bool add_if_absent) {

    ChunkInfo *info = chunks.getptr(pos);

    if (info == NULL && add_if_absent) {

        ChunkRef ref = Chunks::get_or_add(pos);
        chunks[pos] = ChunkInfo(ref);
        info = chunks.getptr(pos);

        if (!info->chunkRef->flags.get(ChunkFlags::LOADED)) {
            load_waiting.add(pos);
            Chunks::load(pos);
        }
    }

    return info;
}

void Observer::update_position(Vector3& position) {

    Int3 chunk_pos = WORLD_TO_CHUNK(position);
    Int3 diff = observer_pos - chunk_pos;

    if (diff.x > 1 || diff.x < -1 ||
        diff.y > 1 || diff.y < -1 ||
        diff.z > 1 || diff.z < -1) {

        observer_pos = chunk_pos;
        observers->update_position(this, observer_pos);
    }
}

void Observers::update_position(Observer* observer, Int3 new_pos) {
    pos_changed_queue.push(observer);
}

void Observers::update_transparency(Chunk *chunk) {

    ChunkBlocks* blocks = &chunk->blocks;

    for (int x = 0; x < CHUNK_BLOCKS_SIZE; x++) {
        for (int y = 0; y < CHUNK_BLOCKS_SIZE; y++) {
            if (blocks->get_block(x, y, 0).id == 0) {
                chunk->transparency |= vxl_t::flags[SIDE_ZZN];
                goto AFTER_Z_NEG;
            }
        }
    }
    AFTER_Z_NEG:

    for (int x = 0; x < CHUNK_BLOCKS_SIZE; x++) {
        for (int y = 0; y < CHUNK_BLOCKS_SIZE; y++) {
            if (blocks->get_block(x, y, CHUNK_BLOCKS_SIZE - 1).id == 0) {
                chunk->transparency |= vxl_t::flags[SIDE_ZZP];
                goto AFTER_Z_POS;
            }
        }
    }
    AFTER_Z_POS:

    for (int x = 0; x < CHUNK_BLOCKS_SIZE; x++) {
        for (int z = 0; z < CHUNK_BLOCKS_SIZE; z++) {
            if (blocks->get_block(x, 0, z).id == 0) {
                chunk->transparency |= vxl_t::flags[SIDE_ZNZ];
                goto AFTER_Y_NEG;
            }
        }
    }
    AFTER_Y_NEG:

    for (int x = 0; x < CHUNK_BLOCKS_SIZE; x++) {
        for (int z = 0; z < CHUNK_BLOCKS_SIZE; z++) {
            if (blocks->get_block(x, CHUNK_BLOCKS_SIZE - 1, z).id == 0) {
                chunk->transparency |= vxl_t::flags[SIDE_ZPZ];
                goto AFTER_Y_POS;
            }
        }
    }
    AFTER_Y_POS:

    for (int z = 0; z < CHUNK_BLOCKS_SIZE; z++) {
        for (int y = 0; y < CHUNK_BLOCKS_SIZE; y++) {
            if (blocks->get_block(0, y, z).id == 0) {
                chunk->transparency |= vxl_t::flags[SIDE_NZZ];
                goto AFTER_X_NEG;
            }
        }
    }
    AFTER_X_NEG:

    for (int z = 0; z < CHUNK_BLOCKS_SIZE; z++) {
        for (int y = 0; y < CHUNK_BLOCKS_SIZE; y++) {
            if (blocks->get_block(CHUNK_BLOCKS_SIZE - 1, y, z).id == 0) {
                chunk->transparency |= vxl_t::flags[SIDE_PZZ];
                goto AFTER_X_POS;
            }
        }
    }
    AFTER_X_POS:
    ;

    for (int i = SIDES_CONTACT_COUNT, ii = SIDES_COUNT; i < ii; i++) {
        if ((chunk->transparency & vxl_t::near_sides_flag[i]) != 0) {
            chunk->transparency |= 1 << i;
        }
    }
}

bool Observers::th_update(void* self) {
    Observers* obs = static_cast<Observers*>(self);
    return obs->th_update_impl();
}

bool Observers::th_update_impl() {

    ChunkRef* ref;
    Observer* obs;
    bool update_in_process = false;

    while (loaded_queue.pop(ref)) {

        update_in_process = true;
        List<Observer*>::Element *it = observers.front();

        while (it != NULL) {
            it->get()->load_waiting.remove(ref->get()->position);
            it = it->next();
        }

        memdelete(ref);
    }

    while (changed_queue.pop(ref)) {

        update_in_process = true;
        List<Observer*>::Element *it = observers.front();

        while (it != NULL) {

            Observer* obs = it->get();

            Observer::ChunkInfo* info = obs->get_info(ref->get()->position);

            if (info != NULL) {
                info->final_depth = 0;
                obs->front.push_back(info);
            }

            it = it->next();
        }

        memdelete(ref);
    }

    while (pos_changed_queue.pop(obs)) {

        Observer::ChunkInfo *info = obs->get_info(obs->observer_pos, true);

        info->distance = 0;
        info->final_depth = 0;
        info->from = NULL;

        float sqr_view_dist = obs->view_distance * obs->view_distance;

        List<Int3> to_remove;

        const Int3* key = NULL;
        while (key = obs->chunks.next(key)) {
            if (*key != info->chunkRef->position) {
                Int3 diff_vec = *key - info->chunkRef->position;
                if (diff_vec.sqr_length() > sqr_view_dist) {
                    to_remove.push_back(*key);
                } else {
                    obs->chunks.get(*key).distance = 99999;
                }
            }
        }

        List<Int3>::Element *remove_it = to_remove.front();
        while (remove_it != NULL) {
            obs->chunks.erase(remove_it->get());
            remove_it = remove_it->next();
        }

        obs->front.clear();
        obs->front.push_back(info);
    }

    List<Observer*>::Element *it = observers.front();

    while (it != NULL) {

        obs = it->get();

        if (obs->load_waiting.size() == 0) {

            List<Observer::ChunkInfo*> new_front;

            List<Observer::ChunkInfo*>::Element *front_it = obs->front.front();

            while (front_it != NULL) {

                Observer::ChunkInfo * const it_info = front_it->get();

                if (it_info->final_depth < 2 && it_info->distance < obs->view_distance) {

                    for (int side_idx = 0; side_idx < SIDES_COUNT; side_idx++) {

                        float new_distance = it_info->distance + vxl_t::side_distance[side_idx];

                        if (new_distance < obs->view_distance) {

                            Int3 near_pos = it_info->chunkRef->position + vxl_t::sides_vec[side_idx];
                            Observer::ChunkInfo* info = obs->get_info(near_pos, true);

                            if ((new_distance == info->distance && it_info->final_depth < info->final_depth) ||
                                (new_distance < info->distance && it_info->final_depth <= info->final_depth)) {

                                if (it_info->final_depth == 0) {
                                    info->final_depth = (it_info->chunkRef->transparency & side_idx) ? 0 : 1;
                                } else {
                                    info->final_depth = it_info->final_depth + 1;
                                }

                                info->distance = new_distance;
                                info->from = it_info;

                                if (info->final_depth < 2) {
                                    new_front.push_back(info);
                                }
                            }
                        }
                    }
                }

                front_it->erase();
                front_it = obs->front.front();
            }

            front_it = new_front.front();
            if (front_it != NULL) {
                update_in_process = true;
            }
            while (front_it != NULL) {
                obs->front.push_back(front_it->get());
                front_it = front_it->next();
            }
        }
        it = it->next();
    }

    Observer* new_obs;
    while (new_observers.pop(new_obs)) {
        observers.push_back(new_obs);
        update_in_process = true;
    }

    return update_in_process;
}

Observer *Observers::create_observer() {
    Observer* obs = memnew(Observer(this));
    new_observers.push(obs);
    return obs;
}

void Observers::chunk_loaded(ChunkRef* ref, int priority) {
    update_transparency(ref->get());
    loaded_queue.push(memnew(ChunkRef(ref->get())));
}

void Observers::chunk_updated(ChunkRef* ref, int priority) {
    update_transparency(ref->get());
    changed_queue.push(memnew(ChunkRef(ref->get())));
}

Observers::Observers() {
    ThreadPool::get_singleton()->create(Observers::th_update, this, 1);
}

Observers::~Observers() {
}

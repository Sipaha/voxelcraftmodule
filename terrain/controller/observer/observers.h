#ifndef OBSERVERS_PROCESSOR_H
#define OBSERVERS_PROCESSOR_H

#include "../../chunks/chunk.h"
#include "../../../utils/structs/int3.h"
#include "../chunk_conveyor.h"
#include "../../../utils/collections/ordered_hset.h"
#include "boost/lockfree/queue.hpp"

class Observer {

    friend class Observers;

    struct ChunkInfo {

        ChunkRef chunkRef;

        ChunkInfo* from = NULL;
        float distance = 9999;
        uint8_t final_depth = 100;

        ChunkInfo(ChunkRef& ref) {
            chunkRef = ref;
        }

        ChunkInfo() {}
    };

    HashMap<Int3, ChunkInfo, Int3> chunks;

private:

    OrderedHSet<Int3> load_waiting;

    List<ChunkInfo*> front;

    float view_distance = 320 / CHUNK_BLOCKS_SIZE;
    Observers* observers;

    Int3 observer_pos = Int3(-10000, -10000, -10000);

    Observer(Observers* obs) {
        observers = obs;
    }

protected:

    ChunkInfo* get_info(Int3 pos, bool add_if_absent = false);

public:

    void update_position(Vector3& position);

};

class Observers {

    friend class Observer;

    ChunkConveyor* conveyor;

    List<Observer*> observers;
    boost::lockfree::queue<Observer*> new_observers {20};

    boost::lockfree::queue<ChunkRef*> loaded_queue {50};
    boost::lockfree::queue<ChunkRef*> changed_queue {50};
    boost::lockfree::queue<Observer*> pos_changed_queue {10};

    void update_position(Observer* observer, Int3 new_pos);
    void update_transparency(Chunk* chunk);

protected:

    static bool th_update(void* self);
    bool th_update_impl();

public:

    Observer* create_observer();

    void chunk_loaded(ChunkRef* ref, int priority);
    void chunk_updated(ChunkRef* ref, int priority);

    Observers();
    ~Observers();

};

#endif // OBSERVERS_PROCESSOR_H

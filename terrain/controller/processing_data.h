#ifndef PROCESSING_DATA_H
#define PROCESSING_DATA_H

#include "color.h"
#include "light/light.h"

struct ProcessingData {

    static const size_t LIGHT_MAP_BYTES = sizeof(LightData) * CHUNK_BLOCKS_LIGHT_SIZE;

    LightData light_map[CHUNK_BLOCKS_LIGHT_SIZE];

    void clean_light_map() {
        memset(light_map, 0, LIGHT_MAP_BYTES);
    }

};

#endif // PROCESSING_DATA_H

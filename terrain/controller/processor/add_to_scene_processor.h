#ifndef ADD_TO_SCENE_PROCESSOR_H
#define ADD_TO_SCENE_PROCESSOR_H

#include "chunk_processor.h"
#include "../../terrain.h"

class AddToSceneProcessor :  public ChunkProcessor {

    virtual Status process(ChunkRef* chunk, int priority, ProcessingData& proc_data) {

        (*chunk)->mesh.build_mesh();
        (*chunk)->mesh.build_physics();

        return Status::CONTINUE;
    }
};

#endif // ADD_TO_SCENE_PROCESSOR_H

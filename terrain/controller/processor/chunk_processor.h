#ifndef CHUNK_PROCESSOR_H
#define CHUNK_PROCESSOR_H

#include "../../chunks/chunks.h"
#include "../processing_data.h"

class ChunkProcessor {

public:

    enum Status {
        CONTINUE,
        STOP,
        QUEUE_BACK
    };

    virtual Status process(ChunkRef* chunk, int priority, ProcessingData& proc_data) = 0;

    virtual ~ChunkProcessor() {}
};

#endif // CHUNK_PROCESSOR_H

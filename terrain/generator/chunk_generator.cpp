#include "chunk_generator.h"
#include "../terrain.h"

void ChunkGenerator::_notification(int p_what) {

    if (p_what == NOTIFICATION_ENTER_TREE || p_what == NOTIFICATION_EXIT_TREE) {

        ChunkGenerator* generator = p_what == NOTIFICATION_ENTER_TREE ? this : NULL;

        Node *parent = get_parent();

        if (parent != NULL) {
            Terrain::set_generator(generator);
        }
    }
}

void ChunkGenerator::_bind_methods() {

    //ClassDB::bind_method(D_METHOD("damage_block", "pos", "amount"), &TerrainNode::damage_block);
    //ClassDB::bind_method(D_METHOD("blow_up", "pos", "force"), &TerrainNode::blow_up);
    //ClassDB::bind_method(D_METHOD("add_block", "pos", "id"), &TerrainNode::add_block);
}

#ifndef CHUNK_GENERATOR_H
#define CHUNK_GENERATOR_H

#include "../chunks/chunk.h"

class ChunkGenerator : public Node {

    GDCLASS(ChunkGenerator, Node)

protected:

    virtual void generateImpl(Chunk* chunk) = 0;

    void _notification(int p_what);
    static void _bind_methods();

public:

    void generate(Chunk* chunk) {
        chunk->blocks.lock();
        generateImpl(chunk);
        chunk->blocks.changed_neighbours = 0;
        chunk->blocks.data_changed = false;
        chunk->blocks.unlock();
    }
};

#endif // CHUNK_GENERATOR_H

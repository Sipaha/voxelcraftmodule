#ifndef FAST_NOISE_GENERATOR_H
#define FAST_NOISE_GENERATOR_H

#include "chunk_generator.h"
#include "../../thirdparty/FastNoiseSIMD/FastNoiseSIMD.h"

class FastNoiseGenerator : public ChunkGenerator {

    GDCLASS(FastNoiseGenerator, ChunkGenerator)

    FastNoiseSIMD* noise;

    int seed = 51123412;

protected:

    void generateImpl(Chunk* chunk) {

        const Int3& pos = chunk->position;
        Vector3 base(pos.x * CHUNK_BLOCKS_SIZE, pos.y * CHUNK_BLOCKS_SIZE, pos.z * CHUNK_BLOCKS_SIZE);

        ChunkBlocks* blocks = &chunk->blocks;

        float *noiseSet = noise->GetSimplexSet(base.x, base.y, base.z, CHUNK_BLOCKS_SIZE, CHUNK_BLOCKS_SIZE, CHUNK_BLOCKS_SIZE);
        int idx = 0;

        for (int x = 0; x < CHUNK_BLOCKS_SIZE; x++) {
            for (int y = 0; y < CHUNK_BLOCKS_SIZE; y++) {
                for (int z = 0; z < CHUNK_BLOCKS_SIZE; z++) {

                    Block block;
                    double block_y = base.y + y;

                    double val = noiseSet[idx];

                    val -= block_y * 0.02;

                    if (val > 0) {
                        block.id = 1;
                    } else {
                        block.id = 0;
                    }

                    blocks->set_block_impl(idx++, block);
                }
            }
        }

        FastNoiseSIMD::FreeNoiseSet(noiseSet);

        if (pos.y > 0) {
            for (int i = 0; i < SIDES_CONTACT_COUNT; i++) {
                chunk->light.sun_light[i] = memnew(NeighbourSunLight(NeighbourSunLight::MAX_INTENCITY));
            }
        }

    }

    void set_seed(int seed) {
        this->seed = seed;
        noise->SetSeed(seed);
    }

    FastNoiseGenerator() {
        noise = FastNoiseSIMD::NewFastNoiseSIMD(seed);
        noise->SetFrequency(0.003f);
    }

    ~FastNoiseGenerator() {
        delete noise;
    }

};

#endif // FAST_NOISE_GENERATOR_H

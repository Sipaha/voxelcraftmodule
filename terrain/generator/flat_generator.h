#ifndef FLAT_GENERATOR_H
#define FLAT_GENERATOR_H

#include "chunk_generator.h"

class FlatGenerator : public ChunkGenerator {

    GDCLASS(FlatGenerator, ChunkGenerator)

    int level = 0;
    uint16_t block_id = 2;

protected:

    void generateImpl(Chunk* chunk) {

        const Int3& pos = chunk->position;
        Vector3 base(pos.x * CHUNK_BLOCKS_SIZE, pos.y * CHUNK_BLOCKS_SIZE, pos.z * CHUNK_BLOCKS_SIZE);

        for (int x = 0; x < CHUNK_BLOCKS_SIZE; x++) {
            for (int y = 0; y < CHUNK_BLOCKS_SIZE; y++) {
                for (int z = 0; z < CHUNK_BLOCKS_SIZE; z++) {

                    Block block;
                    double block_y = base.y + y;

                    if (block_y > level) {
                        block.id = 0;
                    } else {
                        block.id = block_id;
                    }

                    chunk->blocks.set_block(x, y, z, block);
                }
            }
        }
    }

    int get_level() {
        return level;
    }

    void set_level(int level) {
        this->level = level;
    }

    uint16_t get_block() {
        return block_id;
    }

    void set_block(uint16_t block_id) {
        this->block_id = block_id;
    }

    static void _bind_methods() {

        ClassDB::bind_method(D_METHOD("set_level", "level"), &FlatGenerator::set_level);
        ClassDB::bind_method(D_METHOD("get_level"), &FlatGenerator::get_level);

        ClassDB::bind_method(D_METHOD("set_block", "block_id"), &FlatGenerator::set_block);
        ClassDB::bind_method(D_METHOD("get_block"), &FlatGenerator::get_block);

        ADD_PROPERTY(PropertyInfo(Variant::INT, "Block ID"), "set_block", "get_block");
        ADD_PROPERTY(PropertyInfo(Variant::INT, "Level"), "set_level", "get_level");
    }
};

#endif // FLAT_GENERATOR_H

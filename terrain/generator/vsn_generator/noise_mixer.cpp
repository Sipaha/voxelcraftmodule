#include "noise_mixer.h"

void NoiseMixerVSNode::_bind_methods() {


}

VisualScriptNodeInstance *NoiseMixerVSNode::instance(VisualScriptInstance *p_instance) {

	/*
	Variant seed;
	if (!p_instance->get_variable(VAR_SEED, &seed)) {
		seed = 1337;
	}
	*/
	VisualScriptNodeInstanceNoiseMixer *instance = memnew(VisualScriptNodeInstanceNoiseMixer(this));
	instance->node = this;
	instance->instance = p_instance;
	return instance;
}



#include "simd_noise_vs_node.h"

const String SIMDNoiseVSNode::VAR_SEED = "Seed";

const char *SIMDNoiseVSNode::NOISE_TYPE_NAME[SIMD_NOISE_TYPES_COUNT] = {
	"Value",
	"ValueFractal",
	"Perlin",
	"PerlinFractal",
	"Simplex",
	"SimplexFractal",
	"WhiteNoise",
	"Cellular",
	"Cubic",
	"CubicFractal"
};

const char *SIMDNoiseVSNode::FRACTAL_TYPE_NAME[SIMD_FRACTAL_TYPES_COUNT] = {
	"FBM",
	"Billow",
	"RigidMulti"
};


const char *SIMDNoiseVSNode::CELLULAR_RETURN_TYPE_NAME[SIMD_CELLULAR_RETURN_TYPE_COUNT] = {
	"CellValue",
	"Distance",
	"Distance2",
	"Distance2Add",
	"Distance2Sub",
	"Distance2Mul",
	"Distance2Div",
	"NoiseLookup",
	"Distance2Cave"
};

const char *SIMDNoiseVSNode::CELLULAR_DIST_FUNC_NAME[SIMD_CELLULAR_DIST_FUNC_COUNT] = {
	"Euclidean",
	"Manhattan",
	"Natural"
};

const char *SIMDNoiseVSNode::PERTURB_TYPE_NAME[SIMD_PERTURB_TYPE_COUNT] = {
    "None",
    "Gradient",
    "GradientFractal",
    "Normalise",
    "Gradient_Normalise",
    "GradientFractal_Normalise"
};

void SIMDNoiseVSNode::_bind_methods() {

    ClassDB::bind_method(D_METHOD("set_noise_type", "which"), &SIMDNoiseVSNode::set_noise_type);
    ClassDB::bind_method(D_METHOD("get_noise_type"), &SIMDNoiseVSNode::get_noise_type);
    ClassDB::bind_method(D_METHOD("set_upsampling_amount", "which"), &SIMDNoiseVSNode::set_upsampling_amount);
    ClassDB::bind_method(D_METHOD("get_upsampling_amount"), &SIMDNoiseVSNode::get_upsampling_amount);
    ClassDB::bind_method(D_METHOD("set_frequency", "which"), &SIMDNoiseVSNode::set_frequency);
    ClassDB::bind_method(D_METHOD("get_frequency"), &SIMDNoiseVSNode::get_frequency);
    ClassDB::bind_method(D_METHOD("set_fractal_type", "which"), &SIMDNoiseVSNode::set_fractal_type);
    ClassDB::bind_method(D_METHOD("get_fractal_type"), &SIMDNoiseVSNode::get_fractal_type);
    ClassDB::bind_method(D_METHOD("set_fractal_octaves", "which"), &SIMDNoiseVSNode::set_fractal_octaves);
    ClassDB::bind_method(D_METHOD("get_fractal_octaves"), &SIMDNoiseVSNode::get_fractal_octaves);
    ClassDB::bind_method(D_METHOD("set_fractal_lacunarity", "which"), &SIMDNoiseVSNode::set_fractal_lacunarity);
    ClassDB::bind_method(D_METHOD("get_fractal_lacunarity"), &SIMDNoiseVSNode::get_fractal_lacunarity);
    ClassDB::bind_method(D_METHOD("set_fractal_gain", "which"), &SIMDNoiseVSNode::set_fractal_gain);
    ClassDB::bind_method(D_METHOD("get_fractal_gain"), &SIMDNoiseVSNode::get_fractal_gain);
    ClassDB::bind_method(D_METHOD("set_cellular_return_type", "which"), &SIMDNoiseVSNode::set_cellular_return_type);
    ClassDB::bind_method(D_METHOD("get_cellular_return_type"), &SIMDNoiseVSNode::get_cellular_return_type);
    ClassDB::bind_method(D_METHOD("set_cellular_distance_func", "which"), &SIMDNoiseVSNode::set_cellular_distance_func);
    ClassDB::bind_method(D_METHOD("get_cellular_distance_func"), &SIMDNoiseVSNode::get_cellular_distance_func);
    ClassDB::bind_method(D_METHOD("set_cellular_distance2_indicies0", "which"), &SIMDNoiseVSNode::set_cellular_distance2_indicies0);
    ClassDB::bind_method(D_METHOD("get_cellular_distance2_indicies0"), &SIMDNoiseVSNode::get_cellular_distance2_indicies0);
    ClassDB::bind_method(D_METHOD("set_cellular_distance2_indicies1", "which"), &SIMDNoiseVSNode::set_cellular_distance2_indicies1);
    ClassDB::bind_method(D_METHOD("get_cellular_distance2_indicies1"), &SIMDNoiseVSNode::get_cellular_distance2_indicies1);
    ClassDB::bind_method(D_METHOD("set_cellular_jitter", "which"), &SIMDNoiseVSNode::set_cellular_jitter);
    ClassDB::bind_method(D_METHOD("get_cellular_jitter"), &SIMDNoiseVSNode::get_cellular_jitter);
    ClassDB::bind_method(D_METHOD("set_cellular_noise_lookup_type", "which"), &SIMDNoiseVSNode::set_cellular_noise_lookup_type);
    ClassDB::bind_method(D_METHOD("get_cellular_noise_lookup_type"), &SIMDNoiseVSNode::get_cellular_noise_lookup_type);
    ClassDB::bind_method(D_METHOD("set_cellular_noise_lookup_freq", "which"), &SIMDNoiseVSNode::set_cellular_noise_lookup_freq);
    ClassDB::bind_method(D_METHOD("get_cellular_noise_lookup_freq"), &SIMDNoiseVSNode::get_cellular_noise_lookup_freq);
    ClassDB::bind_method(D_METHOD("set_perturb_type", "which"), &SIMDNoiseVSNode::set_perturb_type);
    ClassDB::bind_method(D_METHOD("get_perturb_type"), &SIMDNoiseVSNode::get_perturb_type);
    ClassDB::bind_method(D_METHOD("set_perturb_amp", "which"), &SIMDNoiseVSNode::set_perturb_amp);
    ClassDB::bind_method(D_METHOD("get_perturb_amp"), &SIMDNoiseVSNode::get_perturb_amp);
    ClassDB::bind_method(D_METHOD("set_perturb_frequency", "which"), &SIMDNoiseVSNode::set_perturb_frequency);
    ClassDB::bind_method(D_METHOD("get_perturb_frequency"), &SIMDNoiseVSNode::get_perturb_frequency);
    ClassDB::bind_method(D_METHOD("set_perturb_fractal_octaves", "which"), &SIMDNoiseVSNode::set_perturb_fractal_octaves);
    ClassDB::bind_method(D_METHOD("get_perturb_fractal_octaves"), &SIMDNoiseVSNode::get_perturb_fractal_octaves);
    ClassDB::bind_method(D_METHOD("set_perturb_fractal_lacunarity", "which"), &SIMDNoiseVSNode::set_perturb_fractal_lacunarity);
    ClassDB::bind_method(D_METHOD("get_perturb_fractal_lacunarity"), &SIMDNoiseVSNode::get_perturb_fractal_lacunarity);
    ClassDB::bind_method(D_METHOD("set_perturb_fractal_gain", "which"), &SIMDNoiseVSNode::set_perturb_fractal_gain);
    ClassDB::bind_method(D_METHOD("get_perturb_fractal_gain"), &SIMDNoiseVSNode::get_perturb_fractal_gain);
    ClassDB::bind_method(D_METHOD("set_perturb_normalize_length", "which"), &SIMDNoiseVSNode::set_perturb_normalize_length);
    ClassDB::bind_method(D_METHOD("get_perturb_normalize_length"), &SIMDNoiseVSNode::get_perturb_normalize_length);

    ClassDB::bind_method(D_METHOD("set_node_name", "which"), &SIMDNoiseVSNode::set_node_name);
    ClassDB::bind_method(D_METHOD("get_node_name"), &SIMDNoiseVSNode::get_node_name);

	ADD_PROPERTY(PropertyInfo(Variant::STRING, "node_name"), "set_node_name", "get_node_name");

	String noise_types_str = vxl_util::join_enum_variants(NOISE_TYPE_NAME, SIMD_NOISE_TYPES_COUNT);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "noise_type", PROPERTY_HINT_ENUM, noise_types_str), "set_noise_type", "get_noise_type");
	ADD_PROPERTY(PropertyInfo(Variant::INT, "upsampling_amount"), "set_upsampling_amount", "get_upsampling_amount");
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "frequency", PROPERTY_HINT_RANGE, "0.0,10.0,0.005"), "set_frequency", "get_frequency");

	String fractal_types_str = vxl_util::join_enum_variants(FRACTAL_TYPE_NAME, SIMD_FRACTAL_TYPES_COUNT);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "fractal_type", PROPERTY_HINT_ENUM, fractal_types_str), "set_fractal_type", "get_fractal_type");
	ADD_PROPERTY(PropertyInfo(Variant::INT, "fractal_octaves"), "set_fractal_octaves", "get_fractal_octaves");
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "fractal_lacunarity", PROPERTY_HINT_RANGE, "0.0,10.0,0.005"), "set_fractal_lacunarity", "get_fractal_lacunarity");
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "fractal_gain", PROPERTY_HINT_RANGE, "0.0,10.0,0.005"), "set_fractal_gain", "get_fractal_gain");

	String cell_return_type_str = vxl_util::join_enum_variants(CELLULAR_RETURN_TYPE_NAME, SIMD_CELLULAR_RETURN_TYPE_COUNT);
	String cell_dist_func_str = vxl_util::join_enum_variants(CELLULAR_DIST_FUNC_NAME, SIMD_CELLULAR_DIST_FUNC_COUNT);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "cell_ret_type", PROPERTY_HINT_ENUM, cell_return_type_str), "set_cellular_return_type", "get_cellular_return_type");
	ADD_PROPERTY(PropertyInfo(Variant::INT, "cell_dist_func", PROPERTY_HINT_ENUM, cell_dist_func_str), "set_cellular_distance_func", "get_cellular_distance_func");
	ADD_PROPERTY(PropertyInfo(Variant::INT, "cell_dist2_ind0"), "set_cellular_distance2_indicies0", "get_cellular_distance2_indicies0");
	ADD_PROPERTY(PropertyInfo(Variant::INT, "cell_dist2_ind1"), "set_cellular_distance2_indicies1", "get_cellular_distance2_indicies1");
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "cell_jitter", PROPERTY_HINT_RANGE, "0.0,10.0,0.005"), "set_cellular_jitter", "get_cellular_jitter");
	ADD_PROPERTY(PropertyInfo(Variant::INT, "cell_n_lookup_type", PROPERTY_HINT_ENUM, noise_types_str), "set_cellular_noise_lookup_type", "get_cellular_noise_lookup_type");
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "cell_n_lookup_freq", PROPERTY_HINT_RANGE, "0.0,10.0,0.005"), "set_cellular_noise_lookup_freq", "get_cellular_noise_lookup_freq");

	String perturb_type_str = vxl_util::join_enum_variants(PERTURB_TYPE_NAME, SIMD_PERTURB_TYPE_COUNT);
	ADD_PROPERTY(PropertyInfo(Variant::INT, "ptb_type", PROPERTY_HINT_ENUM, perturb_type_str), "set_perturb_type", "get_perturb_type");
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "ptb_amp", PROPERTY_HINT_RANGE, "0.0,10.0,0.005"), "set_perturb_amp", "get_perturb_amp");
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "ptb_frequency", PROPERTY_HINT_RANGE, "0.0,10.0,0.005"), "set_perturb_frequency", "get_perturb_frequency");
	ADD_PROPERTY(PropertyInfo(Variant::INT, "ptb_fcl_octaves"), "set_perturb_fractal_octaves", "get_perturb_fractal_octaves");
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "ptb_fcl_lacunarity", PROPERTY_HINT_RANGE, "0.0,10.0,0.005"), "set_perturb_fractal_lacunarity", "get_perturb_fractal_lacunarity");
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "ptb_fcl_gain", PROPERTY_HINT_RANGE, "0.0,10.0,0.005"), "set_perturb_fractal_gain", "get_perturb_fractal_gain");
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "ptb_normalize_length", PROPERTY_HINT_RANGE, "0.0,10.0,0.005"), "set_perturb_normalize_length", "get_perturb_normalize_length");

	//noise type
	BIND_ENUM_CONSTANT(FastNoiseSIMD::Value);
	BIND_ENUM_CONSTANT(FastNoiseSIMD::ValueFractal);
	BIND_ENUM_CONSTANT(FastNoiseSIMD::Perlin);
	BIND_ENUM_CONSTANT(FastNoiseSIMD::PerlinFractal);
	BIND_ENUM_CONSTANT(FastNoiseSIMD::Simplex);
	BIND_ENUM_CONSTANT(FastNoiseSIMD::SimplexFractal);
	BIND_ENUM_CONSTANT(FastNoiseSIMD::WhiteNoise);
	BIND_ENUM_CONSTANT(FastNoiseSIMD::Cellular);
	BIND_ENUM_CONSTANT(FastNoiseSIMD::Cubic);
	BIND_ENUM_CONSTANT(FastNoiseSIMD::CubicFractal);

	//fractal type
	BIND_ENUM_CONSTANT(FastNoiseSIMD::FBM);
	BIND_ENUM_CONSTANT(FastNoiseSIMD::Billow);
	BIND_ENUM_CONSTANT(FastNoiseSIMD::RigidMulti);

	//perturb type
	BIND_ENUM_CONSTANT(FastNoiseSIMD::None);
	BIND_ENUM_CONSTANT(FastNoiseSIMD::Gradient);
	BIND_ENUM_CONSTANT(FastNoiseSIMD::GradientFractal);
	BIND_ENUM_CONSTANT(FastNoiseSIMD::Normalise);
	BIND_ENUM_CONSTANT(FastNoiseSIMD::Gradient_Normalise);
	BIND_ENUM_CONSTANT(FastNoiseSIMD::GradientFractal_Normalise);

	//cellular distance func
	BIND_ENUM_CONSTANT(FastNoiseSIMD::Euclidean);
	BIND_ENUM_CONSTANT(FastNoiseSIMD::Manhattan);
	BIND_ENUM_CONSTANT(FastNoiseSIMD::Natural);

	//cellular return type
	BIND_ENUM_CONSTANT(FastNoiseSIMD::CellValue);
	BIND_ENUM_CONSTANT(FastNoiseSIMD::Distance);
	BIND_ENUM_CONSTANT(FastNoiseSIMD::Distance2);
	BIND_ENUM_CONSTANT(FastNoiseSIMD::Distance2Add);
	BIND_ENUM_CONSTANT(FastNoiseSIMD::Distance2Sub);
	BIND_ENUM_CONSTANT(FastNoiseSIMD::Distance2Mul);
	BIND_ENUM_CONSTANT(FastNoiseSIMD::Distance2Div);
	BIND_ENUM_CONSTANT(FastNoiseSIMD::NoiseLookup);
	BIND_ENUM_CONSTANT(FastNoiseSIMD::Distance2Cave);
}

VisualScriptNodeInstance *SIMDNoiseVSNode::instance(VisualScriptInstance *p_instance) {

	Variant seed;
	if (!p_instance->get_variable(VAR_SEED, &seed)) {
		seed = 1337;
	}
	VisualScriptNodeInstanceSIMDNoise *instance = memnew(VisualScriptNodeInstanceSIMDNoise(this, seed));
	instance->node = this;
	instance->instance = p_instance;
	return instance;
}



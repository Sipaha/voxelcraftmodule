#ifndef SIMD_NOISE_VS_NODE_H
#define SIMD_NOISE_VS_NODE_H

#include "modules/visual_script/visual_script.h"
#include "reference.h"
#include "../../../thirdparty/FastNoiseSIMD/FastNoiseSIMD.h"
#include "../../../constants.h"
#include "../../../utils/misc/vxl_util.h"

#define SIMD_NOISE_TYPES_COUNT 10
#define SIMD_FRACTAL_TYPES_COUNT 3
#define SIMD_CELLULAR_RETURN_TYPE_COUNT 9
#define SIMD_CELLULAR_DIST_FUNC_COUNT 3
#define SIMD_PERTURB_TYPE_COUNT 6

class VisualScriptNodeInstanceSIMDNoise;

class SIMDNoiseVSNode : public VisualScriptNode {

    friend class VisualScriptNodeInstanceSIMDNoise;

    GDCLASS(SIMDNoiseVSNode, VisualScriptNode)

protected:

    //general
    FIELD_GET_SET(FastNoiseSIMD::NoiseType, noise_type, FastNoiseSIMD::SimplexFractal)
    FIELD_GET_SET(int, upsampling_amount, 0)
    FIELD_GET_SET(float, frequency, 0.5f)

    //fractal
    FIELD_GET_SET(FastNoiseSIMD::FractalType, fractal_type, FastNoiseSIMD::FBM)
    FIELD_GET_SET(int, fractal_octaves, 5)
    FIELD_GET_SET(float, fractal_lacunarity, 2.0f)
    FIELD_GET_SET(float, fractal_gain, 0.5f)

    //cellular
    FIELD_GET_SET(FastNoiseSIMD::CellularReturnType, cellular_return_type, FastNoiseSIMD::NoiseLookup)
    FIELD_GET_SET(FastNoiseSIMD::CellularDistanceFunction, cellular_distance_func, FastNoiseSIMD::Manhattan)
    FIELD_GET_SET(int, cellular_distance2_indicies0, 0)
    FIELD_GET_SET(int, cellular_distance2_indicies1, 1)
    FIELD_GET_SET(float, cellular_jitter, 0.5f)
    FIELD_GET_SET(FastNoiseSIMD::NoiseType, cellular_noise_lookup_type, FastNoiseSIMD::Simplex)
    FIELD_GET_SET(float, cellular_noise_lookup_freq, 1.2f)

    //perturb
    FIELD_GET_SET(FastNoiseSIMD::PerturbType, perturb_type, FastNoiseSIMD::Gradient)
    FIELD_GET_SET(float, perturb_amp, 1.0f)
    FIELD_GET_SET(float, perturb_frequency, 0.5f)
    FIELD_GET_SET(int, perturb_fractal_octaves, 3)
    FIELD_GET_SET(float, perturb_fractal_lacunarity, 2.0f)
    FIELD_GET_SET(float, perturb_fractal_gain, 0.5f)
    FIELD_GET_SET(float, perturb_normalize_length, 1.0f)

    //other
    FIELD_GET_SET(String, node_name, "SIMDNoise")

    static const String VAR_SEED;
    static const char *NOISE_TYPE_NAME[SIMD_NOISE_TYPES_COUNT];
    static const char *FRACTAL_TYPE_NAME[SIMD_FRACTAL_TYPES_COUNT];
    static const char *CELLULAR_RETURN_TYPE_NAME[SIMD_CELLULAR_RETURN_TYPE_COUNT];
    static const char *CELLULAR_DIST_FUNC_NAME[SIMD_CELLULAR_DIST_FUNC_COUNT];
    static const char *PERTURB_TYPE_NAME[SIMD_PERTURB_TYPE_COUNT];

    static void _bind_methods();

public:

    virtual int get_output_sequence_port_count() const {
        return 0;
    }

    virtual bool has_input_sequence_port() const {
        return false;
    }

    virtual String get_output_sequence_port_text(int p_port) const {
        return String();
    }

    virtual bool has_mixed_input_and_sequence_ports() const {
        return false;
    }

	virtual int get_input_value_port_count() const {
		return 1;
	}

	virtual int get_output_value_port_count() const {
		return 1;
	}

	virtual PropertyInfo get_input_value_port_info(int p_idx) const {
		switch (p_idx) {
		case 0:
			return PropertyInfo(Variant::VECTOR3, "position");
		default:
			return PropertyInfo(Variant::REAL, "unknown");
		}
	}

	virtual PropertyInfo get_output_value_port_info(int p_idx) const {
		return PropertyInfo(Variant::POOL_BYTE_ARRAY, "float[]");
	}

	virtual String get_caption() const {
		return node_name;
	}

	virtual String get_text() const {
		return NOISE_TYPE_NAME[get_noise_type()];
	}

	virtual String get_category() const {
		return "functions";
	}

	virtual VisualScriptNodeInstance *instance(VisualScriptInstance *p_instance);

	//virtual TypeGuess guess_output_type(TypeGuess *p_inputs, int p_output) const;

	SIMDNoiseVSNode() {
	}
};

class VisualScriptNodeInstanceSIMDNoise : public VisualScriptNodeInstance {

	static const size_t DATA_BYTES_COUNT = sizeof(float) * CHUNK_BLOCKS_COUNT;

public:

	SIMDNoiseVSNode *node;
	VisualScriptInstance *instance;

	FastNoiseSIMD* noise;

	int upsampling = 0;

	float* noise_set;
	PoolByteArray noise_result;

	//virtual int get_working_memory_size() const { return 0; }
	//virtual bool is_output_port_unsequenced(int p_idx) const { return false; }
	//virtual bool get_output_port_unsequenced(int p_idx,Variant* r_value,Variant* p_working_mem,String &r_error) const { return true; }

	virtual int step(const Variant **p_inputs, Variant **p_outputs, StartMode p_start_mode, Variant *p_working_mem, Variant::CallError &r_error, String &r_error_str) {

		Vector3 position = *p_inputs[0];

		noise->FillNoiseSet(noise_set,
							(int) position.x,
							(int) position.y,
							(int) position.z,
							CHUNK_BLOCKS_SIZE,
							CHUNK_BLOCKS_SIZE,
							CHUNK_BLOCKS_SIZE, 1.0f);

		PoolByteArray::Write write = noise_result.write();
		memcpy(write.ptr(), noise_set, DATA_BYTES_COUNT);
		write = PoolByteArray::Write();

		*p_outputs[0] = noise_result;

		return 0;
	}

	VisualScriptNodeInstanceSIMDNoise(SIMDNoiseVSNode* node, int seed) {

		noise = FastNoiseSIMD::NewFastNoiseSIMD(seed);
		this->node = node;
		this->upsampling = node->get_upsampling_amount();

		noise->SetNoiseType(node->get_noise_type());
		noise->SetFrequency(node->get_frequency());
		noise->SetFractalType(node->get_fractal_type());
		noise->SetFractalOctaves(node->get_fractal_octaves());
		noise->SetFractalLacunarity(node->get_fractal_lacunarity());
		noise->SetFractalGain(node->get_fractal_gain());
		noise->SetCellularReturnType(node->get_cellular_return_type());
		noise->SetCellularDistanceFunction(node->get_cellular_distance_func());
		noise->SetCellularDistance2Indicies(node->get_cellular_distance2_indicies0(), node->get_cellular_distance2_indicies1());
		noise->SetCellularJitter(node->get_cellular_jitter());
		noise->SetCellularNoiseLookupType(node->get_cellular_noise_lookup_type());
		noise->SetCellularNoiseLookupFrequency(node->get_cellular_noise_lookup_freq());
		noise->SetPerturbType(node->get_perturb_type());
		noise->SetPerturbAmp(node->get_perturb_amp());
		noise->SetPerturbFrequency(node->get_perturb_frequency());
		noise->SetPerturbFractalOctaves(node->get_perturb_fractal_octaves());
		noise->SetPerturbFractalLacunarity(node->get_perturb_fractal_lacunarity());
		noise->SetPerturbFractalGain(node->get_perturb_fractal_gain());
		noise->SetPerturbNormaliseLength(node->get_perturb_normalize_length());

		noise_set = noise->GetEmptySet(CHUNK_BLOCKS_COUNT);
		noise_result.resize(DATA_BYTES_COUNT);
	}

	~VisualScriptNodeInstanceSIMDNoise() {
		noise->FreeNoiseSet(noise_set);
		delete noise;
	}
};

VARIANT_ENUM_CAST(FastNoiseSIMD::NoiseType)
VARIANT_ENUM_CAST(FastNoiseSIMD::FractalType)
VARIANT_ENUM_CAST(FastNoiseSIMD::CellularReturnType)
VARIANT_ENUM_CAST(FastNoiseSIMD::CellularDistanceFunction)
VARIANT_ENUM_CAST(FastNoiseSIMD::PerturbType)

#endif // SIMD_NOISE_VS_NODE_H

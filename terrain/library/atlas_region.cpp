﻿#include "atlas_region.h"

void AtlasRegion::set_data(float inv_w, float inv_h, Point2 pos, Rect2 rect) {

	x = inv_w * pos.x;
	y = inv_h * pos.y;
	w = inv_w * rect.get_size().x;
	h = inv_h * rect.get_size().y;

	uv[0] = Vector2(x, y + h);
	uv[1] = Vector2(x, y);
	uv[2] = Vector2(x + w, y);
	uv[3] = Vector2(x + w, y + h);

	isValid = true;
}

AtlasRegion::AtlasRegion() {
}

﻿#ifndef ATLAS_REGION_H
#define ATLAS_REGION_H

#include "../../../core/math/math_2d.h"

struct AtlasRegion {

	Vector2 uv[4];
	float x, y, w, h;

	bool isValid = false;

	void set_data(float inv_w, float inv_h, Point2 pos, Rect2 rect);

	AtlasRegion();
};

#endif // !ATLAS_REGION_H

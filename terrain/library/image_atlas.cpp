﻿#include "image_atlas.h"

const AtlasRegion* ImageAtlas::get_region(String& path) {
	ImageData* imData = images_data.getptr(path);
	return imData != NULL ? &imData->region : NULL;
}

void ImageAtlas::add_image(String& path) {
	images_data[path] = ImageData();
}

void ImageAtlas::update() {

	initialized = true;

	_load_images();

	_create_atlas_image();
	_fill_atlas_image();

	_release_images();

    uint32_t flags = Texture::FLAG_FILTER;
    texture->create_from_image(atlas_image, flags);
}

void ImageAtlas::_load_images() {

	const String* path = NULL;
	while (path = images_data.next(path)) {

		ImageData* data = images_data.getptr(*path);

		data->image = Ref<Image>(memnew(Image));
		data->image->load(*path);
	}
}

void ImageAtlas::_create_atlas_image() {

	int atlas_w = 0;
	int atlas_h = 0;

	Image::AlphaMode alpha = Image::ALPHA_NONE;

	const String* path = NULL;

	while (path = images_data.next(path)) {

		ImageData im_data = images_data[*path];
		Ref<Image> image = im_data.image;

		atlas_w += image->get_width() + TEXTURE_BORDER_SIZE * 2;
		atlas_h = MAX(atlas_h, image->get_height() + TEXTURE_BORDER_SIZE * 2);

		if (alpha == Image::ALPHA_NONE) {
			alpha = image->detect_alpha();
		}
	}

	//int atlas_size = nearest_power_of_2_templated(MAX(atlas_w, atlas_h));

	int atlas_size = 2048;
	Image::Format format = alpha ? Image::FORMAT_RGBA8 : Image::FORMAT_RGB8;

	atlas_image = Ref<Image>(memnew(Image(atlas_size, atlas_size, true, format)));
}

void ImageAtlas::_fill_atlas_image() {

	int pixel_size = Image::get_format_pixel_size(atlas_image->get_format());

	float atlas_w = atlas_image->get_width();
	float atlas_h = atlas_image->get_height();
	float inv_width = 1 / float(atlas_w);
	float inv_height = 1 / float(atlas_h);

	int x = TEXTURE_BORDER_SIZE;
	int y = TEXTURE_BORDER_SIZE;

	int next_y = 0;

	const String* path = NULL;
	while (path = images_data.next(path)) {

		ImageData* im_data = images_data.getptr(*path);

		Ref<Image> image = im_data->image;

		int im_w = image->get_width();
		int im_h = image->get_height();

		next_y = MAX(y + im_h, next_y);
		if (x + im_w + TEXTURE_BORDER_SIZE * 2 > atlas_w) {
			x = TEXTURE_BORDER_SIZE;
			y = next_y;
		}

		Rect2 src_rect = Rect2(0, 0, im_w, im_h);
		Point2 dest_pos = Point2(x, y);

		image->convert(atlas_image->get_format());
		//image->generate_mipmaps();

		//OS::get_singleton()->print("fill base \n");

		atlas_image->blit_rect(image, src_rect, dest_pos);

		/*
		for (int i = 0; i < TEXTURE_BORDER_SIZE; i++) {
		atlas_image->blit_rect(image, Rect2(0, 0, 1, im_h), Point2(dest_pos.x - 1 - i, dest_pos.y));
		atlas_image->blit_rect(image, Rect2(im_w - 1, 0, 1, im_h), Point2(dest_pos.x + im_w + i, dest_pos.y));
		atlas_image->blit_rect(image, Rect2(0, 0, im_w, 1), Point2(dest_pos.x, dest_pos.y - 1 - i));
		atlas_image->blit_rect(image, Rect2(0, im_h - 1, im_w, 1), Point2(dest_pos.x, dest_pos.y + im_h + i));
		}
		*/

		im_data->region.set_data(inv_width, inv_height, dest_pos, src_rect);
		/*
		atlas_image->lock();

		PoolVector<uint8_t>::Read im_src_read = image->get_data().read();
		const uint8_t* im_src = im_src_read.ptr();
		uint8_t* atlas_im_dst = atlas_image->write_lock.ptr();

		int mm = atlas_image->get_mipmap_count();

		for (int i = 1; i < mm; i++) {

			OS::get_singleton()->print("Fill mipmap %d\n", i);
			
			int a_mm_offset = 0, a_mm_size = 0;
			int a_mm_width = 0, a_mm_height = 0;
			atlas_image->get_mipmap_offset_size_and_dimensions(i, a_mm_offset, a_mm_size, a_mm_width, a_mm_height);
			OS::get_singleton()->print("atlas off: %d w: %d h: %d s: %d\n", a_mm_offset, a_mm_width, a_mm_height, a_mm_size);
			uint8_t* dst_data_ptr = &atlas_im_dst[a_mm_offset];

			int r_dst_pos_x = Math::round(a_mm_width * im_data->region.x);
			int r_dst_pos_y = Math::round(a_mm_height * im_data->region.y);

			int im_mm_offset = 0, im_mm_size = 0;
			int im_mm_width = 0, im_mm_height = 0;
			image->get_mipmap_offset_size_and_dimensions(i, im_mm_offset, im_mm_size, im_mm_width, im_mm_height);
			OS::get_singleton()->print("im off: %d w: %d h: %d s: %d\n", im_mm_offset, im_mm_width, im_mm_height, im_mm_size);
			const uint8_t* src_data_ptr = &im_src[im_mm_offset];
			
			for (int y = 0; y < im_mm_height; y++) {

				for (int x = 0; x < im_mm_width; x++) {

					int dst_x = r_dst_pos_x + x;
					int dst_y = r_dst_pos_y + y;

					int src_idx = (y * im_mm_width + x) * pixel_size;
					int dst_idx = (dst_y * a_mm_width + dst_x) * pixel_size;

					const uint8_t *src = &src_data_ptr[src_idx];
					uint8_t *dst = &dst_data_ptr[dst_idx];

					for (int k = 0; k < pixel_size; k++) {
						dst[k] = src[k];
					}
				}
			}
		}

		atlas_image->unlock();
		im_src_read = PoolVector<uint8_t>::Read();
		*/
		/*
		data->image->lock();

		atlas->set_pixel(dest_pos.x - 1, dest_pos.y - 1, data->image->get_pixel(0, 0));
		atlas->set_pixel(dest_pos.x + im_w, dest_pos.y - 1, data->image->get_pixel(im_w - 1, 0));
		atlas->set_pixel(dest_pos.x - 1, dest_pos.y + im_h, data->image->get_pixel(0, im_h - 1));
		atlas->set_pixel(dest_pos.x + im_w, dest_pos.y + im_h, data->image->get_pixel(im_w - 1, im_h - 1));

		data->image->unlock();
		*/

		x += im_w;
	}

	atlas_image->generate_mipmaps();
}

void ImageAtlas::_release_images() {

	const String* path = NULL;

	while (path = images_data.next(path)) {

		ImageData* data = images_data.getptr(*path);

		data->image = Ref<Image>();
	}
}

void ImageAtlas::save_image(String& path) {

	OS::get_singleton()->print("SAVE MIPMAPS\n");

	atlas_image->save_png(path + "/test_orig.png");

	int mm_c = atlas_image->get_mipmap_count();

	PoolVector<uint8_t> data = atlas_image->get_data();

    /*
	for (int i = 0; i < mm_c; i++) {
		int offset = 0;
		int size = 0;
		int w = 0, h = 0;

		atlas_image->get_mipmap_offset_size_and_dimensions(i, offset, size, w, h);

		OS::get_singleton()->print("Params %d: off: %d, s: %d w: %d h: %d\n", i, offset, size, w, h);

		Ref<Image> image = Ref<Image>(memnew(Image(w, h, false, atlas_image->get_format(), data.subarray(offset, offset + size - 1))));
		OS::get_singleton()->print("image created");

		String img_path = path + "/test_mm_" + String::num(i) + ".png";
		OS::get_singleton()->print("save by name: %s\n", img_path.utf8().get_data());
		image->save_png(img_path);
	}
*/
	
}

ImageAtlas::ImageAtlas() {
	texture = Ref<ImageTexture>(memnew(ImageTexture));
}

ImageAtlas::~ImageAtlas() {

}

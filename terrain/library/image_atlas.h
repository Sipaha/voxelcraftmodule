﻿#ifndef TEXTURE_ATLAS_H
#define TEXTURE_ATLAS_H

#include "image.h"
#include "scene/resources/texture.h"
#include "atlas_region.h"
#include "io/image_loader.h"
#include "os/os.h"

#define TEXTURE_BORDER_SIZE 0

class ImageAtlas : public Reference {
	
private:

	struct ImageData {
		AtlasRegion region;
		Ref<Image> image;
		String smd5;
	};

	Ref<ImageTexture> texture;
	HashMap<String, ImageData> images_data;
	
	Ref<Image> atlas_image;

	bool initialized = false;

	void _load_images();
	void _release_images();
	void _create_atlas_image();
	void _fill_atlas_image();

public:

	const AtlasRegion* get_region(String& path);

	void add_image(String& path);

	void update();

	Ref<ImageTexture> get_texture() {
		return texture;
	}

	void save_image(String& path);

	ImageAtlas();
	~ImageAtlas();
};

#endif // !TEXTURE_ATLAS_H

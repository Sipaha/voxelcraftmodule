﻿#include "voxel_library.h"

void VoxelLibrary::_init_blocks_info(String blocks_root) {

	DirAccess *blocks_root_dir = DirAccess::open(blocks_root);

	if (blocks_root_dir) {

		blocks_root_dir->list_dir_begin();

		List<String> dirs;
		int max_id = 0;

		String next = blocks_root_dir->get_next();
		while (next != String()) {

			OS::get_singleton()->print("Check block folder: '%s'\n", next.utf8().get_data());

			if (blocks_root_dir->current_is_dir()) {
				if (next.is_valid_integer()) {
					int id = next.to_int();
					if (id > 0) {
						max_id = MAX(max_id, id);
						dirs.push_back(next);
					}
				}
			}
			next = blocks_root_dir->get_next();
		}

		blocks_root_dir->list_dir_end();

		OS::get_singleton()->print("Found %d blocks\n", max_id + 1);

		blocks_size = max_id + 1;
		blocks = new BlockInfo[blocks_size];

		for (List<String>::Element *E = dirs.front(); E; E = E->next()) {

			OS::get_singleton()->print("Process block folder: '%s'\n", E->get().utf8().get_data());

			int id = E->get().to_int();
			blocks_root_dir->change_dir(E->get());

			blocks[id] = _load_block_info(id, blocks_root_dir);

			BlockInfo* info = &blocks[id];

			blocks_root_dir->change_dir("..");
		}

		atlas.update();
		String path = "res://assets";
		atlas.save_image(path);

		memdelete(blocks_root_dir);
	}
	else {

		OS::get_singleton()->printerr("Directory not found! path: '%s'\n", blocks_root);
	}
}

BlockInfo VoxelLibrary::_load_block_info(int id, DirAccess *block_dir) {

	BlockInfo info;

	info.id = id;

	block_dir->change_dir("textures");
	String textures_base = block_dir->get_current_dir() + "/";

	block_dir->list_dir_begin();

	String next = block_dir->get_next();
	while (next != String()) {
		if (next.ends_with(".png")) {
			atlas.add_image(textures_base + next);
		}
		next = block_dir->get_next();
	}
	
	block_dir->list_dir_end();

	block_dir->change_dir("..");

    //atlas.save_image(block_dir->get_current_dir());

	Dictionary model = read_model(block_dir);
	fill_model(&info, &model, textures_base);

	return info;
}

void VoxelLibrary::fill_model(BlockInfo* info, Dictionary* data, String textures_base) {

	OS::get_singleton()->print("Start model filling \n");

	Dictionary faces = data->get_valid("faces").operator Dictionary();
	
	info->faces[SIDE_NZZ] = atlas.get_region(textures_base + faces.get_valid("X_NEG").operator String());
	info->faces[SIDE_PZZ] = atlas.get_region(textures_base + faces.get_valid("X_POS").operator String());
	info->faces[SIDE_ZNZ] = atlas.get_region(textures_base + faces.get_valid("Y_NEG").operator String());
	info->faces[SIDE_ZPZ] = atlas.get_region(textures_base + faces.get_valid("Y_POS").operator String());
	info->faces[SIDE_ZZN] = atlas.get_region(textures_base + faces.get_valid("Z_NEG").operator String());
	info->faces[SIDE_ZZP] = atlas.get_region(textures_base + faces.get_valid("Z_POS").operator String());
	
	ShaderMaterial* material = memnew(ShaderMaterial);
	material->set_shader_param("texture_albedo", atlas.get_texture());

	RES shader = ResourceLoader::load("res://assets/shaders/world_main.shader");
	VS::get_singleton()->material_set_shader(material->get_rid(), shader->get_rid());

	//material->set_pa


	/*
	 *
	 * onion.capture.material = Ref<ShaderMaterial>(memnew(ShaderMaterial));

	onion.capture.shader = Ref<Shader>(memnew(Shader));
	onion.capture.shader->set_code(" \
		shader_type canvas_item; \
		\
		uniform vec4 bkg_color; \
		uniform vec4 dir_color; \
		uniform bool differences_only; \
		uniform sampler2D present; \
		\
		float zero_if_equal(vec4 a, vec4 b) { \
			return smoothstep(0.0, 0.005, length(a.rgb - b.rgb) / sqrt(3.0)); \
		} \
		\
		void fragment() { \
			vec4 capture_samp = texture(TEXTURE, UV); \
			vec4 present_samp = texture(present, UV); \
			float bkg_mask = zero_if_equal(capture_samp, bkg_color); \
			float diff_mask = 1.0 - zero_if_equal(present_samp, bkg_color); \
			diff_mask = min(1.0, diff_mask + float(!differences_only)); \
			COLOR = vec4(capture_samp.rgb * dir_color.rgb, bkg_mask * diff_mask); \
		} \
	");
	VS::get_singleton()->material_set_shader(onion.capture.material->get_rid(), onion.capture.shader->get_rid());
	 *
	 * */


    //SpatialMaterial* material = memnew(SpatialMaterial);
    //material->set_metallic(0);
    //material->set_texture(SpatialMaterial::TEXTURE_ALBEDO, atlas.get_texture());
    //material->set_depth_draw_mode(SpatialMaterial::DEPTH_DRAW_ALPHA_OPAQUE_PREPASS);
    //material->set_specular(0);
    //material->set_roughness(1);
    //material->set_albedo(Color(1, 1, 1, 1));
    //material->set_flag(SpatialMaterial::FLAG_USE_VERTEX_LIGHTING, true);
    //material->set_flag(SpatialMaterial::FLAG_UNSHADED, true);

	info->material = Ref<Material>(material);
	info->shader = shader;
}

Dictionary VoxelLibrary::read_model(DirAccess *block_dir) {

	OS::get_singleton()->print("Begin model reading '%s/model.json'\n", block_dir->get_current_dir().utf8().get_data());

	String model_path = block_dir->get_current_dir() + "/model.json";
	FileAccess* file = FileAccess::open(model_path, FileAccess::READ);
	
	CharString file_bytes;

	CharType c = file->get_8();
	while (!file->eof_reached()) {
		file_bytes.push_back(c);
		c = file->get_8();
	}
	file_bytes.push_back(0);

	String model_str = String::utf8(file_bytes.get_data());

	Variant result;
	String err_msg;
	int err_line;
	Error err = JSON::parse(model_str, result, err_msg, err_line);

	if (err) {
		OS::get_singleton()->print("Block model parse failed. Message: '%s' line: %d\n", err_msg.utf8().get_data(), err_line);
	}

	file->close();
	memdelete(file);

	return result.operator Dictionary();
}

VoxelLibrary::VoxelLibrary(String blocks_root) {
	_init_blocks_info(blocks_root);
}


VoxelLibrary::~VoxelLibrary() {
	delete[] blocks;
}

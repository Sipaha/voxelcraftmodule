﻿#ifndef VOXEL_LIBRARY_H
#define VOXEL_LIBRARY_H

#include "os/os.h"
#include "error_macros.h"
#include "ustring.h"
#include "list.h"
#include "vector.h"
#include "io/json.h"
#include "os/dir_access.h"
#include "os/file_access.h"
#include "scene/resources/texture.h"
#include "image_atlas.h"
#include "../chunks/block.h"

class VoxelLibrary {

private:

	int blocks_size = 0;
	BlockInfo* blocks;

	ImageAtlas atlas;

	void _init_blocks_info(String blocks_root);

	BlockInfo _load_block_info(int id, DirAccess *block_dir);

	Dictionary read_model(DirAccess *block_dir);
	void fill_model(BlockInfo* info, Dictionary* data, String textures_base);

public:

	_FORCE_INLINE_ const BlockInfo* get_block(int p_index) const {

		CRASH_BAD_INDEX(p_index, blocks_size);

		return &(blocks[p_index]);
	}

	VoxelLibrary(String blocks_root);
	~VoxelLibrary();

};

#endif // !VOXEL_LIBRARY_H

#include "terrain.h"
#include "scene/main/viewport.h"
#include "controller/commands/commands.h"
#include "print_string.h"

using namespace vxl::commands;

Terrain* Terrain::instance;

Ref<World> Terrain::get_world() {
    return instance->get_tree()->get_root()->get_world();
}

void Terrain::_notification(int p_what) {

    switch (p_what) {

    case NOTIFICATION_ENTER_TREE: {

        if (!Engine::get_singleton()->is_editor_hint()) {
            set_process(true);
        }
        break;

    case NOTIFICATION_PROCESS:

        chunks->update(get_process_delta_time());
        update_stats();

        break;

    }
    }
}

void Terrain::damage_block(const Vector3& pos, const float amount) {
    Int3 ipos = Int3((int) Math::round(pos.x),
                     (int) Math::round(pos.y),
                     (int) Math::round(pos.z));

    Int3 chunk_pos = BLOCK_TO_CHUNK(ipos);
    Int3 block_in_chunk = ipos - CHUNK_TO_BLOCK(chunk_pos);

    Command* comm = new_command(this, &Terrain::damage_block_impl, block_in_chunk, amount);
    Chunks::execute(chunk_pos, comm);
}

void Terrain::damage_block_impl(Chunk* chunk, Int3 block_in_chunk, const float damage) {

    Block block = chunk->blocks.get_block(block_in_chunk);

    if (block.id != 0) {

        chunk->blocks.lock();

        block.id = 0;
        chunk->blocks.set_block(block_in_chunk, block);

        chunk->blocks.unlock();
    }
}

Ref<Material> Terrain::get_material() {
    //return test_material;
    return library->get_block(0)->material;
}

void Terrain::set_material(const Ref<Material> &p_texture) {
    this->test_material = p_texture;
}

void Terrain::add_block(const Vector3& pos, const int id) {
    Int3 ipos = Int3((int)Math::round(pos.x),
                     (int)Math::round(pos.y),
                     (int)Math::round(pos.z));

    Int3 chunk_pos = BLOCK_TO_CHUNK(ipos);
    Int3 block_in_chunk = ipos - CHUNK_TO_BLOCK(chunk_pos);

    Command* comm = new_command(this, &Terrain::add_block_impl, block_in_chunk, id);
    Chunks::execute(chunk_pos, comm);
}

void Terrain::add_block_impl(Chunk* chunk, Int3 block_in_chunk, int id) {

    Block block = chunk->blocks.get_block(block_in_chunk);

    if (block.id == 0) {

        chunk->blocks.lock();

        block.id = id;
        chunk->blocks.set_block(block_in_chunk, block);

        chunk->blocks.unlock();
    }
}

void Terrain::blow_up(const Vector3& pos, const float force) {

    Int3 ipos = Int3((int)Math::round(pos.x),
                     (int)Math::round(pos.y),
                     (int)Math::round(pos.z));

    Int3 chunk_pos = BLOCK_TO_CHUNK(ipos);

    Int3 zero_vec;
    float sqr_force = force * force;
    Command* comm = new_command(this, &Terrain::blow_up_impl, ipos, zero_vec, sqr_force);
    Chunks::execute(chunk_pos, comm);
}

void Terrain::blow_up_impl(Chunk* chunk, Int3 epicenter, Int3 vector, float radius) {

    Int3 blocks_base = CHUNK_TO_BLOCK(chunk->position);

    chunk->blocks.lock();
    bool changed = false;

    for (int x = 0; x < CHUNK_BLOCKS_SIZE; x++) {
        for (int y = 0; y < CHUNK_BLOCKS_SIZE; y++) {
            for (int z = 0; z < CHUNK_BLOCKS_SIZE; z++) {

                Int3 pos = blocks_base + Int3(x, y, z);

                if ((epicenter - pos).sqr_length() < radius) {

                    Block block = chunk->blocks.get_block(x, y, z);
                    changed = true;
                    if (block.id != 0) {
                        block.id = 0;
                        chunk->blocks.set_block(x, y, z, block);
                    }
                }
            }
        }
    }

    chunk->blocks.unlock();

    if (vector == Int3(0, 0, 0)) {

        int mask = 1;
        for (int i = 0; i < SIDES_CONTACT_COUNT; i++, mask <<= 1) {
            Int3 new_vec = vxl_t::sides_vec[i];
            Command* comm = new_command(this, &Terrain::blow_up_impl, epicenter, new_vec, radius);
            Chunks::execute(chunk->position + new_vec, comm);
        }

        for (int i = SIDES_CONTACT_COUNT; i < SIDES_COUNT; i++) {
            Int3 new_vec = vxl_t::sides_vec[i];
            Command* comm = new_command(this, &Terrain::blow_up_impl, epicenter, new_vec, radius);
            Chunks::execute(chunk->position + new_vec, comm);
        }


    } else if (changed) {

        for (int i = 0; i < SIDES_COUNT; i++) {

            Int3 vec = vxl_t::sides_vec[i];
            Int3 subvec = Int3(vec.x != 0 ? vector.x : 0,
                               vec.y != 0 ? vector.y : 0,
                               vec.z != 0 ? vector.z : 0);
            if (vec == subvec) {
                Command* comm = new_command(this, &Terrain::blow_up_impl, epicenter, vec, radius);
                Chunks::execute(chunk->position + vec, comm);
            }
        }
    }
}



void Terrain::update_stats() {

    /*
    uint32_t msec = OS::get_singleton()->get_ticks_msec();

    for (int i = 0; i < ChunkProcessor::processors.size(); i++) {
        ChunkProcessor* processor = ChunkProcessor::processors[i];
        stats[processor->get_name()] = processor->get_input()->size();
    }

    stats[String("time_to_get_stats")] = OS::get_singleton()->get_ticks_msec() - msec;
    */
}

void Terrain::_bind_methods() {

    ClassDB::bind_method(D_METHOD("damage_block", "pos", "amount"), &Terrain::damage_block);
    ClassDB::bind_method(D_METHOD("blow_up", "pos", "force"), &Terrain::blow_up);
    ClassDB::bind_method(D_METHOD("add_block", "pos", "id"), &Terrain::add_block);

    ClassDB::bind_method(D_METHOD("get_stats"), &Terrain::get_stats);

    ClassDB::bind_method(D_METHOD("set_material"), &Terrain::set_material);
    ClassDB::bind_method(D_METHOD("get_material"), &Terrain::get_material);

    ADD_PROPERTYNZ(PropertyInfo(Variant::OBJECT, "material_override", PROPERTY_HINT_RESOURCE_TYPE, "ShaderMaterial,SpatialMaterial"), "set_material", "get_material");
    //ADD_PROPERTYNZ(PropertyInfo(Variant::OBJECT, "sunlight_slider", PROPERTY_HINT_, "ShaderMaterial,SpatialMaterial"), "set_material", "get_material");
}

Terrain::Terrain() {
    instance = this;
    chunks = memnew(Chunks);
    observers = memnew(Observers);
    library = new VoxelLibrary("res://assets/blocks");
}

Terrain::~Terrain() {
    memdelete(chunks);
}

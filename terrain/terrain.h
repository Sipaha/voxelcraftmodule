#ifndef TERRAIN_H
#define TERRAIN_H

#include "../constants.h"
#include "list.h"
#include "../utils/collections/concurrent_list.h"
#include "../utils/collections/ordered_hset.h"
#include "os/thread.h"
#include "scene/main/node.h"
#include "scene/3d/mesh_instance.h"
#include "print_string.h"
#include "message_queue.h"
#include "../utils/threads/lock_set.h"
#include "../utils/threads/thread_pool.h"
#include "../utils/collections/ordered_hset.h"
#include "chunks/chunks.h"
#include "generator/chunk_generator.h"
#include "library/voxel_library.h"
#include "controller/observer/observers.h"

class Terrain : public Node {

    GDCLASS(Terrain, Node)

private:

    static Terrain* instance;

    Chunks* chunks;

    ChunkGenerator* generator = NULL;
    VoxelLibrary* library = NULL;

    ChunkRef* test_refs;

    Observers* observers;

    Dictionary stats;

    Ref<Material> test_material;

    //Slider* sunlight_slider;
    //float sunlight_value = 1.0;

    void update_stats();

    void damage_block_impl(Chunk* chunk, Int3 block_in_chunk, const float damage);
    void blow_up_impl(Chunk* chunk, Int3 epicenter, Int3 vector, float radius);
    void add_block_impl(Chunk* chunk, Int3 block_in_chunk, int id);

protected:

	static void _bind_methods();
	void _notification(int p_what);

public:

    void damage_block(const Vector3& pos, const float amount);
    void blow_up(const Vector3& pos, const float force);
    void add_block(const Vector3& pos, const int id);

    Ref<Material> get_material();
    void set_material(const Ref<Material> &p_texture);

    _FORCE_INLINE_ static void set_generator(ChunkGenerator* generator) {
        instance->generator = generator;
    }

    _FORCE_INLINE_ static ChunkGenerator* get_generator() {
        return instance->generator;
    }

    _FORCE_INLINE_ static VoxelLibrary* get_library() {
        return instance->library;
    }

    _FORCE_INLINE_ static void add_child_node(Node* child) {
        instance->add_child(child);
    }

    _FORCE_INLINE_ static Observers* get_observers() {
        return instance->observers;
    }

    static Ref<World> get_world();

    Dictionary get_stats() {
        return stats;
    }

    /*
    void set_sunlight_slider(Slider* slider) {
        sunlight_slider = slider;
    }

    Slider* get_sunlight_slider() {
        return sunlight_slider;
    }
    */

    Terrain();
    ~Terrain();
};

#endif // TERRAIN_H

#ifndef CACHE_H
#define CACHE_H

#include "hmap.h"
#include "print_string.h"

#define HMAP_T HMap<T, uint32_t, T, T>

template<class T>
class Cache {

    HMAP_T cache;
    Mutex* mutex;

public:

    T& add(T* value) {
        return add(*value);
    }

    T& add(T& value) {

        T::hash(value);

        mutex->lock();

        HMAP_T::Entry* entry = cache.get_entry(value);

        if (entry) {
            entry->pair.data++;
        } else {
            entry = cache.set(value, 1);
        }

        mutex->unlock();
        return entry->pair.key;
    }

    void remove(T* value) {
        remove(*value);
    }

    void remove(T& value) {

        mutex->lock();

        HMAP_T::Entry* entry = cache.get_entry(value);

        if (entry) {
            entry->pair.data = entry->pair.data - 1;
            if (entry->pair.data <= 0) {
                cache.erase(value);
            }
        }
        mutex->unlock();
    }

    void clear() {
        mutex->lock();
        cache.clear();
        mutex->unlock();
    }

    Cache() {
        mutex = Mutex::create();
    }

    ~Cache() {
        memdelete(mutex);
    }
};

#undef HMAP_T

#endif // CACHE_H

#ifndef CONCURRENT_LIST_H
#define CONCURRENT_LIST_H

#include "list.h"
#include "os/mutex.h"
#include "print_string.h"

template <class T>
class ConcurrentList {

private:

    struct CElement {

        friend class ConcurrentList<T>;

        T value;
        float weight;

        CElement(const T& value, float weight) {
            this->value = value;
            this->weight = weight;
        }

        CElement(){}
    };

    List<CElement> data;
    Mutex* mutex;

public:

    _FORCE_INLINE_ void add(const T& value, float weight = 0) {

        mutex->lock();

        List<CElement>::Element *el = data.back();
        List<CElement>::Element *el_to_move = NULL;

        while (el && weight < el->get().weight) {
            el_to_move = el;
            el = el->prev();
        }

        CElement c_element(value, weight);
        List<CElement>::Element* list_el = data.push_back(c_element);

        if (el_to_move != NULL) {
            data.move_before(list_el, el_to_move);
        }

        mutex->unlock();
    }

    _FORCE_INLINE_ T pop() {
        mutex->lock();
        T result = 0;
        if (data.size() > 0) {
            result = data.front()->get().value;
            data.pop_front();
        }
        mutex->unlock();
        return result;
    }

    _FORCE_INLINE_ void delete_all() {

        mutex->lock();

        if (data.size() > 0) {

            List<CElement>::Element* el = data.front();
            do {
                delete el->get().value;
            } while (el = el->next());

            data.clear();
        }

        mutex->unlock();
    }

    _FORCE_INLINE_ void clear() {
        mutex->lock();
        data.clear();
        mutex->unlock();
    }

    ConcurrentList() {
        mutex = Mutex::create();
    }

    ~ConcurrentList() {
        memdelete(mutex);
        mutex = NULL;
    }
};

#endif // CONCURRENT_LIST_H

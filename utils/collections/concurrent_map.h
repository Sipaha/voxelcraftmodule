#ifndef CONCURRENT_MAP_H
#define CONCURRENT_MAP_H

#include "hash_map.h"
#include "os/mutex.h"
#include "../../constants.h"

template<class K, class V>
class ConcurrentMap {

    HashMap<K, V, K> data;
    Mutex* mutex;

public:

    template<class C>
    void for_each(C* consumer, int(C::*method)(const K* key, V val)) {
/*
        mutex->lock();

        List<K> keys_to_remove;
        const K *key = NULL;
        while (key = data.next(key)) {
            int action = (consumer->*method)(key, data.get(key));
            if (action == Actions::REMOVE) {
                keys_to_remove.push_back(*key);
            }
        }

        for (List<K>::Element *el = keys_to_remove.front(); el; el = el->next()) {
            data.erase(el->get());
        }

        mutex->unlock();
        */
    }

    _FORCE_INLINE_ V* getptr(const K& p_key) {
        V* result = NULL;
        mutex->lock();
        result = data.getptr(p_key);
        mutex->unlock();
        return result;
    }

    _FORCE_INLINE_ void put(const K& p_key, const V& p_data) {
        mutex->lock();
        data.set(p_key, p_data);
        mutex->unlock();
    }

    _FORCE_INLINE_ V get(const K& p_key) {
        V* ptr = getptr(p_key);
        V result = 0;
        if (ptr != NULL) {
            result = *ptr;
        }
        return result;
    }

    _FORCE_INLINE_ bool erase(const K& p_key) {
        mutex->lock();
        bool result = data.erase(p_key);
        mutex->unlock();
        return result;
    }

    _FORCE_INLINE_ void clear() {
        mutex->lock();
        data.clear();
        mutex->unlock();
    }

    ConcurrentMap() {
        mutex = Mutex::create();
    }

    ~ConcurrentMap() {
        memdelete(mutex);
    }
};

#endif // CONCURRENT_MAP_H

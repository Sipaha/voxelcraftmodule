#ifndef ORDERED_SET_H
#define ORDERED_SET_H

#include "list.h"
#include "hmap.h"
#include "../../constants.h"

template <class T>
class OrderedHSet {

public:

    struct OElement {

        T value;
        float order;
        bool is_valid;

        OElement(T value, float order) {
            this->value = value;
            this->order = order;
            this->is_valid = true;
        }

        OElement(const OElement& other) {
            this->value = other.value;
            this->order = other.order;
            this->is_valid = other.is_valid;
        }

        OElement() {
            this->is_valid = false;
        }
    };

private:

    typedef List<OElement> OHSetList;
    typedef HMap<T, typename OHSetList::Element*, T, T> OHSetHMap;

    OHSetList elements;
    OHSetHMap list_elements_by_value;
    Mutex* mutex;

public:

    template<class C>
    void for_each(C* consumer, int(C::*method)(T& val)) {

        mutex->lock();

        for (List<T>::Element *el = elements.front(); el; el = el->next()) {
            const T item = el->get().value;
            int action = (consumer->*method)(item);
            if (action == Actions::REMOVE) {
                list_elements_by_value.erase(item);
                el->erase();
            }
        }

        mutex->unlock();
    }

    template<class A>
    OElement pop_accepted(A* consumer, bool (A::*method)(T& value)) {

        OElement result = OElement();

        mutex->lock();

        OHSetList::Element* element = elements.front();
        while (element != NULL && !(consumer->*method)(element->get().value)) {
            element = element->next();
        }

        if (element != NULL) {
            result = element->get();
            list_elements_by_value.erase(result->value);
            element->erase();
        }

        mutex->unlock();

        return result;
    }

    OElement pop() {

        OElement result = OElement();

        mutex->lock();

        if (elements.size() > 0) {
            result = elements.front()->get();
            elements.pop_front();
            list_elements_by_value.erase(result.value);
        }

        mutex->unlock();

        return result;
    }

    void add(T const &value, float order = 0) {

        mutex->lock();

        OHSetHMap::Entry *ent = list_elements_by_value.get_entry(value);
        OHSetList::Element *element = NULL;

        if (ent != NULL) {

            element = ent->pair.data;
            if (element->get().order > order) {
                element->get().order = order;
            }

        } else {

            OElement s_el(value, order);
            element = elements.push_back(s_el);
            list_elements_by_value.set(value, element);

        }

        OHSetList::Element *move_to = element;

        while (move_to->prev() != NULL && move_to->prev()->get().order > order) {
            move_to = move_to->prev();
        }

        if (move_to != element) {
            elements.move_before(element, move_to);
        }

        mutex->unlock();
    }

    OElement* remove(T const &value) {

        OElement* result = NULL;

        mutex->lock();

        OHSetHMap::Entry *ent = list_elements_by_value.get_entry(value);
        if (ent != NULL) {
            result = &ent->pair.data->get();
            elements.erase(ent->pair.data);
            list_elements_by_value.erase(value);
        }

        mutex->unlock();

        return result;
    }

    int size() {
        mutex->lock();
        int result = elements.size();
        mutex->unlock();
        return result;
    }

    void clear() {
        mutex->lock();
        elements.clear();
        list_elements_by_value.clear();
        mutex->unlock();
    }

    OrderedHSet() {
        mutex = Mutex::create();
    }

    ~OrderedHSet() {
        memdelete(mutex);
    }
};

#endif // ORDERED_SET_H

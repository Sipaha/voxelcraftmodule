#ifndef WAITING_QUEUE_H
#define WAITING_QUEUE_H

#include "list.h"
#include "os/mutex.h"

template <class T>
class WaitingQueue {

private:

    struct Timer {
        T value;
        float time;
        Timer(const T& value, float time) {
            this->value = value;
            this->time = time;
        }

        Timer() {}
    };

    Mutex* mutex;
    List<Timer> data;

    _FORCE_INLINE_ typename List<Timer>::Element* find(const typename T& value) {
        for (List<Timer>::Element* el = data.front(); el != NULL; el = el->next()) {
            if (T::compare(el->get().value, value)) {
                return el;
            }
        }
        return NULL;
    }

public:

    void add(const T& value, float time) {

        mutex->lock();

        List<Timer>::Element* el = find(value);
        if (el != NULL) {
            el->get().time = MAX(el->get().time, time);
        } else {
            data.push_back(Timer(value, time));
        }

        mutex->unlock();
    }

    bool remove(const T& value) {

        mutex->lock();

        List<Timer>::Element *el = find(value);
        bool result = false;
        if (el != NULL) {
            el->erase();
            result = true;
        }

        mutex->unlock();
        return result;
    }

    void update(float delta, List<T>* output) {

        mutex->lock();

        List<Timer>::Element* next;
        for (List<Timer>::Element* el = data.front(); el != NULL; el = next) {
            next = el->next();
            el->get().time -= delta;
            if (el->get().time <= 0) {
                output->push_back(el->get().value);
                el->erase();
            }
        }

        mutex->unlock();
    }

    void update(float delta) {

        mutex->lock();

        List<Timer>::Element* next;
        for (List<Timer>::Element* el = data.front(); el != NULL; el = next) {
            next = el->next();
            el->get().time -= delta;
            if (el->get().time <= 0) {
                el->erase();
            }
        }

        mutex->unlock();
    }

    WaitingQueue() {
        mutex = Mutex::create();
    }

};

#endif // WAITING_QUEUE_H


#include "vxl_util.h"

String vxl_util::join_enum_variants(const char **variants, int count) {
	String result;
	for (int i = 0; i < count; i++) {
		if (i > 0) {
			result += ",";
		}
		result += variants[i];
	}
	return result;
}

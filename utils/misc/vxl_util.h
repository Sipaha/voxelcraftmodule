#ifndef MACROS_H
#define MACROS_H

#include "ustring.h"

#define FIELD_GET_SET(type, name, deft) type name = ##deft##; \
void set_##name##(type value) {\
    name = value;\
    _change_notify();\
    ports_changed_notify();\
}\
type get_##name##() const {\
    return name##;\
}

namespace vxl_util {

    String join_enum_variants(const char** variants, int count);
}

#endif // MACROS_H

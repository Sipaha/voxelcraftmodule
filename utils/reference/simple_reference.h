#ifndef SIMPLE_REFERENCE_H
#define SIMPLE_REFERENCE_H

#include "safe_refcount.h"

class SimpleReference {

public:

	SafeRefCount refcount;
	SafeRefCount refcount_init;

	bool init_ref() {

		if (reference()) {

			// this may fail in the scenario of two threads assigning the pointer for the FIRST TIME
			// at the same time, which is never likely to happen (would be crazy to do)
			// so don't do it.

			if (refcount_init.get() > 0) {
				refcount_init.unref();
				unreference(); // first referencing is already 1, so compensate for the ref above
			}

			return true;
		} else {

			return false;
		}
	}

	inline int reference_get_count() const {
		return refcount.get();
	}

	inline bool reference() {
		return refcount.ref();
	}

	inline bool unreference() {
		return refcount.unref();
	}

	SimpleReference() {
		refcount.init();
		refcount_init.init();
	}

	~SimpleReference() {
	}
};

#endif // SIMPLE_REFERENCE_H

#ifndef INT3_H
#define INT3_H

#include "typedefs.h"
#include "math/math_funcs.h"
#include "vector3.h"
#include "variant.h"

#define INT_SIGN(n) (n < 0 ? -1 : (n > 0 ? 1 : 0))

struct Int3 {

private:

    mutable uint32_t _hash = 0;

public:

    int x;
    int y;
    int z;

    Int3(int x, int y, int z) : x(x),
                                y(y),
                                z(z) {
    }

    Int3(real_t x, real_t y, real_t z) : x((int) x),
                                         y((int) y),
                                         z((int) z) {
    }

    Int3(const Vector3& other) : x((int) other.x),
                                 y((int) other.y),
                                 z((int) other.z) {
    }

    Int3() : x(0),
             y(0),
             z(0) {
    }

    Int3(const Int3& other) : x(other.x),
                              y(other.y),
                              z(other.z) {
    }

    _FORCE_INLINE_ Int3 negate() {
        return Int3(-x, -y, -z);
    }

    _FORCE_INLINE_ Int3 sign() {
        return Int3(INT_SIGN(x), INT_SIGN(y), INT_SIGN(z));
    }

    _FORCE_INLINE_ Int3 to_x_pos() {
        return Int3(x + 1, y, z);
    }

    _FORCE_INLINE_ Int3 to_x_neg() {
        return Int3(x - 1, y, z);
    }

    _FORCE_INLINE_ Int3 to_y_pos() {
        return Int3(x, y + 1, z);
    }

    _FORCE_INLINE_ Int3 to_y_neg() {
        return Int3(x, y - 1, z);
    }

    _FORCE_INLINE_ Int3 to_z_pos() {
        return Int3(x, y, z + 1);
    }

    _FORCE_INLINE_ Int3 to_z_neg() {
        return Int3(x, y, z - 1);
    }

    _FORCE_INLINE_ float length() {
        return Math::sqrt((float) x * x + y * y + z * z);
    }

    _FORCE_INLINE_ int sqr_length() {
        return x * x + y * y + z * z;
    }

    _FORCE_INLINE_ bool operator ==(const Int3& other) const {
        return other.x == x && other.y == y && other.z == z;
    }

    _FORCE_INLINE_ bool operator !=(const Int3& other) const {
        return other.x != x || other.y != y || other.z != z;
    }

    _FORCE_INLINE_ Int3 operator +(const Int3& other) const {
        return Int3(x + other.x, y + other.y, z + other.z);
    }

    _FORCE_INLINE_ Int3 operator -(const Int3& other) const {
        return Int3(x - other.x, y - other.y, z - other.z);
    }

    _FORCE_INLINE_ Int3 operator *(const int& num) const {
        return Int3(x * num, y * num, z * num);
    }

    _FORCE_INLINE_ Int3 operator /(const int& num) const {
        return Int3(x / num, y / num, z / num);
    }

    static Int3 cast(const Variant& other) {
        return Int3(other.operator Vector3());
    }

    static _FORCE_INLINE_ uint32_t hash(const Int3& vec) {
        if (vec._hash == 0) {
            vec._hash = 31 * (31 * (31 + vec.x) + vec.y) + vec.z;
        }
        return vec._hash;
    }

    static _FORCE_INLINE_ uint32_t hash(const Int3*& vec) {
        return hash(*vec);
    }

    static _FORCE_INLINE_ bool compare(const Int3& p_lhs, const Int3& p_rhs) {
        return p_lhs.x == p_rhs.x && p_lhs.y == p_rhs.y && p_lhs.z == p_rhs.z;
    }

    static _FORCE_INLINE_ bool compare(const Int3*& p_lhs, const Int3*& p_rhs) {
        return compare(*p_lhs, *p_rhs);
    }
};

#endif // INT3_H

#ifndef LOCK_SET_H
#define LOCK_SET_H

#include "../collections/hmap.h"
#include "reference.h"

template <typename T>
class LockSet {

public:

    struct Lock : public Reference {

    private:

        LockSet* scope;

    public:

        T value;

        Lock(LockSet* scope, const T& value) {
            this->scope = scope;
            this->value = value;
            scope->data.set(value, 0);
        }

        ~Lock() {
            scope->release(value);
        }
    };

    template<size_t size>
    struct Locks : public Reference {

    private:

        LockSet* scope;

    public:

        T values[size];

        Locks(LockSet* scope, const T* values) {
            this->scope = scope;
            for (int i = 0; i < size; ++i) {
                this->values[i] = values[i];
                scope->data.set(values[i], 0);
            }
        }

        ~Locks() {
            scope->release_arr<size>(values);
        }
    };

private:

    typedef HMap<T, uint8_t, T, T> Data_t;

    Data_t data;
    Mutex* mutex;

    _FORCE_INLINE_ void release(const T& elem) {
        mutex->lock();
        data.erase(elem);
        mutex->unlock();
    }

    template<size_t size>
    _FORCE_INLINE_ void release_arr(const T* elems) {
        mutex->lock();
        for (int i = 0; i < size; i++) {
            data.erase(elems[i]);
        }
        mutex->unlock();
    }

public:

    _FORCE_INLINE_ Ref<Lock> try_lock(const T& elem) {

        Ref<Lock> result;
        mutex->lock();
        if (data.getptr(elem) == NULL) {
            result = Ref<Lock>(memnew(Lock(this, elem)));
        }
        mutex->unlock();
        return result;
    }

    template<size_t size>
    _FORCE_INLINE_ Ref<Locks<size>> try_lock(const T* elems) {
        Ref<Locks<size>> result;
        mutex->lock();
        bool can_lock = true;
        for (int i = 0; i < size; ++i) {
            if (data.getptr(elems[i]) != NULL) {
                can_lock = false;
                break;
            }
        }
        if (can_lock) {
            result = Ref<Locks<size>>(memnew(Locks<size>(this, elems)));
        }
        mutex->unlock();
        return result;
    }

    Ref<Lock> wait_and_lock(const T& elem) {
        while (true) {
            Ref<Lock> lock = try_lock(elem);
            if (lock.is_valid()) {
                return lock;
            } else {
                OS::get_singleton()->delay_usec(1000);
            }
        }
    }

    template<size_t size>
    Ref<Locks<size>> wait_and_lock(const T* elems) {
        while (true) {
            Ref<Locks<size>> lock = try_lock<size>(elems);
            if (lock.is_valid()) {
                return lock;
            } else {
                OS::get_singleton()->delay_usec(1000);
            }
        }
    }

    LockSet() {
        mutex = Mutex::create();
    }

    ~LockSet() {
        memdelete(mutex);
    }
};

#endif // LOCK_SET_H

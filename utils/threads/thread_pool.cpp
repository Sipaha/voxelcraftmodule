#include "thread_pool.h"

//200ms
#define JOB_THREAD_QUANT 200000

bool MTJob::thread_process() {
    return mt_job_proc_func(data);
}

bool MTJob::try_to_get() {

    bool result = false;

    if (mutex->try_lock() == Error::OK) {
        if (!is_sleeping() && curr_threads < max_threads) {
            curr_threads++;
            result = true;
        }
        mutex->unlock();
    }

    return result;
}

void MTJob::release() {
    mutex->lock();

    curr_threads = MAX(0, curr_threads - 1);

    mutex->unlock();
}

MTJob::MTJob(typename MTProcFunc mt_job_proc_func, void* data, int threads) {
    this->mt_job_proc_func = mt_job_proc_func;
    this->mutex = Mutex::create();
    this->data = data;
    max_threads = threads;
    curr_threads = 0;
}

MTJob::~MTJob() {
    memdelete(mutex);
}

ThreadPool* ThreadPool::instance = NULL;

void ThreadPool::_threads_update(void *data) {

    ThreadData* thread_data = static_cast<ThreadData*>(data);

    while (!thread_data->is_active()) {
        OS::get_singleton()->delay_usec(100000);
    }

    Vector<MTJob*>* jobs = thread_data->get_jobs();

    while (thread_data->is_active()) {

        bool job_exists = false;

        for (int i = 0; i < jobs->size(); i++) {

            MTJob* job = jobs->get(i);

            if (job->try_to_get()) {

                uint64_t until = OS::get_singleton()->get_ticks_usec() + JOB_THREAD_QUANT;

                do {

                    if (!job->thread_process()) {
                        job->sleep();
                        break;
                    } else {
                        job_exists = true;
                    }

                } while(OS::get_singleton()->get_ticks_usec() < until);

                job->release();
            }
        }

        if (!job_exists) {
            OS::get_singleton()->delay_usec(5000);
        }
    }

    thread_data->finished();
}

void ThreadPool::create(typename MTProcFunc mt_job_proc_func, void* data, int threads) {
    jobs.push_back(new MTJob(mt_job_proc_func, data, threads));
}

void ThreadPool::start() {
    is_active = true;
}

void ThreadPool::finish() {
    is_active = false;
}

bool ThreadPool::is_finished() {
    mutex->lock();
    bool result = finished_threads == PROC_THREAD_POOL_SIZE;
    mutex->unlock();
    return result;
}

ThreadPool::ThreadPool() {
    mutex = Mutex::create();
    mutex->lock();
    threads = memnew_arr(ThreadData, PROC_THREAD_POOL_SIZE);
    for (int i = 0; i < PROC_THREAD_POOL_SIZE; i++) {
        threads[i].initialize(this);
    }
    mutex->unlock();
}


ThreadPool::~ThreadPool() {
    is_active = false;
    memdelete_arr(threads);
    for (int i = 0; i < jobs.size(); i++) {
        delete jobs[i];
    }
    memdelete(mutex);
}




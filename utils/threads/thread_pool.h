#ifndef PROC_THREAD_POOL_H
#define PROC_THREAD_POOL_H

#include "vector.h"
#include "os/thread.h"
#include "os/os.h"

#define PROC_THREAD_POOL_SIZE 5

typedef bool (*MTProcFunc)(void* data);

class MTJob {

private:

    void* data;
    int max_threads;
    volatile int curr_threads;
    Mutex* mutex;
    uint64_t sleep_until = 0;

    _FORCE_INLINE_ bool is_sleeping() {
        return OS::get_singleton()->get_ticks_usec() < sleep_until;
    }

public:

    MTProcFunc mt_job_proc_func;

    bool thread_process();

    _FORCE_INLINE_ void sleep() {
        sleep_until = OS::get_singleton()->get_ticks_usec() + 5000; //5ms
    }

    bool try_to_get();
    void release();

    MTJob(typename MTProcFunc mt_job_proc_func, void* data, int threads);
    ~MTJob();
};

class ThreadPool {

    struct ThreadData {

        Thread* thread;
        ThreadPool* parent;

    public:

        _FORCE_INLINE_ void initialize(ThreadPool* parent) {
            this->parent = parent;
            thread = Thread::create(_threads_update, this);
        }

        _FORCE_INLINE_ bool is_active() {
            return parent->is_active;
        }

        _FORCE_INLINE_ Vector<MTJob*>* get_jobs() {
            return &parent->jobs;
        }

        void finished() {
            parent->mutex->lock();
            parent->finished_threads++;
            parent->mutex->unlock();
        }

        ThreadData() {}

        ~ThreadData() {
            memdelete(thread);
        }
    };

private:

    static ThreadPool* instance;

    ThreadPool();

protected:

    volatile bool is_active = false;

    ThreadData* threads;
    Vector<MTJob*> jobs;
    Mutex* mutex;

    volatile int finished_threads = 0;

    static void _threads_update(void* data);

public:

    static ThreadPool* get_singleton() {
        if (instance == NULL) {
            instance = memnew(ThreadPool);
        }
        return instance;
    }

    void create(typename MTProcFunc mt_job_proc_func, void* data, int threads);

    void start();
    void finish();
    bool is_finished();

    ~ThreadPool();
};

#endif // PROC_THREAD_POOL_H

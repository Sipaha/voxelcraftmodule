#include "time_utils.h"

void TimeWatch::start(String name) {
    this->name = name;

    OS::get_singleton()->print("Task '%s' started\n", name.utf8().get_data());

    start_ticks = OS::get_singleton()->get_ticks_usec();
}

void TimeWatch::stop() {

    uint64_t ticks = OS::get_singleton()->get_ticks_usec();
    double diff = (double)(ticks - start_ticks) / 1000000.0;

    OS::get_singleton()->print("Task '%s' finished. Time: '%f'\n", name.utf8().get_data(), diff);

}

TimeWatch::TimeWatch() {


}

TimeWatch::~TimeWatch() {


}

#ifndef VXL_TIME_UTILS_H
#define VXL_TIME_UTILS_H

#include "reference.h"
#include "string.h"
#include "os/os.h"

class TimeWatch {

private:

    uint64_t start_ticks;
    String name;

public:

    void start(String name);
    void stop();

    TimeWatch();
    ~TimeWatch();
};

#endif
